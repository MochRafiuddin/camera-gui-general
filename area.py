import numpy as np
import supervision as sv
import torch
import cv2
import sqlite3
import app_sync
import time

class CountObject():

    def __init__(self, id_data) -> None:

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = torch.hub.load('yolov5', 'custom', 'models/crowdhuman_yolov5m.pt', force_reload=True, source='local').to(self.device).eval()

        self.id_data = id_data
        self.data = self.get_peopel_hari_ini()

        self.colors = sv.ColorPalette.DEFAULT
        self.labels = eval(self.data[11]) if self.data[11] else []
        # area = eval(self.data[8]) if self.data[8] else []
        areas = eval(self.data[8]) if self.data[8] else []
        self.ratio_width = self.data[15]
        self.ratio_height = self.data[16]
        self.areas = [[[int(point[0] * float(self.ratio_width)), int(point[1] * float(self.ratio_width))] for point in area] for area in areas]
        resolution_wh = eval(self.data[17]) if self.data[17] else ()
        # print(area)
        self.polygons = np.array(self.areas)
        self.zones = [
            sv.PolygonZone(
                polygon=polygon,
            )
            for polygon
            in self.polygons
        ]

        self.zone_annotators = [
            sv.PolygonZoneAnnotator(
                zone=zone, 
                color=self.colors.by_idx(index), 
                thickness=4,
                text_thickness=1,
                text_scale=0.5
            )
            for index, zone
            in enumerate(self.zones)
        ]

        self.box_annotators = [
            sv.BoundingBoxAnnotator(
                color=self.colors.by_idx(index), 
                thickness=4,
                )
            for index
            in range(len(self.polygons))
        ]

        self.last_time_insert = time.time()
        self.time_interval_secondary = app_sync.get_cosntant_by_code('time_interval_secondary')

    def process_frame(self, frame: np.ndarray) -> np.ndarray:
        # detect
        results = self.model(frame)
        detections = sv.Detections.from_yolov5(results)
        detections = detections[(detections.class_id == 1) & (detections.confidence > 0.375)]
        area = 0
        people_in_area = []
        zone_total_all_zones = 0
        for zone, zone_annotator, box_annotator in zip(self.zones, self.zone_annotators, self.box_annotators):
            mask = zone.trigger(detections=detections)
            detections_filtered = detections[mask]
            # print(f'Area {area}: {len(detections_filtered)} detections')
            # alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            # sequence = alphabet[area]
            label_area = self.labels
            # print(f"area_code : {label_area[area][0]}, total : {len(detections_filtered)}")
            zone_total_all_zones = zone_total_all_zones + len(detections_filtered)
            people_in_area.append({'area_code': label_area[area][0], 'people': len(detections_filtered)})
            frame = box_annotator.annotate(scene=frame, detections=detections_filtered)
            frame = zone_annotator.annotate(scene=frame)

            # poly_points = [(int(point.x() * self.ratio_width), int(point.y() * self.ratio_height)) for point in self.areas[area]]
            poly_points = np.array(self.areas[area], dtype=np.int32)
            # roi_coords = np.array(self.areas[area], np.int32)
            # roi_coords = roi_coords.reshape((-1, 1, 2))
            min_x = np.min(poly_points[:, 0])
            min_y = np.min(poly_points[:, 1])
            input_box_position = (min_x + 10, min_y + 10)
            input_box_size = (180, 30)
            label_text = f"{label_area[area][0]}"
            # label_position = zone_annotator.get_label_position()  # Dapatkan posisi label dari zona
            cv2.putText(frame, label_text, (input_box_position[0] + 5, input_box_position[1] + input_box_size[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1, cv2.LINE_AA)

            area += 1
        
        self.update_data(people_in_area)
        self.update_current_deteksi(zone_total_all_zones, 'zone_total_all_zones')

        return frame
    
    def process_video(self, frame):
        
        processed_frame = self.process_frame(frame)

        current_time = time.time()
        if (current_time - self.last_time_insert) > int(self.time_interval_secondary[2]):
            app_sync.simpan_log_area(self.data[0])
            self.last_time_insert = current_time

        return processed_frame

    def update_data(self, people_in_area):
        record_id = self.data[0]

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        update_query = "UPDATE setting_camera_area SET people_in_area = ? WHERE id = ?"
        
        # Execute the update query
        cursor.execute(update_query, (str(people_in_area), record_id))
        
        # Commit the changes
        conn.commit()
        # print("update success")
        
        cursor.close()
        conn.close()
    
    def get_peopel_hari_ini(self):
        
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        # Create the table if it doesn't exist
        data = cursor.execute(f'SELECT * FROM setting_camera_area where id="{self.id_data}"').fetchone()
        # print(data)
        return data

    def update_current_deteksi(self, value, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"UPDATE setting_camera_area SET {code} = {value} WHERE id = {self.id_data}")
        conn.commit()
        cursor.close()
        conn.close()