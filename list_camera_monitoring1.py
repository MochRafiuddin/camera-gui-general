import sys
import os
import subprocess
import cv2
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QGridLayout, QWidget, QVBoxLayout, QHBoxLayout, QScrollArea
from PyQt5.QtGui import QPixmap, QFont, QImage
from PyQt5.QtCore import Qt, QProcess, QTimer, pyqtSlot, pyqtSignal, QEvent
import sqlite3
import time

from modal_snapshot import PopupForm
import numpy as np
# from home import App_home
from multiprocessing.managers import BaseManager

class QueueManager(BaseManager):
    pass
QueueManager.register('get_processed_frame_queue')

class MainWindowMonitoring(QMainWindow):
    control_panel_clicked = pyqtSignal()
    minimize_clicked = pyqtSignal()
    close_clicked = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.setStyleSheet("background-color: #222222;")
        # self.update_layout()  # Panggil update_layout untuk pengaturan awal
        # self.timer = QTimer()
        # self.timer.timeout.connect(self.update_detail_camera)
        # self.timer.start(30)


    def update_layout(self):
        self.manager = QueueManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
        self.manager.connect()
        self.screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = self.screen_geometry.width() / 1280
        self.height_ratio = self.screen_geometry.height() / 832

        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        # Buat QScrollArea
        scroll_area = QScrollArea()
        scroll_area.setContentsMargins(0, 0, 0, 0)
        scroll_area.setStyleSheet("border: 0px;")
        central_widget.setLayout(QVBoxLayout())
        central_widget.layout().setContentsMargins(0, 0, 0, 0)
        central_widget.layout().addWidget(scroll_area)

        # Set up grid layout inside scroll area
        scroll_widget = QWidget()
        self.grid_layout = QGridLayout(scroll_widget)
        self.grid_layout.setSpacing(10)  # Menambahkan spasi antar widget
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(scroll_widget)
        scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        camera_area = self.get_data_camera('setting_camera_area')
        camera_gender = self.get_data_camera('setting_camera_gender')
        camera_in_out = self.get_data_camera('setting_camera')
        site_code = self.get_cosntant_by_code('site_code')

        camera1 = []
        camera2 = self.get_camera_info(camera1, camera_in_out, 2, "People-Counting", 23, 25, 1,0, 18)
        camera3 = self.get_camera_info(camera2, camera_gender, 2, "Gender", 23, 25,1,0, 17)
        self.cameras = self.get_camera_info(camera3, camera_area, 2, "Area", 18, 20,1,0, 10)
        self.image_labels = []
        self.layout_1 = []
        self.layout_2 = []
        self.layout_3 = []
        self.layout_4 = []
        self.model_detaksi = []


        if len(self.cameras) > 1:
            self.navbar = QLabel(self)
            self.navbar.setStyleSheet("margin-bottom:10px")                    
            self.navbar.setFixedSize(int(self.screen_geometry.width() - 20), int(150 * self.height_ratio))
            self.top_navbar()
            self.grid_layout.addWidget(self.navbar, 0, 0, 1, 2)  # Menggabungkan self.navbar ke kolom 0 dan 1)
        else:
            self.navbar = QLabel(self)
            self.navbar.setStyleSheet("margin-bottom:10px")                                
            self.navbar.setFixedSize(int(self.screen_geometry.width()), int(150 * self.height_ratio))
            self.top_navbar()
            self.grid_layout.addWidget(self.navbar, 0, 0, 1, 2)  # Menggabungkan self.navbar ke kolom 0 dan 1)
            self.grid_layout.setAlignment(Qt.AlignTop)  # Mengatur alignment grid layout ke atas

        self.captures = []
        no_cap = 0
        for url in self.cameras:            
            # capture = cv2.VideoCapture(url[0])
            # if
            self.captures.append((url[6],url[7]))
            no_cap += 1

        # print(self.captures)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.display_frames)
        self.timer.start(25)  # Set interval waktu dalam milidetik (misalnya: 100ms)

        # Tambahkan widget ke dalam grid layout
        for row in range(len(self.cameras)):
            if self.cameras[row][5] == 0:
                continue  # Melompati iterasi jika i adalah bilangan genap
            image_label = QLabel(self)
            image_label.setStyleSheet("margin-left: 20px")
            
            label_camera = QLabel(self.cameras[row][3], image_label)
            label_camera.setStyleSheet("color: white;")
            label_camera.setFont(
                QFont("Times", int(14 * min(self.width_ratio, self.height_ratio)))
            )
            label_camera.move( int(image_label.width() * 1 / 100), int(image_label.height() * 10 / 100))
            label_camera.setFixedHeight(int(image_label.height() * 115 / 100))
            label_camera.setFixedWidth(int(image_label.width() * 895 / 100))
            
            letak_camera = QLabel(image_label)            
            letak_camera.move( int(image_label.width() * 1 / 100), int(image_label.height() * 180 / 100))
            letak_camera.setFixedHeight(int(image_label.height() * 1444 / 100))
            letak_camera.setFixedWidth(int(image_label.width() * 945 / 100))
            letak_camera.setScaledContents(True)
            self.grid_layout.addWidget(image_label, row + 1, 0)            
            self.image_labels.append(letak_camera)
            
            # if self.cameras[row][1] == "People-Counting":
            #     load_detaksi = VideoCamera(self.model, self.device)
            # elif self.cameras[row][1] == "Gender":
            #     load_detaksi = face_gender(self.device, self.FaceDetector, self.FaceClassifier)
            # elif self.cameras[row][1] == "Area":                        
            #     load_detaksi = CountObject(self.model, self.cameras[row][5])

            # self.model_detaksi.append(load_detaksi)

            empty_label2 = QLabel(self)
            empty_label2.setFixedHeight(500)  # Set tinggi label ke 600 piksel
            # Tambahkan layout untuk tombol-tombol
            # print(self.cameras[row][4][0])
            if self.cameras[row][2] == 3:
                # Tombol 1
                label_1 = QLabel(empty_label2)
                label_1.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_1.move( int(empty_label2.width() * 15 / 100), int(empty_label2.height() * 11 / 100))
                label_1.setFixedHeight(int(empty_label2.height() * 41 / 100))
                label_1.setFixedWidth(int(empty_label2.width() * 500 / 100))
                title_label_1 = QLabel("left (current)", label_1)
                title_label_1.setStyleSheet("color: white; border: None")
                title_label_1.setFont(
                    QFont("Times", int(28 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_1.move( int(image_label.width() * 10 / 100), int(image_label.height() * 40 / 100))

                value_label_1 = QLabel(self.add_zero(self.cameras[row][4][0]['outer_traffic_left']), label_1)
                value_label_1.setStyleSheet("color: white; border: None")
                value_label_1.setFont(
                    QFont("Times", int(64 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_1.move( int(image_label.width() * 300/100), int(image_label.height() * 230 / 100))

                # Tombol 2
                label_2 = QLabel(empty_label2)
                label_2.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_2.move( int(empty_label2.width() * 15 / 100), int(empty_label2.height() * 56 / 100))
                label_2.setFixedHeight(int(empty_label2.height() * 41 / 100))
                label_2.setFixedWidth(int(empty_label2.width() * 500 / 100))
                title_label_2 = QLabel("right (current)", label_2)
                title_label_2.setStyleSheet("color: white; border: None")
                title_label_2.setFont(
                    QFont("Times", int(28 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_2.move( int(image_label.width() * 10 / 100), int(image_label.height() * 40 / 100))

                value_label_2 = QLabel(self.add_zero(self.cameras[row][4][0]['outer_traffic_right']), label_2)
                value_label_2.setStyleSheet("color: white; border: None")
                value_label_2.setFont(
                    QFont("Times", int(64 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_2.move( int(image_label.width() * 300/100), int(image_label.height() * 230 / 100))

                # Tombol 2
                label_3 = QLabel(empty_label2)
                label_3.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_3.move( int(empty_label2.width() * 535 / 100), int(empty_label2.height() * 11 / 100))
                label_3.setFixedHeight(int(empty_label2.height() * 86 / 100))
                label_3.setFixedWidth(int(empty_label2.width() * 374 / 100))
                title_label_3 = QLabel("Total <br> Outer Traffic", label_3)
                title_label_3.setStyleSheet("color: white; border: None")
                title_label_3.setAlignment(Qt.AlignCenter)
                title_label_3.setFont(
                    QFont("Times", int(28 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_3.move( int(image_label.width() * 50 / 100), int(image_label.height() * 100 / 100))

                value_label_3 = QLabel(self.add_zero(self.cameras[row][4][0]['outer_traffic_total_traffic']), label_3)
                value_label_3.setStyleSheet("color: white; border: None")
                value_label_3.setAlignment(Qt.AlignCenter)
                value_label_3.setFont(
                    QFont("Times", int(78 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_3.move( int(image_label.width() * 80/100), int(image_label.height() * 650 / 100))

                self.layout_3.append({'title_label_1':title_label_1, 'value_label_1': value_label_1, 'title_label_2':title_label_2, 'value_label_2': value_label_2, 'title_label_3':title_label_3, 'value_label_3': value_label_3})
            
            elif self.cameras[row][2] == 2:
                label_1 = QLabel(empty_label2)
                label_1.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_1.move( int(empty_label2.width() * 15 / 100), int(empty_label2.height() * 11 / 100))
                label_1.setFixedHeight(int(empty_label2.height() * 86 / 100))
                label_1.setFixedWidth(int(empty_label2.width() * 435 / 100))
                title_label_1 = QLabel("Daily in", label_1)
                title_label_1.setStyleSheet("color: white; border: None")
                title_label_1.setAlignment(Qt.AlignCenter)
                title_label_1.setFont(
                    QFont("Times", int(28 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_1.move( int(image_label.width() * 130 / 100), int(image_label.height() * 100 / 100))

                value_label_1 = QLabel(self.add_zero(self.cameras[row][4][0]['visitor_daily_in']), label_1)
                value_label_1.setStyleSheet("color: white; border: None")
                value_label_1.setAlignment(Qt.AlignCenter)
                value_label_1.setFont(
                    QFont("Times", int(78 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_1.move( int(image_label.width() * 110/100), int(image_label.height() * 650 / 100))

                label_2 = QLabel(empty_label2)
                label_2.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_2.move( int(empty_label2.width() * 475 / 100), int(empty_label2.height() * 11 / 100))
                label_2.setFixedHeight(int(empty_label2.height() * 86 / 100))
                label_2.setFixedWidth(int(empty_label2.width() * 435 / 100))
                title_label_2 = QLabel("Daily Out", label_2)
                title_label_2.setStyleSheet("color: white; border: None")
                title_label_2.setAlignment(Qt.AlignCenter)
                title_label_2.setFont(
                    QFont("Times", int(28 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_2.move( int(image_label.width() * 130 / 100), int(image_label.height() * 100 / 100))
                value_label_2 = QLabel(self.add_zero(self.cameras[row][4][0]['visitor_daily_out']), label_2)
                value_label_2.setStyleSheet("color: white; border: None")
                value_label_2.setAlignment(Qt.AlignCenter)
                value_label_2.setFont(
                    QFont("Times", int(78 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_2.move( int(image_label.width() * 110/100), int(image_label.height() * 650 / 100))
                self.layout_2.append({'title_label_1':title_label_1, 'value_label_1': value_label_1, 'title_label_2':title_label_2, 'value_label_2': value_label_2})

            elif self.cameras[row][2] == 4:
                label_1 = QLabel(empty_label2)
                label_1.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_1.move( int(empty_label2.width() * 15 / 100), int(empty_label2.height() * 11 / 100))
                label_1.setFixedHeight(int(empty_label2.height() * 41 / 100))
                label_1.setFixedWidth(int(empty_label2.width() * 435 / 100))
                title_label_1 = QLabel("Male In (current)", label_1)
                title_label_1.setStyleSheet("color: white; border: None")
                title_label_1.setFont(
                    QFont("Times", int(24 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_1.move( int(image_label.width() * 10 / 100), int(image_label.height() * 40 / 100))

                value_label_1 = QLabel(self.add_zero(self.cameras[row][4][0]['gender_male_in']), label_1)
                value_label_1.setStyleSheet("color: white; border: None")
                value_label_1.setFont(
                    QFont("Times", int(64 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_1.move( int(image_label.width() * 233/100), int(image_label.height() * 230 / 100))

                label_2 = QLabel(empty_label2)
                label_2.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_2.move( int(empty_label2.width() * 15 / 100), int(empty_label2.height() * 56 / 100))
                label_2.setFixedHeight(int(empty_label2.height() * 41 / 100))
                label_2.setFixedWidth(int(empty_label2.width() * 435 / 100))
                title_label_2 = QLabel("Male Out (current)", label_2)
                title_label_2.setStyleSheet("color: white; border: None")
                title_label_2.setFont(
                    QFont("Times", int(24 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_2.move( int(image_label.width() * 10 / 100), int(image_label.height() * 40 / 100))

                value_label_2 = QLabel(self.add_zero(self.cameras[row][4][0]['gender_male_out']), label_2)
                value_label_2.setStyleSheet("color: white; border: None")
                value_label_2.setFont(
                    QFont("Times", int(64 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_2.move( int(image_label.width() * 233/100), int(image_label.height() * 230 / 100))

                label_3 = QLabel(empty_label2)
                label_3.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_3.move( int(empty_label2.width() * 475 / 100), int(empty_label2.height() * 11 / 100))
                label_3.setFixedHeight(int(empty_label2.height() * 41 / 100))
                label_3.setFixedWidth(int(empty_label2.width() * 435 / 100))
                title_label_3 = QLabel("Female In (current)", label_3)
                title_label_3.setStyleSheet("color: white; border: None")
                title_label_3.setFont(
                    QFont("Times", int(24 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_3.move( int(image_label.width() * 10 / 100), int(image_label.height() * 40 / 100))

                value_label_3 = QLabel(self.add_zero(self.cameras[row][4][0]['gender_female_in']), label_3)
                value_label_3.setStyleSheet("color: white; border: None")
                value_label_3.setFont(
                    QFont("Times", int(64 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_3.move( int(image_label.width() * 233/100), int(image_label.height() * 230 / 100))

                label_4 = QLabel(empty_label2)
                label_4.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_4.move( int(empty_label2.width() * 475 / 100), int(empty_label2.height() * 56 / 100))
                label_4.setFixedHeight(int(empty_label2.height() * 41 / 100))
                label_4.setFixedWidth(int(empty_label2.width() * 435 / 100))
                title_label_4 = QLabel("Female Out (current)", label_4)
                title_label_4.setStyleSheet("color: white; border: None")
                title_label_4.setFont(
                    QFont("Times", int(24 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_4.move( int(image_label.width() * 10 / 100), int(image_label.height() * 40 / 100))

                value_label_4 = QLabel(self.add_zero(self.cameras[row][4][0]['gender_female_out']), label_4)
                value_label_4.setStyleSheet("color: white; border: None")
                value_label_4.setFont(
                    QFont("Times", int(64 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_4.move( int(image_label.width() * 233/100), int(image_label.height() * 230 / 100))
                self.layout_4.append({'title_label_1':title_label_1, 'value_label_1': value_label_1, 'title_label_2':title_label_2, 'value_label_2': value_label_2, 'title_label_3':title_label_3, 'value_label_3': value_label_3, 'title_label_4':title_label_4, 'value_label_4': value_label_4})

            elif self.cameras[row][2] == 1:
                label_1 = QLabel(empty_label2)
                label_1.setStyleSheet("border: 1px solid white; color: white; border-radius: 10px;")
                label_1.move( int(empty_label2.width() * 15 / 100), int(empty_label2.height() * 11 / 100))
                label_1.setFixedHeight(int(empty_label2.height() * 86 / 100))
                label_1.setFixedWidth(int(empty_label2.width() * 895 / 100))
                title_label_1 = QLabel("Total All Zone", label_1)
                title_label_1.setStyleSheet("color: white; border: None")
                title_label_1.setFont(
                    QFont("Times", int(68 * min(self.width_ratio, self.height_ratio)))
                )
                title_label_1.move( int(image_label.width() * 40 / 100), int(image_label.height() * 40 / 100))

                value_label_1 = QLabel(self.add_zero(self.cameras[row][4][0]['zone_total_all_zones']), label_1)
                value_label_1.setStyleSheet("color: white; border: None")
                value_label_1.setFont(
                    QFont("Times", int(120 * min(self.width_ratio, self.height_ratio)))
                )
                value_label_1.move( int(image_label.width() * 500/100), int(image_label.height() * 550 / 100))
                self.layout_1.append({'title_label_1':title_label_1, 'value_label_1': value_label_1})
                
            self.grid_layout.addWidget(empty_label2, row + 1, 1)

        self.timer = QTimer()
        self.timer.timeout.connect(self.update_detail_camera)
        self.timer.start(25)

    def display_frames(self):
        for i, (id, type_cam) in enumerate(self.captures):
            # while True:
                # Use the proxy object directly without calling it
                # print(manager.get_processed_frame_queue(id_camera, type_camera))
                frame = self.manager.get_processed_frame_queue(id, type_cam).get()
                frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = frame_rgb.shape
                bytes_per_line = ch * w
                q_image = QImage(frame_rgb.data, w, h, bytes_per_line, QImage.Format_RGB888)

                # Tampilkan frame di dalam label yang sesuai
                if i < len(self.image_labels):
                    pixmap = QPixmap.fromImage(q_image)
                    self.image_labels[i].setPixmap(pixmap)
            # else:
            #     print("gagla")

    def update_detail_camera(self):
        for row in range(len(self.cameras)):
            if self.cameras[row][5] == 0:
                continue  # Melompati iterasi jika i adalah bilangan genap
            total_kotak_cam = self.cameras[row][2]
            if total_kotak_cam == 1:
                data_monitoring = self.get_data_monitoring_camera('setting_camera_area', self.cameras[row][6])
                zone_total_all_zones = data_monitoring[21]
                for lay1 in self.layout_1:
                    # lay1['title_label_1'].setText('')
                    lay1['value_label_1'].setText(self.add_zero(zone_total_all_zones[2]))

            elif total_kotak_cam == 2:
                data_monitoring = self.get_data_monitoring_camera('setting_camera', self.cameras[row][6])
                # print(data_monitoring)
                visitor_daily_in = data_monitoring[26]
                visitor_daily_out = data_monitoring[27]
                for lay2 in self.layout_2:
                    # lay1['title_label_1'].setText('')
                    lay2['value_label_1'].setText(self.add_zero(visitor_daily_in))
                    # lay1['title_label_2'].setText('')
                    lay2['value_label_2'].setText(self.add_zero(visitor_daily_out))
            elif total_kotak_cam == 3:
                data_monitoring = self.get_data_monitoring_camera('setting_camera', self.cameras[row][6])
                outer_traffic_left = data_monitoring[28]
                outer_traffic_right = data_monitoring[29]
                outer_traffic_total_traffic = data_monitoring[30]
                for lay3 in self.layout_3:
                    # lay1['title_label_1'].setText('')
                    lay3['value_label_1'].setText(self.add_zero(outer_traffic_left))
                    # lay1['title_label_2'].setText('')
                    lay3['value_label_2'].setText(self.add_zero(outer_traffic_right))
                    # lay1['title_label_3'].setText('')
                    lay3['value_label_3'].setText(self.add_zero(outer_traffic_total_traffic))
            elif total_kotak_cam == 4:
                gender_male_in = self.get_cosntant_by_code('gender_male_in')
                gender_male_out = self.get_cosntant_by_code('gender_male_out')
                gender_female_in = self.get_cosntant_by_code('gender_female_in')
                gender_female_out = self.get_cosntant_by_code('gender_female_out')
                for lay4 in self.layout_4:
                    # lay1['title_label_1'].setText('')
                    lay4['value_label_1'].setText(self.add_zero(gender_male_in[2]))
                    # lay1['title_label_2'].setText('')
                    lay4['value_label_2'].setText(self.add_zero(gender_male_out[2]))
                    # lay1['title_label_3'].setText('')
                    lay4['value_label_3'].setText(self.add_zero(gender_female_in[2]))
                    # lay1['title_label_4'].setText('')
                    lay4['value_label_4'].setText(self.add_zero(gender_female_out[2]))

    def top_navbar(self):
        top_bar = QLabel(self.navbar)
        top_bar.setStyleSheet("background-color: #000000;")
        # top_bar.setGeometry(0, 0,self.navbar.width(), int(63 * self.height_ratio))
        top_bar.move(0, 0)
        top_bar.setFixedWidth(int(self.navbar.width()))
        top_bar.setFixedHeight(int(63 * self.height_ratio))

        self.icon1_label = QLabel(self.navbar)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.move(int(1206 * self.width_ratio), int(17 * self.height_ratio))
        self.icon1_label.setFixedWidth(int(24 * self.width_ratio))
        self.icon1_label.setFixedHeight(int(24 * self.height_ratio))
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        )         
        self.icon1_label.mousePressEvent = self.minimizeWindow

        self.icon2_label = QLabel(self.navbar)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")        
        self.icon2_label.move(int(1232 * self.width_ratio),int(15 * self.height_ratio))
        self.icon2_label.setFixedWidth(int(33 * self.width_ratio))
        self.icon2_label.setFixedHeight(int(33 * self.height_ratio))
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        ) 
        self.icon3_label = QLabel(self.navbar)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")        
        self.icon3_label.move(int(1252 * self.width_ratio),int(9 * self.height_ratio))
        self.icon3_label.setFixedWidth(int(45 * self.width_ratio))
        self.icon3_label.setFixedHeight(int(45 * self.height_ratio))
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  

        self.icon3_label.mousePressEvent = self.openKeluarWindow

        self.control_panel_label = QLabel("Monitoring View", self.navbar)
        self.control_panel_label.setStyleSheet("color: white; background-color: #000000;")
        self.control_panel_label.setFont(
            QFont("Times", int(23 * min(self.width_ratio, self.height_ratio)))
        )
        self.control_panel_label.move(int(30 * self.width_ratio), int(10 * self.height_ratio))
        self.control_panel_label.setFixedWidth(int(350 * self.width_ratio))
        self.control_panel_label.setFixedHeight(int(45 * self.height_ratio))

        self.GButton_21 = QPushButton(self.navbar)        
        self.GButton_21.move(int(1070 * self.width_ratio),int(80 * self.height_ratio))
        self.GButton_21.setFixedWidth(int(191 * self.width_ratio))
        self.GButton_21.setFixedHeight(int(51 * self.height_ratio))
        self.GButton_21.setStyleSheet(
            "background-color: #1e9fff; color: white; font-family: Times; font-size: 16px; border-radius: 10px;"
        )
        self.GButton_21.setText("Back to Control Panel")
        self.GButton_21.clicked.connect(self.GButton_21_command)

    def GButton_21_command(self):
        # for capture in self.captures:
        #     capture.release()
        # current_directory = os.path.dirname(os.path.abspath(__file__))            
        # panel_path = os.path.join(current_directory, "home.pyc")            
        # # subprocess.Popen(["python", panel_path])
        # self.home = App_home(panel_path)
        # self.home.show()
        # self.home.closed.connect(self.close)
        # self.close()
        # self.timer.stop()
        self.control_panel_clicked.emit()

    def minimizeWindow(self, event):
        self.minimize_clicked.emit()

    def openKeluarWindow(self, event):
        self.close_clicked.emit()
        # current_directory = os.path.dirname(os.path.abspath(__file__))
        # panel_path = os.path.join(current_directory, "keluar.py")
        # subprocess.Popen(["python", panel_path])
    
    def add_zero(self, num):
        # Konversi angka menjadi string
        num_str = str(num)
        
        # Jika jumlah karakter kurang dari 2, tambahkan '0' di depan angka
        if len(num_str) == 1:
            num_str = '00' + num_str
        elif len(num_str) == 2:
            num_str = '0' + num_str
        else :
            num_str = num_str
        return num_str
    
    def get_camera_info(self, data_camera, camera_source, index_rtsp, type_cam, total_kotak, index_display, index_nama, index_id, index_type_cam):
        nomor = 1
        # books = [('camera in out 1','A01','rtsp://camera:12345678@192.168.100.140:554/stream1','DKY005',"People-Counting",'1')]
        for cam in camera_source:            
            rtsp_cam = cam[index_rtsp]
            nama_cam = cam[index_nama]
            total_kotak_cam = cam[total_kotak]
            display = cam[index_display]
            id_cam = cam[index_id]
            type_cam_index = cam[index_type_cam]
            data = []
            if total_kotak_cam == 1:
                data_monitoring = self.get_data_monitoring_camera('setting_camera_area', id_cam)
                # zone_total_all_zones = self.get_cosntant_by_code('zone_total_all_zones')
                zone_total_all_zones = data_monitoring[21]
                data.append({'zone_total_all_zones' : zone_total_all_zones})
            elif total_kotak_cam == 2:
                data_monitoring = self.get_data_monitoring_camera('setting_camera', id_cam)
                # visitor_daily_in = self.get_cosntant_by_code('visitor_daily_in')
                # visitor_daily_out = self.get_cosntant_by_code('visitor_daily_out')
                visitor_daily_in = data_monitoring[26]
                visitor_daily_out = data_monitoring[27]
                data.append({'visitor_daily_in' : visitor_daily_in, 'visitor_daily_out' : visitor_daily_out})
            elif total_kotak_cam == 3:
                data_monitoring = self.get_data_monitoring_camera('setting_camera', id_cam)
                # outer_traffic_left = self.get_cosntant_by_code('outer_traffic_left')
                # outer_traffic_right = self.get_cosntant_by_code('outer_traffic_right')
                # outer_traffic_total_traffic = self.get_cosntant_by_code('outer_traffic_total_traffic')
                outer_traffic_left = data_monitoring[28]
                outer_traffic_right = data_monitoring[29]
                outer_traffic_total_traffic = data_monitoring[30]
                data.append({'outer_traffic_left' : outer_traffic_left, 'outer_traffic_right' : outer_traffic_right, 'outer_traffic_total_traffic' : outer_traffic_total_traffic})
            elif total_kotak_cam == 4:
                gender_male_in = self.get_cosntant_by_code('gender_male_in')
                gender_male_out = self.get_cosntant_by_code('gender_male_out')
                gender_female_in = self.get_cosntant_by_code('gender_female_in')
                gender_female_out = self.get_cosntant_by_code('gender_female_out')
                data.append({'gender_male_in' : gender_male_in[2], 'gender_male_out' : gender_male_out[2], 'gender_female_in' : gender_female_in[2], 'gender_female_out' : gender_female_out[2]})

            camera_info = (                
                rtsp_cam,                
                type_cam,
                total_kotak_cam,
                nama_cam,
                data,
                display,
                id_cam,
                type_cam_index,
            )
            nomor +=1
            data_camera.append(camera_info)
        return data_camera
    
    def get_data_camera(self, table):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM {table} where deleted=1 and display=1').fetchall()
        return data
    
    def get_data_monitoring_camera(self, table, id_data):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM {table} where id={id_data}').fetchone()
        return data

    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = MainWindowMonitoring('')
#     window.show()
#     sys.exit(app.exec_())
