import sys
import os
import subprocess
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QMessageBox
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal
from sidebar import App_sidebar
import sqlite3
from datetime import datetime

class App_record_camera(QMainWindow):
    add_edit_record_clicked = pyqtSignal(int, int, int)
    preview_record_clicked = pyqtSignal(int)

    def __init__(self):
        super().__init__()
        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()  # Panggil update_layout untuk pengaturan awal
        self.update_content()
        
    def update_content(self):
        self.menu4()
    
    def menu4(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        # Main Content Area
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(270 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" Data Record Camera ")

        add_data_button = QPushButton("Add Data", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1136 * width_ratio),
            int(124 * height_ratio),
            int(87 * width_ratio),
            int(43 * height_ratio),
        )
        
        add_data_button.clicked.connect(self.emit_menu_add_record_clicked)    
        self.table_widget = QTableWidget(self)
        self.table_widget.setGeometry(
            int(312 * width_ratio),
            int(191 * height_ratio),
            int(921 * width_ratio),
            int(510 * height_ratio),
        )
        self.table_widget.setColumnCount(8)
        self.table_widget.setStyleSheet(
            """
            QTableWidget {
                background-color: #3d3d3d;
                border: none;
                font-family: arial;
                font-size: 18px;
                
            }
            QTableWidget::item {
                padding: 10px;
                border-bottom: 1px solid white; /* Set border color to white */
                color: white;
                font-family: arial;
                font-size: 18px;
            }
            QHeaderView::section {
                background-color: black; /* Set header background to black */
                color: white; 
                border: none;
                font-weight: 400;
                
                padding: 10px;
                font-family: arial;
                font-size: 20px;
                
            }
            
            """
        )

        self.table_widget.setHorizontalHeaderLabels(
            ["No", "Camera Type", "Name Camera", "Date", "Start Time", "End Time", "Status", "Opsi"]
        )

        header = self.table_widget.horizontalHeader()
        header.setStyleSheet(
            """
            background-color: black; /* Set header background to black */
            color: white; /* Set header text color to white */

           
            border: none;
            """
        )

        self.table_widget.setShowGrid(False)
        self.table_widget.verticalHeader().setVisible(False)

        self.table_widget.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)

        self.table_widget.horizontalHeader().resizeSection(0, int(35 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(1, int(100 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(2, int(320 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(3, int(80 * width_ratio))  # Atur lebar kolom 3 secara manual
        self.table_widget.horizontalHeader().resizeSection(4, int(80 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(5, int(80 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(6, int(140 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(7, int(80 * width_ratio))

        # self.table_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.add_data_to_table()

    def get_data_camera_by_type_id(self, tabel, id):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"SELECT * FROM {tabel} where id = {id}")
        data = cursor.fetchone()
        conn.commit()                
        conn.close()

        return data
    
    def add_data_to_table(self):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute("SELECT * FROM t_record where deleted = 1 ORDER BY id desc")
        dataall = cursor.fetchall()
        conn.commit()                
        conn.close()
        books = []
        camera_type = ""
        name_camera = ""
        status = ""
        for data in dataall:
            if data[1] == 1:
                data_camera = self.get_data_camera_by_type_id("setting_camera", data[2])
                camera_type = "Visitor"
                name_camera = data_camera[1]

            elif data[1] == 2:
                data_camera = self.get_data_camera_by_type_id("setting_camera_gender", data[2])
                camera_type = "Gender"
                name_camera = data_camera[1]

            elif data[1] == 3:
                data_camera = self.get_data_camera_by_type_id("setting_camera_area", data[2])
                camera_type = "Zone"
                name_camera = data_camera[1]

            elif data[1] == 4:
                data_camera = self.get_data_camera_by_type_id("setting_camera", data[2])
                camera_type = "Outer Traffic"
                name_camera = data_camera[1]

            
            tanggal_awal = data[3]            
            tanggal_objek = datetime.strptime(tanggal_awal, "%Y-%m-%d")
            tanggal = tanggal_objek.strftime("%d-%m-%Y")

            if data[7] == 0:
                status = "Not Processed Yet"
            elif data[7] == 1:
                status = "Successful"
            elif data[7] == 2:
                status = "In progress"
            elif data[7] == 3:
                status = "Failed"

            books.append((data[0], camera_type, name_camera, tanggal, data[4], data[5], status))

        for row, (id_record, cam_type, name_camera, tanggal, start_time, end_time, status) in enumerate(books):
            self.table_widget.insertRow(row)

            no = QTableWidgetItem(f"{row+ 1}")
            self.table_widget.setItem(row, 0, no)

            camera_type_title = QTableWidgetItem(cam_type)
            self.table_widget.setItem(row, 1, camera_type_title)

            nama_camera_title = QTableWidgetItem(name_camera)
            self.table_widget.setItem(row, 2, nama_camera_title)

            tanggal_title = QTableWidgetItem(tanggal)
            self.table_widget.setItem(row, 3, tanggal_title)
            
            start_title = QTableWidgetItem(start_time)
            self.table_widget.setItem(row, 4, start_title)            

            end_title = QTableWidgetItem(end_time)
            self.table_widget.setItem(row, 5, end_title) 

            status_title = QTableWidgetItem(status)
            self.table_widget.setItem(row, 6, status_title)            

            icon_widget = QWidget()
            icon_widget.setStyleSheet("QWidget { background-color: transparent;}")

            icon_layout = QHBoxLayout()

            if status == "Successful":
                self.button_edit_snapshot = QLabel()
                self.button_edit_snapshot.setPixmap(QPixmap("img/preview2.png").scaled(23, 23))
                self.button_edit_snapshot.setAlignment(Qt.AlignRight)
                icon_layout.addWidget(self.button_edit_snapshot)
                self.button_edit_snapshot.setStyleSheet("QLabel { background-color: #3d3d3d;}")
                self.button_edit_snapshot.mousePressEvent = lambda event, id_data=id_record: self.emit_menu_preview_record_clicked(id_data)

            if status == "Not Processed Yet":
                self.button_edit_snapshot = QLabel()
                self.button_edit_snapshot.setPixmap(QPixmap("img/edit.png").scaled(20, 20))
                self.button_edit_snapshot.setAlignment(Qt.AlignRight)
                icon_layout.addWidget(self.button_edit_snapshot)
                self.button_edit_snapshot.setStyleSheet("QLabel { background-color: #3d3d3d;}")
                self.button_edit_snapshot.mousePressEvent = lambda event, id_data=id_record: self.emit_menu_edit_record_clicked(id_data)

            clickable_icon_2 = QLabel()
            clickable_icon_2.setPixmap(QPixmap("img/del.png").scaled(20, 20))
            clickable_icon_2.setAlignment(Qt.AlignLeft)
            clickable_icon_2.setFixedHeight(20)
            icon_layout.addWidget(clickable_icon_2)
            clickable_icon_2.setStyleSheet("QLabel { background-color: #3d3d3d; }")
            clickable_icon_2.mousePressEvent = lambda event, id_data=id_record: self.show_delete_confirmation_popup(id_data)

            icon_widget.setLayout(icon_layout)

            self.table_widget.setCellWidget(row, 7, icon_widget)

            self.table_widget.setRowHeight(row, 60)
    
    def show_delete_confirmation_popup(self, id_data):
        # Membuat instance QMessageBox
        msg = QMessageBox()
        msg.setWindowTitle("Confirm Data Deletion")
        msg.setText("Are you sure you want to delete the data?")
        msg.setIcon(QMessageBox.Warning)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setDefaultButton(QMessageBox.Cancel)

        # Menampilkan popup dan mendapatkan respons pengguna
        response = msg.exec_()

        # Jika pengguna menekan tombol Ok, maka lakukan aksi penghapusan data
        if response == QMessageBox.Ok:
            print("Data berhasil dihapus!") 
            conn = sqlite3.connect('setting.db')
            cursor = conn.cursor()
            update_query = f"UPDATE t_record SET deleted = ? where id = ?"
            cursor.execute(update_query, ('0', id_data))
            conn.commit()        
            cursor.close()
            conn.close()
            self.clear_table()
            self.add_data_to_table()

    def clear_table(self):
        self.table_widget.clearContents()  # Menghapus konten tabel
        self.table_widget.setRowCount(0)  # Mengatur jumlah baris tabel menjadi 0

    def emit_menu_preview_record_clicked(self, id_data):
        print("Menu record preview clicked")
        self.preview_record_clicked.emit(id_data)

    def emit_menu_edit_record_clicked(self, id_data):
        # print("Menu edit cam list clicked")
        self.add_edit_record_clicked.emit(id_data, 0, 0)

    def emit_menu_add_record_clicked(self, event):
        # print("Menu add cam list clicked")
        self.add_edit_record_clicked.emit(0, 0, 0)

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_record_camera()
#     window.show()
#     sys.exit(app.exec_())
