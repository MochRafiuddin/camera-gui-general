# Define the paths to the .bat files
$batFile1 = "D:\PYTHON\camera-gui-general\kill_detection.bat"
$batFile2 = "D:\PYTHON\camera-gui-general\Camfig-Pro.bat"
$batFile3 = "D:\PYTHON\camera-gui-general\Camfig-Pro Live Preview.bat"

# Function to run a .bat file and wait for it to complete
function Run-BatFileAndWait {
    param (
        [string]$batFilePath
    )

    Write-Host "Running $batFilePath..."
    # Start the .bat file process and wait for it to complete
    Start-Process -FilePath $batFilePath -NoNewWindow -Wait
    Write-Host "$batFilePath completed."
}

# Function to run a .bat file without waiting for it to complete
function Run-BatFileWithoutWait {
    param (
        [string]$batFilePath
    )

    Write-Host "Running $batFilePath without waiting..."
    # Start the .bat file process without waiting for it to complete
    Start-Process -FilePath $batFilePath -NoNewWindow
    Write-Host "$batFilePath started."
}

# Run the first .bat file and wait for it to complete
Run-BatFileAndWait -batFilePath $batFile1

# Run the third .bat file without waiting for it to complete
Run-BatFileWithoutWait -batFilePath $batFile3

# Run the second .bat file without waiting for it to complete
Run-BatFileWithoutWait -batFilePath $batFile2

Write-Host "All .bat files started."
