import sys
import os
import subprocess
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QLineEdit
from PyQt5.QtGui import QPixmap, QFont, QIcon, QPainter, QColor, QBrush
from PyQt5.QtCore import QSize, Qt, QRect
import sqlite3
from home import App_home

class App(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Login")
        self.setStyleSheet("background-color: #222222;")
        self.original_resolution = QSize(1280, 832)

        # Mendapatkan ukuran layar
        screen_geometry = QApplication.desktop().screenGeometry()
        self.setFixedSize(screen_geometry.width(), screen_geometry.height())

        self.logo_image_path = "img/Logo-CCTV.png"
        self.logo_image = QPixmap(self.logo_image_path)        
        self.logo_label = QLabel(self)
        self.logo_label.setPixmap(self.logo_image)
        self.logo_label.setStyleSheet("background-color: #222222; opacity: 0;")
        # self.logo_label.setGeometry(550, 91, 251, 112)  #

        # Label judul
        # self.title_label = QLabel(self)  # membuat label judul menjadi atribut
        # self.title_label.setText("    Advance AI Cam App")
        # self.title_label.setFont(QFont("Inter", 32))
        # self.title_label.setStyleSheet("font-weight: 600; color: white;")
        # self.title_label.setAlignment(Qt.AlignCenter)

        # Label dan input field untuk username
        self.username_label = QLabel(self)
        self.username_label.setText("Username")
        self.username_label.setFont(QFont("Inter", 21))
        self.username_label.setStyleSheet("font-weight: 400; color: white;")
        self.username_label.setGeometry(440, 380, 140, 25)

        self.username_input = QLineEdit(self)
        self.username_input.setStyleSheet(
            "background-color: #454447; border: none; color: white; font-family: Inter; font-size: 27px; border-radius: 5px;"
        )
        self.username_input.setGeometry(420, 410, 434, 53)
        self.username_input.setText("admin")

        # Label dan input field untuk password
        self.password_label = QLabel(self)
        self.password_label.setText("Password")
        self.password_label.setFont(QFont("Inter", 21))
        self.password_label.setStyleSheet("font-weight: 400; color: white")
        self.password_label.setGeometry(420, 500, 140, 25)

        self.password_input = QLineEdit(self)
        self.password_input.setEchoMode(QLineEdit.Password)
        self.password_input.setStyleSheet(
            "background-color: #454447; border: none; border-radius: 5px; color: white; font-family: Inter; font-size: 27px;"
        )
        self.password_input.setGeometry(420, 532, 434, 53)
        self.password_input.setText("admin")

        self.show_password_button = QPushButton(self)
        self.show_password_button.setIcon(QIcon("img/logomata.png"))
        self.show_password_button.setGeometry(815, 532, 40, 53)
        self.show_password_button.setStyleSheet(
            "background-color: #454447; border: none; border-radius: 5px;"
        )
        self.show_password_button.setCheckable(True)
        self.show_password_button.clicked.connect(self.toggle_password_echo)

        icon_size = QSize(30, 30)
        self.show_password_button.setIconSize(icon_size)
        # Button Login
        self.login_button = QPushButton(self)
        self.login_button.setText("Login")
        self.login_button.setStyleSheet(
            "background-color: #29282b; color: white; font-family: Inter; font-size: 20px; border-radius: 5px;"
        )
        self.login_button.setGeometry(420, 630, 437, 51)
        self.login_button.clicked.connect(self.login_button_clicked)

        self.forgot_label = QLabel(self)
        self.forgot_label.setText(
            "<span style='color:white;'>Forgot your credential?</span><span style='color:#1e90ff;'> Contact Support</span>"
        )
        self.forgot_label.setFont(QFont("Inter", 16))
        self.forgot_label.setStyleSheet("font-weight: 400;")
        self.forgot_label.setGeometry(500, 762, 310, 19)

        self.rect_label = QLabel("Login", self)
        self.rect_label.setGeometry(26, 17, 63, 29)
        self.rect_label.setStyleSheet(
            "background-color: #000000; color: #FFFFFF; font-size: 24px; font-family: Inter; font-weight: 400;"
        )

        # Rectangle header
        self.header_rect = QRect(0, 0, self.width(), 63)

        self.update_layout()

    def update_layout(self):
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / self.original_resolution.width()
        height_ratio = screen_geometry.height() / self.original_resolution.height()
        # aptikma123

        # Perbarui posisi dan ukuran elemen GUI dengan memperhitungkan rasio resolusi
        self.logo_label.setGeometry(
            int(450 * width_ratio),
            int(91 * height_ratio),
            int(400 * width_ratio),
            int(125 * height_ratio),
        )
        # self.title_label.setGeometry(
        #     int(440 * width_ratio),
        #     int(240 * height_ratio),
        #     int(500 * width_ratio),
        #     int(41 * height_ratio),
        # )
        self.username_label.setGeometry(
            int(420 * width_ratio),
            int(380 * height_ratio),
            int(140 * width_ratio),
            int(25 * height_ratio),
        )
        self.username_input.setGeometry(
            int(420 * width_ratio),
            int(410 * height_ratio),
            int(434 * width_ratio),
            int(53 * height_ratio),
        )
        self.password_label.setGeometry(
            int(420 * width_ratio),
            int(500 * height_ratio),
            int(140 * width_ratio),
            int(25 * height_ratio),
        )
        self.password_input.setGeometry(
            int(420 * width_ratio),
            int(532 * height_ratio),
            int(434 * width_ratio),
            int(53 * height_ratio),
        )
        self.show_password_button.setGeometry(
            int(815 * width_ratio),
            int(532 * height_ratio),
            int(40 * width_ratio),
            int(53 * height_ratio),
        )
        self.login_button.setGeometry(
            int(420 * width_ratio),
            int(630 * height_ratio),
            int(437 * width_ratio),
            int(51 * height_ratio),
        )
        self.forgot_label.setGeometry(
            int(500 * width_ratio),  # left
            int(762 * height_ratio),  # top
            int(310 * width_ratio),  # width
            int(19 * height_ratio),  # height
        )

        self.icon1_label = QLabel(self)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.setGeometry(
            int(1206 * width_ratio),
            int(17 * height_ratio),
            int(24 * width_ratio),
            int(24 * height_ratio),
        )
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        )
        # self.icon1_label.setScaledContents(True)

        self.icon1_label.mousePressEvent = self.minimizeWindow

        self.icon2_label = QLabel(self)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon2_label.setGeometry(
            int(1232 * width_ratio),
            int(15 * height_ratio),
            int(33 * width_ratio),
            int(33 * height_ratio),
        )
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        )

        self.icon3_label = QLabel(self)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon3_label.setGeometry(
            int(1252 * width_ratio),
            int(9 * height_ratio),
            int(45 * width_ratio),
            int(45 * height_ratio),
        )
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  
        # self.icon3_label.setScaledContents(True)

        self.icon3_label.mousePressEvent = self.openKeluarWindow

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setPen(Qt.NoPen)

        painter.setBrush(QColor("#000000"))
        painter.drawRect(self.header_rect)

        painter.setFont(QFont("Inter", 14))
        painter.setBrush(QColor("#ffffff"))
        painter.drawText(QRect(26, 17, 63, 29), Qt.AlignCenter, "Login")

    def toggle_password_echo(self):
        if self.show_password_button.isChecked():
            self.password_input.setEchoMode(QLineEdit.Normal)
        else:
            self.password_input.setEchoMode(QLineEdit.Password)

    def login_button_clicked(self):
        username = self.username_input.text()
        password = self.password_input.text()
        # print("Username:", username)
        # print("Password:", password)
        # print("command")

        cek_login = self.login(username, password)
        
        if cek_login:
            current_directory = os.path.dirname(os.path.abspath(__file__))            
            panel_path = os.path.join(current_directory, "home.pyc")            
            # subprocess.Popen(["python", panel_path])
            self.new_window = App_home(panel_path)
            self.new_window.show()
            self.new_window.closed.connect(self.close)
            self.hide()

        else :
            self.username_input.clear()
            self.password_input.clear()

    # def open_new_window(self, path):
    #     self.new_window = MainWindow(path)
    #     self.new_window.show()
            
    def resizeEvent(self, event):
        # Panggil metode superclass untuk menangani perubahan ukuran jendela
        self.update_layout()

    ########################################################################################Proses jendela
    def minimizeWindow(self, event):
        # Minimize the window
        self.showMinimized()

    def openKeluarWindow(self, event):
        # Open keluar.py window
        current_directory = os.path.dirname(os.path.abspath(__file__))
        # Bentuk jalur absolut ke panel.py
        panel_path = os.path.join(current_directory, "keluar.py")

        # Mulai proses untuk menjalankan panel.py
        subprocess.Popen(["python", panel_path])
        self.close()

    ##############################################################################################

    def login(self, username, password):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()

        # Cari pengguna dengan username dan password yang cocok
        cursor.execute("SELECT * FROM users WHERE username = ? AND password = ?", (username, password))
        user = cursor.fetchone()

        if user:
            update_query = f"UPDATE users SET is_login = ? where id = ?"
            cursor.execute(update_query, ('1', user[0]))
            conn.commit()        
            # Tutup koneksi
            conn.close()

            # Jika pengguna ditemukan, kembalikan True, jika tidak, kembalikan False
            return True
        else:
            return False
    
    def update_logout(self):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM users WHERE is_login = 1")
        user = cursor.fetchone()

        update_query = f"UPDATE users SET is_login = ? where id = ?"
        cursor.execute(update_query, ('0', user[0]))
        conn.commit()        
        cursor.close()
        conn.close()

        return True

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = App()
    window.showFullScreen()  # Tampilan menjadi fullscreen
    sys.exit(app.exec_())
