# Mendapatkan daftar proses terminal (cmd dan PowerShell)
$terminalProcesses = Get-Process | Where-Object { $_.ProcessName -eq "cmd" }

# Menampilkan daftar terminal yang terbuka beserta judul jendela
if ($terminalProcesses) {
    Write-Host "Daftar terminal yang terbuka:"
    $terminalProcesses | ForEach-Object {
        $windowTitle = $_.MainWindowTitle
        if ($windowTitle -eq "") {
            $windowTitle = "<No Window Title>"
        }
        Write-Host $_.ProcessName $_.Id $windowTitle
    }
} else {
    Write-Host "Tidak ada terminal yang terbuka."
}
