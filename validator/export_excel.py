import openpyxl
from openpyxl.styles import Font, Alignment, PatternFill

class ExcelReport:
    def __init__(self):
        self.wb = openpyxl.Workbook()
        self.ws = self.wb.active
        self.header_font = Font(bold=True, color="000000")
        self.header_fill = PatternFill(start_color="00C0C0C0", end_color="00C0C0C0", fill_type="solid")
        self.data_fill = PatternFill(start_color="00E5E5E5", end_color="00E5E5E5", fill_type="solid")
        self.center_alignment = Alignment(horizontal="center")
        self.bold_font = Font(bold=True)

    def set_column_widths(self):
        column_widths = {
            'A': 10, 'B': 30, 'C': 15, 'D': 40, 'E': 12, 'F': 20, 
            'G': 12, 'H': 15, 'I': 15, 'J': 15, 'K': 15, 'L': 20, 
            'M': 20, 'N': 15, 'O': 15
        }
        for col, width in column_widths.items():
            self.ws.column_dimensions[col].width = width

    def add_customer_data(self):
        self.ws.merge_cells('A2:C2')
        self.ws['A2'] = "CUSTOMER DATA"
        self.ws['A2'].font = self.bold_font
        self.ws['A3'] = "Lokasi"
        self.ws['B3'] = "Living World Kota Wisata"

    def add_system_data(self):
        self.ws['A4'] = "System :"
        self.ws['A4'].font = self.bold_font
        self.ws['B4'] = "Brickstream 2500"

    def add_table_header(self):
        headers = [
            "Number", "Site", "Device Name", "File Name", "Date", "Recording Time", 
            "Duration (Jam)", "Brickstream IN Count", "Visual IN Count", "Brickstream OUT Count", 
            "Visual OUT Count", "Difference IN Calculation", "Difference OUT Calculation", 
            "Accuracy IN", "Accuracy OUT", "Verify By"
        ]
        for col_num, header in enumerate(headers, start=1):
            # print(col_num, header)
            cell = self.ws.cell(row=6, column=col_num, value=header)
            cell.font = self.header_font
            cell.fill = self.header_fill
            cell.alignment = self.center_alignment

    def add_data(self, data):
        for row_num, row_data in enumerate(data, start=7):
            for col_num, cell_value in enumerate(row_data, start=1):
                cell = self.ws.cell(row=row_num, column=col_num, value=cell_value)
                if col_num == 1:
                    cell.font = self.bold_font
                if row_num % 2 == 0:
                    cell.fill = self.data_fill
                cell.alignment = self.center_alignment

    def save(self, filename):
        self.wb.save(filename)
