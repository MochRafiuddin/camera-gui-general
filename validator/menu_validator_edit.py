import sys
import os
import subprocess
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QLineEdit,
    QMessageBox,
    QFileDialog
)
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QFont, QPixmap
from PyQt5.QtCore import pyqtSignal, QDate, QTime
from datetime import datetime

import sqlite3

class App_record_camera_edit(QMainWindow):
    back_record_clicked = pyqtSignal()
    save_record_clicked = pyqtSignal()
    minimize_clicked = pyqtSignal()
    close_clicked = pyqtSignal()

    def __init__(self):
        super().__init__()
        # self.set_parameter(id_data)

    def set_parameter(self, id_data):
        self.label_menu = " Add Validator "
        self.id_data = id_data

        self.site = ""
        self.device_name = ""
        self.filename = ""
        self.date = ""
        self.record_time = ""
        self.duration = ""
        self.system_in = ""
        self.system_out = ""
        self.verify_by = ""

        if id_data != 0:
            self.label_menu = " Edit Validator "
            conn = sqlite3.connect('db_validator.db')
            cursor = conn.cursor()        
            cursor.execute(f"SELECT * FROM t_validator WHERE id = {id_data}")
            data = cursor.fetchone()
            conn.commit()                
            conn.close()
            
            self.site = data[1]
            self.device_name = data[2]
            self.filename = data[3]
            self.date = data[4]
            self.record_time = data[5]
            self.duration = data[6]
            self.system_in = data[7]
            self.system_out = data[9]
            self.verify_by = data[15]

        self.update_form()  # Panggil update_form untuk pengaturan awal
        self.top_bar()  # Panggil update_form untuk pengaturan awal

    def update_form(self):
        self.screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = self.screen_geometry.width() / 1280
        self.height_ratio = self.screen_geometry.height() / 832
        # Main Content Area
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(20 * self.width_ratio),
            int(90 * self.height_ratio),
            int(1238 * self.width_ratio),
            int(699 * self.height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(30 * self.width_ratio),
            int(105 * self.height_ratio),
            int(250 * self.width_ratio),
            int(43 * self.height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(self.label_menu)

        self.label1 = QtWidgets.QLabel(self)
        self.label1.setFont(QtGui.QFont("Inter", 15))
        self.label1.setText("Site")
        self.label1.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label1.setGeometry(70, 240, 170, 36)
        self.input_site = QtWidgets.QLineEdit(self)
        self.input_site.setFont(QtGui.QFont("Inter", 16))
        self.input_site.setGeometry(230, 240, 400, 36)
        self.input_site.setText(self.site)
        self.input_site.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")

        self.label2 = QtWidgets.QLabel(self)
        self.label2.setFont(QtGui.QFont("Inter", 15))
        self.label2.setText("Device Name")
        self.label2.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label2.setGeometry(70, 320, 170, 36)
        self.input_devicename = QtWidgets.QLineEdit(self)
        self.input_devicename.setFont(QtGui.QFont("Inter", 16))
        self.input_devicename.setGeometry(230, 320, 400, 36)
        self.input_devicename.setText(self.device_name)
        self.input_devicename.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("File Video")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(70, 400, 170, 36)
        self.input_filename = QtWidgets.QLineEdit(self)
        self.input_filename.setFont(QtGui.QFont("Inter", 16))
        self.input_filename.setGeometry(230, 400, 400, 36)
        self.input_filename.setText(self.filename)
        self.input_filename.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")
        browse_button = QPushButton("Browse", self)
        browse_button.setGeometry(640, 400, 80, 36)
        browse_button.setStyleSheet("background-color: #1e9fff; color: white; border-radius: 5px;")
        browse_button.clicked.connect(self.browse_file)

        self.label_cam = QtWidgets.QLabel(self)
        self.label_cam.setFont(QtGui.QFont("Inter", 15))
        self.label_cam.setText("File Cam")
        self.label_cam.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label_cam.setGeometry(70, 480, 170, 36)
        self.input_filecam = QtWidgets.QLineEdit(self)
        self.input_filecam.setFont(QtGui.QFont("Inter", 16))
        self.input_filecam.setGeometry(230, 480, 400, 36)
        self.input_filecam.setText(self.filename)
        self.input_filecam.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")
        self.browse_cam_button = QPushButton("Browse", self)
        self.browse_cam_button.setGeometry(640, 480, 80, 36)
        self.browse_cam_button.setStyleSheet("background-color: #1e9fff; color: white; border-radius: 5px;")
        self.browse_cam_button.clicked.connect(self.browse_filecam)

        self.label4 = QLabel(self)
        self.label4.setFont(QFont("Inter", 15))
        self.label4.setText("Date")
        self.label4.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label4.setGeometry(70, 480, 170, 36)
        self.input_date = QtWidgets.QDateEdit(self)
        self.input_date.setFont(QFont("Inter", 16))
        self.input_date.setGeometry(230, 480, 400, 36)
        self.input_date.setStyleSheet("QDateEdit {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}")
        self.input_date.setCalendarPopup(True)
        self.input_date.setDate(QtCore.QDate.currentDate())

        self.label5 = QLabel(self)
        self.label5.setFont(QFont("Inter", 15))
        self.label5.setText("Record Time")
        self.label5.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label5.setGeometry(70, 560, 170, 36)
        self.input_record_time = QtWidgets.QTimeEdit(self)
        self.input_record_time.setFont(QFont("Inter", 16))
        self.input_record_time.setGeometry(230, 560, 400, 36)
        self.input_record_time.setStyleSheet("QTimeEdit {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}")
        self.input_record_time.setDisplayFormat("HH:mm:ss")
        self.input_record_time.setTime(QtCore.QTime.currentTime())

        self.label6 = QtWidgets.QLabel(self)
        self.label6.setFont(QtGui.QFont("Inter", 15))
        self.label6.setText("Duration")
        self.label6.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label6.setGeometry(70, 640, 170, 36)
        self.input_duration = QtWidgets.QTimeEdit(self)
        self.input_duration.setFont(QtGui.QFont("Inter", 16))
        self.input_duration.setGeometry(230, 640, 400, 36)
        self.input_duration.setStyleSheet("QTimeEdit {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}")
        self.input_duration.setDisplayFormat("HH:mm:ss")
        self.input_duration.setTime(QtCore.QTime(0, 0, 0))  # Set time to 00:00:00

        self.label7 = QtWidgets.QLabel(self)
        self.label7.setFont(QtGui.QFont("Inter", 15))
        self.label7.setText("System IN")
        self.label7.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label7.setGeometry(70, 720, 170, 36)
        self.input_system_in = QtWidgets.QLineEdit(self)
        self.input_system_in.setFont(QtGui.QFont("Inter", 16))
        self.input_system_in.setGeometry(230, 720, 400, 36)
        self.input_system_in.setText(str(self.system_in))
        self.input_system_in.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")

        self.label8 = QtWidgets.QLabel(self)
        self.label8.setFont(QtGui.QFont("Inter", 15))
        self.label8.setText("System OUT")
        self.label8.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label8.setGeometry(70, 800, 170, 36)
        self.input_system_out = QtWidgets.QLineEdit(self)
        self.input_system_out.setFont(QtGui.QFont("Inter", 16))
        self.input_system_out.setGeometry(230, 800, 400, 36)
        self.input_system_out.setText(str(self.system_out))
        self.input_system_out.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")

        self.label9 = QtWidgets.QLabel(self)
        self.label9.setFont(QtGui.QFont("Inter", 15))
        self.label9.setText("Verify By")
        self.label9.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        # self.label9.setGeometry(70, 880, 170, 36)
        self.input_verify_by = QtWidgets.QLineEdit(self)
        self.input_verify_by.setFont(QtGui.QFont("Inter", 16))
        # self.input_verify_by.setGeometry(230, 880, 400, 36)
        self.input_verify_by.setText(str(self.verify_by))
        self.input_verify_by.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")

        add_data_button = QPushButton("Back", self)
        add_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1175 * self.width_ratio),
            int(730 * self.height_ratio),
            int(60 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        add_data_button.clicked.connect(self.back_button_clicked)

        add_data_button = QPushButton("Save", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1100 * self.width_ratio),
            int(730 * self.height_ratio),
            int(60 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        add_data_button.clicked.connect(self.save_button_clicked)

        if self.id_data != 0:
            self.label_cam.setVisible(False)
            self.input_filecam.setVisible(False)
            self.browse_cam_button.setVisible(False)
            self.set_date_time(self.date, self.record_time, self.duration)
            self.label9.setGeometry(70, 880, 170, 36)
            self.input_verify_by.setGeometry(230, 880, 400, 36)
        else:
            self.label4.setVisible(False)
            self.input_date.setVisible(False)
            self.label5.setVisible(False)
            self.input_record_time.setVisible(False)
            self.label6.setVisible(False)
            self.input_duration.setVisible(False)
            self.label7.setVisible(False)
            self.input_system_in.setVisible(False)
            self.label8.setVisible(False)
            self.input_system_out.setVisible(False)            
            self.label_cam.setVisible(True)
            self.input_filecam.setVisible(True)
            self.browse_cam_button.setVisible(True)
            self.label9.setGeometry(70, 560, 170, 36)
            self.input_verify_by.setGeometry(230, 560, 400, 36)

    def browse_file(self):
        file_name, _ = QFileDialog.getOpenFileName(self, "Select File")
        if file_name:
            self.input_filename.setText(file_name)

    def browse_filecam(self):
        file_name, _ = QFileDialog.getOpenFileName(self, "Select File", "", "Cam Files (*.cam)")
        if file_name:
            self.input_filecam.setText(file_name)
    
    def top_bar(self):
        # top bar
        top_bar = QLabel(self)
        top_bar.setStyleSheet("background-color: #000000;")
        top_bar.setGeometry(0, 0, self.screen_geometry.width(), int(63 * self.height_ratio))

        self.icon1_label = QLabel(self)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.setGeometry(
            int(1206 * self.width_ratio),
            int(17 * self.height_ratio),
            int(24 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        ) 
        self.icon1_label.mousePressEvent = self.minimize_window

        self.icon2_label = QLabel(self)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon2_label.setGeometry(
            int(1232 * self.width_ratio),
            int(15 * self.height_ratio),
            int(33 * self.width_ratio),
            int(33 * self.height_ratio),
        )
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        ) 

        self.icon3_label = QLabel(self)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon3_label.setGeometry(
            int(1252 * self.width_ratio),
            int(9 * self.height_ratio),
            int(45 * self.width_ratio),
            int(45 * self.height_ratio),
        )
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  
        self.icon3_label.mousePressEvent = self.on_close_button_clicked

        control_panel_label = QLabel("Validator Camera", self)
        control_panel_label.setStyleSheet("color: white; background-color: #000000;")
        control_panel_label.setFont(
            QFont("Times", int(23 * min(self.width_ratio, self.height_ratio)))
        )
        control_panel_label.setGeometry(
            int(30 * self.width_ratio),
            int(10 * self.height_ratio),
            int(280 * self.width_ratio),
            int(45 * self.height_ratio),
        )

    def set_date_time(self, date_str, start_time_str, duration_str):
        date = QtCore.QDate.fromString(date_str, "yyyy-MM-dd")
        start_time = QtCore.QTime.fromString(start_time_str, "HH:mm:ss")  
        input_record_time_conv = datetime.strptime(duration_str, "%H:%M:%S").time()        
              
        self.input_date.setDate(date)
        self.input_record_time.setTime(start_time)
        self.input_duration.setTime(input_record_time_conv)

    def back_button_clicked(self):
        self.back_record_clicked.emit()        

    def save_button_clicked(self):
        # Get input values
        site = self.input_site.text()
        device_name = self.input_devicename.text()
        filename = self.input_filename.text()
        filecam = self.input_filecam.text()
        date_str = self.input_date.date().toString("yyyy-MM-dd")
        record_time_str = self.input_record_time.time().toString("HH:mm:ss")
        duration = self.input_duration.time().toString("HH:mm:ss")
        system_in = self.input_system_in.text()
        system_out = self.input_system_out.text()
        verify_by = self.input_verify_by.text()

        conn = sqlite3.connect('db_validator.db')
        cursor = conn.cursor()

        if self.id_data == 0:
            if not site or not device_name or not filename or not filecam:
                msg = QMessageBox()
                msg.setWindowTitle("Invalid Input")
                msg.setText("All fields must be filled.")
                msg.setIcon(QMessageBox.Warning)
                msg.setStyleSheet("""
                    QMessageBox {
                        background-color: #222222;
                        color: white;
                    }
                    QMessageBox QLabel {
                        color: white;
                        font-family: 'Inter';
                        font-size: 12px;
                    }
                    QMessageBox QPushButton {
                        background-color: #444;
                        color: white;
                        border: 1px solid #555;
                    }
                    QMessageBox QPushButton:pressed {
                        background-color: #333;
                    }
                """)
                msg.exec_()
                return
            
            with open(filecam, 'r') as file:
                lines = file.read().strip().split('#')
                if len(lines) != 5:
                    self.show_message_box("File Error", "The file does not contain the expected format.", QMessageBox.Warning)
                    return
                date_str, record_time_str, duration_str, system_in, system_out = lines
            
            input_date_conv = datetime.strptime(date_str, "%Y-%m-%d").date()
            input_record_time_conv = datetime.strptime(record_time_str, "%H:%M:%S").time()

            cursor.execute("""
                INSERT INTO t_validator (
                    site, device_name, filename, date, record_time, duration, system_in, system_out, verify_by
                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
            """, (site, device_name, filename, str(input_date_conv), str(input_record_time_conv), duration_str, int(system_in), int(system_out), verify_by))
        else:
            if not site or not device_name or not filename or not duration or not system_in or not system_out or not verify_by:
                msg = QMessageBox()
                msg.setWindowTitle("Invalid Input")
                msg.setText("All fields must be filled.")
                msg.setIcon(QMessageBox.Warning)
                msg.setStyleSheet("""
                    QMessageBox {
                        background-color: #222222;
                        color: white;
                    }
                    QMessageBox QLabel {
                        color: white;
                        font-family: 'Inter';
                        font-size: 12px;
                    }
                    QMessageBox QPushButton {
                        background-color: #444;
                        color: white;
                        border: 1px solid #555;
                    }
                    QMessageBox QPushButton:pressed {
                        background-color: #333;
                    }
                """)
                msg.exec_()
                return
            
            # Convert strings to datetime objects
            input_date_conv = datetime.strptime(date_str, "%Y-%m-%d").date()
            input_record_time_conv = datetime.strptime(record_time_str, "%H:%M:%S").time()

            cursor.execute("""
                UPDATE t_validator
                SET site = ?, device_name = ?, filename = ?, date = ?, record_time = ?, duration = ?, system_in = ?, system_out = ?, verify_by = ?
                WHERE id = ?
            """, (site, device_name, filename, str(input_date_conv), str(input_record_time_conv), duration, int(system_in), int(system_out), verify_by, self.id_data))

        conn.commit()
        conn.close()

        self.save_record_clicked.emit()

    def minimize_window(self, event):
        self.minimize_clicked.emit() 

    def on_close_button_clicked(self, event):
        self.close_clicked.emit() 

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_snapshot_edit("8")
#     window.show()
#     sys.exit(app.exec_())
