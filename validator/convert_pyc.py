import py_compile
import os
import glob

files_to_compile = ["export_excel.py", "home_validator.py", "menu_validator_camera.py", "menu_validator_edit.py", "player_validator.py"]

for file in files_to_compile:
    # Kompilasi file
    py_compile.compile(file)
    
    # Dapatkan nama file asli tanpa ekstensi
    base_name = os.path.splitext(os.path.basename(file))[0]
    
    # Temukan file .pyc yang dihasilkan dalam direktori __pycache__
    pyc_file_pattern = os.path.join('__pycache__', f'{base_name}.cpython-*.pyc')
    pyc_files = glob.glob(pyc_file_pattern)
    
    for pyc_file in pyc_files:
        # Buat nama file baru tanpa bagian .cpython-311
        new_pyc_file = os.path.join('__pycache__', f'{base_name}.pyc')
        
        # Ubah nama file
        os.rename(pyc_file, new_pyc_file)