import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QStackedWidget, QMessageBox
from PyQt5.QtCore import Qt, pyqtSignal, QTimer

from menu_validator_camera import App_record_camera
from menu_validator_edit import App_record_camera_edit
from player_validator import CameraLayout

class App_home(QMainWindow):
    closed = pyqtSignal()

    def __init__(self, path):
        super().__init__()
        self.setWindowTitle("Control Panel")
        self.setStyleSheet("background-color: #222222;")
        self.showFullScreen()
        
        self.stacked_widget = QStackedWidget(self)
        self.setCentralWidget(self.stacked_widget)

        self.setup_ui()
        self.setup_signals()

    def setup_ui(self):
        self.menu_record_camera = App_record_camera()
        self.menu_record_camera_edit = App_record_camera_edit()
        self.menu_preview_record_camera = CameraLayout()

        self.stacked_widget.addWidget(self.menu_record_camera)
        self.stacked_widget.addWidget(self.menu_record_camera_edit)
        self.stacked_widget.addWidget(self.menu_preview_record_camera)

    def setup_signals(self):
        self.menu_record_camera.add_edit_record_clicked.connect(self.show_menu_edit_record_camera)
        self.menu_record_camera.preview_record_clicked.connect(self.show_preview_record_camera)
        self.menu_record_camera.minimize_clicked.connect(self.showMinimized)
        self.menu_record_camera.close_clicked.connect(self.close_window)
        
        self.menu_record_camera_edit.back_record_clicked.connect(self.show_record_camera)
        self.menu_record_camera_edit.save_record_clicked.connect(self.show_record_camera)
        
        self.menu_preview_record_camera.back_record_clicked.connect(self.show_record_camera)
        self.menu_preview_record_camera.save_record_clicked.connect(self.show_record_camera)
        self.menu_preview_record_camera.minimize_clicked.connect(self.showMinimized)
        self.menu_preview_record_camera.close_clicked.connect(self.close_window)

    def close_window(self):
        msg = QMessageBox()
        msg.setWindowTitle("Confirm Close Window")
        msg.setText("Are you sure you want to close the window?")
        msg.setIcon(QMessageBox.Warning)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        response = msg.exec_()
        if response == QMessageBox.Ok:
            self.close()

    def closeEvent(self, event):
        self.closed.emit()
        event.accept()

    def show_record_camera(self):
        self.menu_record_camera.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_record_camera)

    def show_preview_record_camera(self, id_data):
        self.menu_preview_record_camera.update_layout(id_data)
        self.stacked_widget.setCurrentWidget(self.menu_preview_record_camera)

    def show_menu_edit_record_camera(self, id_data):
        self.menu_record_camera_edit.set_parameter(id_data)
        self.stacked_widget.setCurrentWidget(self.menu_record_camera_edit)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = App_home('')
    window.show()
    sys.exit(app.exec_())
