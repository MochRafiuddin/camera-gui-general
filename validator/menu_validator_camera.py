import sys
import os
import subprocess
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QMessageBox
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal
import sqlite3
from datetime import datetime
from export_excel import ExcelReport

class App_record_camera(QMainWindow):
    add_edit_record_clicked = pyqtSignal(int)
    preview_record_clicked = pyqtSignal(int)
    export_data_clicked = pyqtSignal(int)
    minimize_clicked = pyqtSignal()
    close_clicked = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.update_content()
        
    def update_content(self):
        self.menu4()
        self.top_bar()
    
    def menu4(self):
        self.screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = self.screen_geometry.width() / 1280
        self.height_ratio = self.screen_geometry.height() / 832
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(20 * self.width_ratio),
            int(90 * self.height_ratio),
            int(1238 * self.width_ratio),
            int(699 * self.height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(30 * self.width_ratio),
            int(105 * self.height_ratio),
            int(270 * self.width_ratio),
            int(43 * self.height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" Data validator ")

        add_data_button = QPushButton("Add Data", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1150 * self.width_ratio),
            int(124 * self.height_ratio),
            int(87 * self.width_ratio),
            int(43 * self.height_ratio),
        )
        add_data_button.clicked.connect(self.emit_menu_add_record_clicked)    

        export_data_button = QPushButton("Export Data", self)
        export_data_button.setStyleSheet(
            """
            background-color: #1eff7f;
            color: black;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        export_data_button.setGeometry(
            int(1050 * self.width_ratio),
            int(124 * self.height_ratio),
            int(87 * self.width_ratio),
            int(43 * self.height_ratio),
        )
        export_data_button.clicked.connect(self.emit_export_data_clicked)    

        self.table_widget = QTableWidget(self)
        self.table_widget.setGeometry(
            int(30 * self.width_ratio),
            int(191 * self.height_ratio),
            int(1220 * self.width_ratio),
            int(510 * self.height_ratio),
        )
        self.table_widget.setColumnCount(17)
        self.table_widget.setStyleSheet(
            """
            QTableWidget {
                background-color: #3d3d3d;
                border: none;
                font-family: arial;
                font-size: 18px;
                
            }
            QTableWidget::item {
                padding: 10px;
                border-bottom: 1px solid white; /* Set border color to white */
                color: white;
                font-family: arial;
                font-size: 18px;
            }
            QHeaderView::section {
                background-color: black; /* Set header background to black */
                color: white; 
                border: none;
                font-weight: 400;
                
                padding: 10px;
                font-family: arial;
                font-size: 20px;
                
            }
            
            """
        )

        self.table_widget.setHorizontalHeaderLabels(
            ["No", "Site", "Device Name", "File Name", "Date", "Recording Time", "Duration (Jam)", "System IN", "Visual IN", "System Out", "Visual OUT", "Difference IN", "Difference OUT", "Accuracy IN", "Accuracy OUT", "Verify By", "Opsi"]
        )

        header = self.table_widget.horizontalHeader()
        header.setStyleSheet(
            """
            background-color: black; /* Set header background to black */
            color: white; /* Set header text color to white */

           
            border: none;
            """
        )

        self.table_widget.setShowGrid(False)
        self.table_widget.verticalHeader().setVisible(False)

        self.table_widget.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)

        self.table_widget.horizontalHeader().resizeSection(0, int(35 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(1, int(150 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(2, int(250 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(3, int(420 * self.width_ratio))  # Atur lebar kolom 3 secara manual
        self.table_widget.horizontalHeader().resizeSection(4, int(90 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(5, int(110 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(6, int(110 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(7, int(100 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(8, int(100 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(9, int(100 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(10, int(100 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(11, int(100 * self.width_ratio))  # Atur lebar kolom 3 secara manual
        self.table_widget.horizontalHeader().resizeSection(12, int(110 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(13, int(100 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(14, int(100 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(15, int(110 * self.width_ratio))
        self.table_widget.horizontalHeader().resizeSection(16, int(80 * self.width_ratio))
        # self.table_widget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
        # self.table_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.add_data_to_table()

    def top_bar(self):
        # top bar
        top_bar = QLabel(self)
        top_bar.setStyleSheet("background-color: #000000;")
        top_bar.setGeometry(0, 0, self.screen_geometry.width(), int(63 * self.height_ratio))

        self.icon1_label = QLabel(self)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.setGeometry(
            int(1206 * self.width_ratio),
            int(17 * self.height_ratio),
            int(24 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        ) 
        self.icon1_label.mousePressEvent = self.minimize_window

        self.icon2_label = QLabel(self)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon2_label.setGeometry(
            int(1232 * self.width_ratio),
            int(15 * self.height_ratio),
            int(33 * self.width_ratio),
            int(33 * self.height_ratio),
        )
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        ) 

        self.icon3_label = QLabel(self)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon3_label.setGeometry(
            int(1252 * self.width_ratio),
            int(9 * self.height_ratio),
            int(45 * self.width_ratio),
            int(45 * self.height_ratio),
        )
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  
        self.icon3_label.mousePressEvent = self.on_close_button_clicked

        control_panel_label = QLabel("Validator Camera", self)
        control_panel_label.setStyleSheet("color: white; background-color: #000000;")
        control_panel_label.setFont(
            QFont("Times", int(23 * min(self.width_ratio, self.height_ratio)))
        )
        control_panel_label.setGeometry(
            int(30 * self.width_ratio),
            int(10 * self.height_ratio),
            int(280 * self.width_ratio),
            int(45 * self.height_ratio),
        )
    
    def add_data_to_table(self):
        conn = sqlite3.connect('db_validator.db')
        cursor = conn.cursor()        
        cursor.execute("SELECT * FROM t_validator where deleted = 1 ORDER BY id desc")
        dataall = cursor.fetchall()
        conn.commit()                
        conn.close()

        for row, (id_valid, site, device_name, filename, date, record_time, duration, system_in, real_in, system_out, real_out, diff_in, diff_out, accurary_in, accurary_out, verify_by, deleted) in enumerate(dataall):
            self.table_widget.insertRow(row)

            no = QTableWidgetItem(f"{row+ 1}")
            self.table_widget.setItem(row, 0, no)

            site_title = QTableWidgetItem(site)
            self.table_widget.setItem(row, 1, site_title)

            device_name_title = QTableWidgetItem(device_name)
            self.table_widget.setItem(row, 2, device_name_title)

            filename_title = QTableWidgetItem(filename)
            self.table_widget.setItem(row, 3, filename_title)
            
            tanggal_obj = datetime.strptime(date, "%Y-%m-%d")
            tanggal_baru_str = tanggal_obj.strftime("%d-%m-%Y")
            date_title = QTableWidgetItem(tanggal_baru_str)
            self.table_widget.setItem(row, 4, date_title)            

            record_time_title = QTableWidgetItem(record_time)
            self.table_widget.setItem(row, 5, record_time_title) 

            duration_title = QTableWidgetItem(str(duration))
            self.table_widget.setItem(row, 6, duration_title)
            
            system_in_title = QTableWidgetItem(str(system_in))
            self.table_widget.setItem(row, 7, system_in_title)

            real_in_txt = real_in if real_in else 0
            real_in_title = QTableWidgetItem(str(real_in_txt))
            self.table_widget.setItem(row, 8, real_in_title)

            system_out_title = QTableWidgetItem(str(system_out))
            self.table_widget.setItem(row, 9, system_out_title)

            real_out_txt = real_out if real_out else 0
            real_out_title = QTableWidgetItem(str(real_out_txt))
            self.table_widget.setItem(row, 10, real_out_title)

            diff_in_txt = diff_in if diff_in else 0
            diff_in_title = QTableWidgetItem(str(diff_in_txt))
            self.table_widget.setItem(row, 11, diff_in_title)

            diff_out_txt = diff_out if diff_out else 0
            diff_out_title = QTableWidgetItem(str(diff_out_txt))
            self.table_widget.setItem(row, 12, diff_out_title)
            
            accurary_in_txt = accurary_in if accurary_in else 0
            accurary_in_title = QTableWidgetItem(str(accurary_in_txt))
            self.table_widget.setItem(row, 13, accurary_in_title)

            accurary_out_txt = accurary_out if accurary_out else 0
            accurary_out_title = QTableWidgetItem(str(accurary_out_txt))
            self.table_widget.setItem(row, 14, accurary_out_title)

            verify_by_title = QTableWidgetItem(verify_by)
            self.table_widget.setItem(row, 15, verify_by_title)

            icon_widget = QWidget()
            icon_layout = QHBoxLayout()
            icon_widget.setStyleSheet("QWidget { background-color: transparent;}")
            self.button_edit_snapshot = QLabel()
            self.button_edit_snapshot.setPixmap(QPixmap("img/preview2.png").scaled(23, 23))
            self.button_edit_snapshot.setAlignment(Qt.AlignRight)
            icon_layout.addWidget(self.button_edit_snapshot)
            self.button_edit_snapshot.setStyleSheet("QLabel { background-color: #3d3d3d;}")
            self.button_edit_snapshot.mousePressEvent = lambda event, id_data=id_valid: self.emit_menu_preview_record_clicked(id_data)

            self.button_edit_snapshot = QLabel()
            self.button_edit_snapshot.setPixmap(QPixmap("img/edit.png").scaled(20, 20))
            self.button_edit_snapshot.setAlignment(Qt.AlignRight)
            icon_layout.addWidget(self.button_edit_snapshot)
            self.button_edit_snapshot.setStyleSheet("QLabel { background-color: #3d3d3d;}")
            self.button_edit_snapshot.mousePressEvent = lambda event, id_data=id_valid: self.emit_menu_edit_record_clicked(id_data)

            clickable_icon_2 = QLabel()
            clickable_icon_2.setPixmap(QPixmap("img/del.png").scaled(20, 20))
            clickable_icon_2.setAlignment(Qt.AlignLeft)
            clickable_icon_2.setFixedHeight(20)
            icon_layout.addWidget(clickable_icon_2)
            clickable_icon_2.setStyleSheet("QLabel { background-color: #3d3d3d; }")
            clickable_icon_2.mousePressEvent = lambda event, id_data=id_valid: self.show_delete_confirmation_popup(id_data)

            icon_widget.setLayout(icon_layout)

            self.table_widget.setCellWidget(row, 16, icon_widget)

            self.table_widget.setRowHeight(row, 60)
    
    def show_delete_confirmation_popup(self, id_data):
        # Membuat instance QMessageBox
        msg = QMessageBox()
        msg.setWindowTitle("Confirm Data Deletion")
        msg.setText("Are you sure you want to delete the data?")
        msg.setIcon(QMessageBox.Warning)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setDefaultButton(QMessageBox.Cancel)

        # Menampilkan popup dan mendapatkan respons pengguna
        response = msg.exec_()

        # Jika pengguna menekan tombol Ok, maka lakukan aksi penghapusan data
        if response == QMessageBox.Ok:
            print("Data berhasil dihapus!") 
            conn = sqlite3.connect('db_validator.db')
            cursor = conn.cursor()
            update_query = f"UPDATE t_validator SET deleted = ? where id = ?"
            cursor.execute(update_query, ('0', id_data))
            conn.commit()        
            cursor.close()
            conn.close()
            self.clear_table()
            self.add_data_to_table()

    def clear_table(self):
        self.table_widget.clearContents()  # Menghapus konten tabel
        self.table_widget.setRowCount(0)  # Mengatur jumlah baris tabel menjadi 0

    def emit_menu_preview_record_clicked(self, id_data):
        # print("Menu edit cam list clicked")
        self.preview_record_clicked.emit(id_data)

    def emit_menu_edit_record_clicked(self, id_data):
        # print("Menu edit cam list clicked")
        self.add_edit_record_clicked.emit(id_data)

    def emit_menu_add_record_clicked(self, event):
        # print("Menu add cam list clicked")
        self.add_edit_record_clicked.emit(0)

    def emit_export_data_clicked(self, event):
        # Usage
        report = ExcelReport()
        report.set_column_widths()
        report.add_customer_data()
        report.add_system_data()
        report.add_table_header()

        conn = sqlite3.connect('db_validator.db')
        cursor = conn.cursor()        
        cursor.execute("SELECT * FROM t_validator WHERE deleted = 1 ORDER BY id DESC")
        dataall = cursor.fetchall()
        conn.commit()
        conn.close()

        data = []
        # Sample data
        for row, (id_valid, site, device_name, filename, date, record_time, duration, system_in, real_in, system_out, real_out, diff_in, diff_out, accurary_in, accurary_out, verify_by, deleted) in enumerate(dataall):
            tanggal_obj = datetime.strptime(date, "%Y-%m-%d")
            tanggal_baru_str = tanggal_obj.strftime("%d-%m-%Y")
            data.append([row + 1, site, device_name, filename, tanggal_baru_str, record_time, duration, system_in, real_in, system_out, real_out, diff_in, diff_out, accurary_in, accurary_out, verify_by])

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
        output_filename = f'export_excel/export_{timestamp}.xlsx'
        report.add_data(data)
        report.save(output_filename)

        msg = QMessageBox()
        msg.setWindowTitle("Export Successful")
        msg.setText(f"Data has been successfully exported to {output_filename}.")
        msg.setIcon(QMessageBox.Information)
        msg.setStyleSheet("""
            QMessageBox {
                background-color: #222222;
                color: white;
            }
            QMessageBox QLabel {
                color: white;
                font-family: 'Inter';
                font-size: 12px;
            }
            QMessageBox QPushButton {
                background-color: #444;
                color: white;
                border: 1px solid #555;
            }
            QMessageBox QPushButton:pressed {
                background-color: #333;
            }
        """)
        msg.exec_()
        return
    
    def minimize_window(self, event):
        self.minimize_clicked.emit() 

    def on_close_button_clicked(self, event):
        self.close_clicked.emit() 
# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_record_camera()
#     window.show()
#     sys.exit(app.exec_())
