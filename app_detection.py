from multiprocessing import Queue
from multiprocessing.managers import BaseManager
import cv2
import threading
import sqlite3
from keluar_masuk import VideoCamera
from area import CountObject
from gender1 import face_gender
import datetime
import os
import sys
import time
# Connect to SQLite database and fetch settings
conn = sqlite3.connect('setting.db')
cursor = conn.cursor()
query = """
    SELECT id, path, type_camera FROM setting_camera WHERE deleted = 1
    UNION ALL    
    SELECT id, path, type_camera FROM setting_camera_area WHERE deleted = 1
    UNION ALL    
    SELECT id, path, type_camera FROM setting_camera_gender WHERE deleted = 1;
"""
cursor.execute(query)
results = cursor.fetchall()
conn.close()

frame_queues = [Queue(maxsize=10) for _ in range(len(results))]
processed_frame_queues = [(row[0], row[2], Queue(maxsize=10)) for i, row in enumerate(results)]  # id, type_camera, Queue
processed_video_frame =[]
#total_in = 0
#total_out =0
def initialize_log_txt():
    log_file = 'detection.txt'
    now = datetime.datetime.today()
    formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")
    if os.path.exists(log_file):
        with open(log_file, 'a') as f:
            f.write(f'# Start Detection - {formatted_now}\n')
    else:
        with open(log_file, 'w') as f:
            f.write(f'# Start Detection - {formatted_now}\n')

def update_log_txt(new_segment):
    log_file = 'detection.txt'
    now = datetime.datetime.today()
    formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")

    with open(log_file, 'a') as f:        
        f.write(f'{formatted_now} -- {new_segment}\n')

def get_setting_camera_by_id(table, id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM {table} where deleted=1 and id={id_cam}').fetchone()
    return data

def check_and_add_camera(processed_video_frame, data):
    exists = any(frame for frame in processed_video_frame if frame['id'] == data[0])
    if not exists:
        processed_video_frame.append({'id': data[0], 'type_camera': data[1], 'id_camera': data[2], "queue_frame": Queue(maxsize=10)})
        print("add Array", processed_video_frame)

def rtsp_stream_reader_single(rtsp_url, frame_queue, id_cam):
    cap = cv2.VideoCapture(rtsp_url)
    while True:
        ret, frame = cap.read()
        if not ret:
            update_status_camera(id_cam, 2, 0)
            continue
        frame_queue.put(frame)  # Just putting the frame
    # cap.release()

def frame_processor_inout(frame_queue, processed_frame_queue, id_camera, type_camera):
    camera_keluar_masuk = VideoCamera()
    while True:
        frame_data = next((data for data in processed_video_frame 
                        if data['type_camera'] == type_camera and data['id_camera'] == id_camera), None)
        if frame_data is None:
            id_data = 0
        else:
            id_data = frame_data['id']

        frame = frame_queue.get()  # Retrieving the frame
        frame_ai, frame_vidio = camera_keluar_masuk.get_frame(frame, id_camera, id_data)

        # Add AI processing code here
        processed_frame = frame_ai  
        if not processed_frame_queue.full():
            processed_frame_queue.put(processed_frame)  # Just putting the processed frame
        
        if frame_data is None:
            continue
        else:
            if not frame_data["queue_frame"].full():
                frame_data["queue_frame"].put(frame_vidio)

def frame_processor_area(frame_queue, processed_frame_queue, id_camera):
    camera_area = CountObject(id_camera)
    while True:
        frame = frame_queue.get()  # Retrieving the frame
        frame_ai = camera_area.process_video(frame)
        # Add AI processing code here
        processed_frame = frame_ai  
        if not processed_frame_queue.full():
            processed_frame_queue.put(processed_frame)  # Just putting the processed frame

def frame_processor_gender(frame_queue, processed_frame_queue, id_camera):
    camera_gender = face_gender()
    while True:
        frame = frame_queue.get()  # Retrieving the frame
        frame_ai = camera_gender.detect_faces(frame, id_camera)
        # Add AI processing code here
        processed_frame = frame_ai  
        if not processed_frame_queue.full():
            processed_frame_queue.put(processed_frame)  # Just putting the processed frame

def reset_array_camera_in_out(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    update_query = "UPDATE setting_camera SET people_kanan = ?, people_kiri = ?, people_masuk = ? where id = ?"
    cursor.execute(update_query, ('[]', '[]', "{}", id_cam))
    conn.commit()
    cursor.close()
    conn.close()
    update_log_txt(f"array camera in_out id: {id_cam}, reset successfully")

def reset_array_camera_gender(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()    
   
    update_query = "UPDATE setting_camera_gender SET array_kanan = ?, array_kiri = ? where id = ?"
    cursor.execute(update_query,('[]', '[]', id_cam))
    conn.commit()
    
    cursor.close()
    conn.close()
    update_log_txt(f"array camera gender id: {id_cam},reset successfully")

def update_status_camera(id, type_camera, status_camera):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    if type_camera == 1:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    elif type_camera == 2:        
        update_query = "UPDATE setting_camera_gender SET status_camera = ? where id = ?"
    elif type_camera == 3:
        update_query = "UPDATE setting_camera_area SET status_camera = ? where id = ?"
    elif type_camera == 4:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    cursor.execute(update_query,(status_camera,id))
    conn.commit()

    cursor.close()
    conn.close()


def restart_file():
    python = sys.executable
    os.execl(python, python, *sys.argv)

class QueueManager(BaseManager):
    pass

def get_processed_frame_queue(stream_id, type_cam):
    idx = 0
    for index, (id_cam, t_cam, queue) in enumerate(processed_frame_queues):
        if id_cam == stream_id and t_cam == type_cam:
            return queue
    return None

def get_processed_video_frame_queue(id_data):
    # today = datetime.datetime.now().strftime('%Y-%m-%d')
    # current_time = datetime.datetime.now()
    # # Subtract 2 seconds
    # adjusted_time = current_time - datetime.timedelta(seconds=2)
    # # Format the adjusted time to 'HH:MM:SS'
    # formatted_time = adjusted_time.strftime('%H:%M:%S')
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    query = f"""
    SELECT * FROM t_record
    WHERE id = ?
    """
    data = cursor.execute(query, (id_data, )).fetchone()
    conn.close()
    
    check_and_add_camera(processed_video_frame, data)
    
    for frame_data in processed_video_frame:
        if frame_data['id'] == id_data:
            return frame_data["queue_frame"]
    return None

def remove_frame_by_id(id_data):
    global processed_video_frame
    processed_video_frame = [frame for frame in processed_video_frame if frame['id'] != id_data]
    print("remove array success", processed_video_frame)

# Register the functions
QueueManager.register('get_processed_frame_queue', callable=lambda id_cam, type_cam: get_processed_frame_queue(id_cam, type_cam))
QueueManager.register('get_processed_video_frame_queue', callable=lambda id_data :get_processed_video_frame_queue(id_data))
QueueManager.register('remove_frame_by_id', callable=lambda id_data :remove_frame_by_id(id_data))


if __name__ == "__main__":
    initialize_log_txt()
    threads = []
    for i, (id, path, type_camera) in enumerate(results):
        print(id, type_camera, path)
        stream_thread = threading.Thread(target=rtsp_stream_reader_single, args=(path, frame_queues[i], id))
        stream_thread.start()
        threads.append(stream_thread)

        if type_camera == 1 or type_camera == 4:
            reset_array_camera_in_out(id)
            processor_thread = threading.Thread(target=frame_processor_inout, args=(frame_queues[i], processed_frame_queues[i][2], id, type_camera))  # Correct indexing
            processor_thread.start()
            threads.append(processor_thread)
        elif type_camera == 2:
            processor_thread = threading.Thread(target=frame_processor_gender, args=(frame_queues[i], processed_frame_queues[i][2], id))  # Correct indexing
            processor_thread.start()
            threads.append(processor_thread)
        elif type_camera == 3:
            processor_thread = threading.Thread(target=frame_processor_area, args=(frame_queues[i], processed_frame_queues[i][2], id))  # Correct indexing
            processor_thread.start()
            threads.append(processor_thread)

    manager = QueueManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    server = manager.get_server()
    print("Server started at address ('127.0.0.1', 50000)")

    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()
    threads.append(server_thread)
    
    for thread in threads:
        thread.join()
