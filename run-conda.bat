@echo off

rem Change directory to the specified location
cd /d C:\Users\USER\Documents\aplikasi\camera-gui-general\

rem Run the first Python script
start "rtsp_process1" cmd /k "conda run -n base python rtsp_process2.py"

rem Mengakhiri script batch utama agar tidak tetap terbuka
exit
