@echo off

rem Change directory to the specified location
cd /d D:\PYTHON\camera-gui-general

rem Run the first Python script with a window title
start "Applikasi_Deteksi" cmd /k "title Applikasi Deteksi && conda run -n base python app_detection.py"

rem Run the second Python script with a window title
start "Applikasi_Sync" cmd /k "title Applikasi Sync && conda run -n base python app_sync.py"

rem End the main batch script to prevent it from staying open
exit
