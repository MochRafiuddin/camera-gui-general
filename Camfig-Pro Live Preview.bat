@echo off

rem Jalankan conda init
rem call conda init

rem Aktifkan lingkungan conda (misalnya 'base')
call conda activate base

rem Jalankan skrip Python
python D:\PYTHON\camera-gui-general\loginfix.py

rem Jeda agar output bisa dilihat sebelum menutup (opsional)
pause
