import sys
import os
import subprocess
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QLineEdit,
    QMessageBox
)
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtCore import pyqtSignal, QDate, QTime
from datetime import datetime

import sqlite3
from sidebar import App_sidebar

class App_record_camera_edit(QMainWindow):
    back_record_clicked = pyqtSignal()
    save_record_clicked = pyqtSignal()

    def __init__(self, id_data):
        super().__init__()
        # self.set_parameter(id_data)

    def set_parameter(self, id_data, id_cam, type_cam):
        self.camera_type = 0
        self.camera_id = 0
        # self.datas = data
        self.label_menu = " Add Record "
        self.id_data = id_data
        if id_data != 0:
            self.label_menu = " Edit Record "
            conn = sqlite3.connect('setting.db')
            cursor = conn.cursor()        
            cursor.execute(f"SELECT * FROM t_record WHERE id = {id_data}")
            data = cursor.fetchone()
            conn.commit()                
            conn.close()
            
            self.camera_type = data[1]
            self.camera_id = data[2]
            self.tanggal_record = data[3]
            self.start_time = data[4]
            self.end_time = data[5]
        elif id_cam !=0 and type_cam !=0:
            # print(id_data, id_cam, type_cam)
            self.camera_type = type_cam
            self.camera_id = id_cam

        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()
        self.update_form()  # Panggil update_form untuk pengaturan awal


    def update_form(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        # Main Content Area
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(250 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(self.label_menu)

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Camera Type")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 240, 170, 36)
        
        self.cam_type = QtWidgets.QComboBox(self)
        self.cam_type.addItem("Visitor")
        self.cam_type.addItem("Gender")
        self.cam_type.addItem("Zone")
        self.cam_type.addItem("Outer Traffic")
        self.cam_type.setFont(QtGui.QFont("Inter", 16))
        self.cam_type.setGeometry(600, 240, 400, 36)
        self.cam_type.setStyleSheet(
            "QComboBox {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
            "QComboBox::drop-down:button{background-color: #5C6164;width: 40px;}"
            "QComboBox::down-arrow {image: url(img/arrrow.png);width: 40px;height: 36px ;padding: 5px;}"
            "QComboBox::item { color: white; }" # Mengatur warna teks item menjadi putih
        )
        self.cam_type.currentIndexChanged.connect(self.update_camera_combobox)

        self.label4 = QLabel(self)
        self.label4.setFont(QFont("Inter", 15))
        self.label4.setText("Camera")
        self.label4.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label4.setGeometry(450, 320, 170, 36)
        
        self.camera_ids = QtWidgets.QComboBox(self)
        self.camera_ids.setFont(QFont("Inter", 16))
        self.camera_ids.setGeometry(600, 320, 400, 36)
        self.camera_ids.setStyleSheet(
            "QComboBox {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
            "QComboBox::drop-down:button{background-color: #5C6164;width: 40px;}"
            "QComboBox::down-arrow {image: url(img/arrrow.png);width: 40px;height: 36px ;padding: 5px;}"
            "QComboBox::item { color: white; }"
        )
        self.update_camera_combobox(self.cam_type.currentIndex())  # Initialize secondary combo box with the first item

        self.label_date = QLabel(self)
        self.label_date.setFont(QFont("Inter", 15))
        self.label_date.setText("Date")
        self.label_date.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label_date.setGeometry(450, 400, 170, 36)

        self.date_edit = QtWidgets.QDateEdit(self)
        self.date_edit.setFont(QFont("Inter", 16))
        self.date_edit.setGeometry(600, 400, 400, 36)
        self.date_edit.setStyleSheet(
            "QDateEdit {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
        )
        self.date_edit.setCalendarPopup(True)
        self.date_edit.setDate(QtCore.QDate.currentDate())

        self.label_start_time = QLabel(self)
        self.label_start_time.setFont(QFont("Inter", 15))
        self.label_start_time.setText("Start Time")
        self.label_start_time.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label_start_time.setGeometry(450, 480, 170, 36)

        self.start_time_edit = QtWidgets.QTimeEdit(self)
        self.start_time_edit.setFont(QFont("Inter", 16))
        self.start_time_edit.setGeometry(600, 480, 400, 36)
        self.start_time_edit.setStyleSheet(
            "QTimeEdit {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
        )
        self.start_time_edit.setDisplayFormat("HH:mm:ss")
        self.start_time_edit.setTime(QtCore.QTime.currentTime())

        self.label_end_time = QLabel(self)
        self.label_end_time.setFont(QFont("Inter", 15))
        self.label_end_time.setText("End Time")
        self.label_end_time.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label_end_time.setGeometry(450, 560, 170, 36)

        self.end_time_edit = QtWidgets.QTimeEdit(self)
        self.end_time_edit.setFont(QFont("Inter", 16))
        self.end_time_edit.setGeometry(600, 560, 400, 36)
        self.end_time_edit.setStyleSheet(
            "QTimeEdit {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
        )
        self.end_time_edit.setDisplayFormat("HH:mm:ss")
        self.end_time_edit.setTime(QtCore.QTime.currentTime())

        add_data_button = QPushButton("Back", self)
        add_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1175 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        add_data_button.clicked.connect(self.back_button_clicked)

        add_data_button = QPushButton("Save", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1100 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        add_data_button.clicked.connect(self.save_button_clicked)

        if self.camera_type != 0:
            self.cam_type.setCurrentIndex(int(self.camera_type) - 1)
        if self.id_data != 0:
            self.set_date_time(self.tanggal_record, self.start_time, self.end_time)

    def update_camera_combobox(self, index):
        self.camera_ids.clear()
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        # print("index ==", index)
        query = ""

        if index == 0:  # Visitor
            query = "SELECT id, nama FROM setting_camera WHERE type_camera = ? and deleted = 1"
        elif index == 1:  # Gender
            query = "SELECT id, nama FROM setting_camera_gender WHERE type_camera = ? and deleted = 1"
        elif index == 2:  # Zone
            query = "SELECT id, nama FROM setting_camera_area WHERE type_camera = ? and deleted = 1"
        elif index == 3:  # Outer Traffic
            query = "SELECT id, nama FROM setting_camera WHERE type_camera = ? and deleted = 1"

        cursor.execute(query, (index + 1,))
        data = cursor.fetchall()

        for item in data:
            self.camera_ids.addItem(item[1], item[0])  # Add camera_name as display text and id_camera as hidden data

        if self.camera_id != 0:
            self.set_combobox_current_by_id(self.camera_ids, self.camera_id)

        cursor.close()
        conn.close()

    def set_combobox_current_by_id(self, combobox, id_data):
        index = combobox.findData(id_data)
        if index != -1:
            combobox.setCurrentIndex(index)

    def set_date_time(self, date_str, start_time_str, end_time_str):
        date = QtCore.QDate.fromString(date_str, "yyyy-MM-dd")
        start_time = QtCore.QTime.fromString(start_time_str, "HH:mm:ss")
        end_time = QtCore.QTime.fromString(end_time_str, "HH:mm:ss")

        self.date_edit.setDate(date)
        self.start_time_edit.setTime(start_time)
        self.end_time_edit.setTime(end_time)

    def back_button_clicked(self):
        self.back_record_clicked.emit()        

    def save_button_clicked(self):
        cam_type_index = self.cam_type.currentIndex()
        camera_id = self.camera_ids.currentData()  # Get the hidden data (id_camera)
        date_str = self.date_edit.date().toString("yyyy-MM-dd")
        start_time_str = self.start_time_edit.time().toString("HH:mm:ss")
        end_time_str = self.end_time_edit.time().toString("HH:mm:ss")

        # Convert strings to datetime objects
        input_date = datetime.strptime(date_str, "%Y-%m-%d").date()
        input_start_time = datetime.strptime(start_time_str, "%H:%M:%S").time()
        input_end_time = datetime.strptime(end_time_str, "%H:%M:%S").time()

        # Get current date and time
        now = datetime.now()
        current_date = now.date()
        current_time = now.time()

        # Check if date and times are in the future
        if input_date < current_date or (input_date == current_date and input_start_time <= current_time):
            msg = QMessageBox()
            msg.setWindowTitle("Invalid Input")
            msg.setText("Date and start time must be in the future.")
            msg.setIcon(QMessageBox.Warning)
            msg.setStyleSheet("""
                QMessageBox {
                    background-color: #222222;
                    color: white;
                }
                QMessageBox QLabel {
                    color: white;
                    font-family: 'Inter';
                    font-size: 12px;
                }
                QMessageBox QPushButton {
                    background-color: #444;
                    color: white;
                    border: 1px solid #555;
                }
                QMessageBox QPushButton:pressed {
                    background-color: #333;
                }
            """)
            msg.exec_()
            return
        
        if input_date == current_date and input_start_time >= input_end_time:
            msg = QMessageBox()
            msg.setWindowTitle("Invalid Input")
            msg.setText("End time must be after start time.")
            msg.setIcon(QMessageBox.Warning)
            msg.setStyleSheet("""
                QMessageBox {
                    background-color: #222222;
                    color: white;
                }
                QMessageBox QLabel {
                    color: white;
                    font-family: 'Inter';
                    font-size: 12px;
                }
                QMessageBox QPushButton {
                    background-color: #444;
                    color: white;
                    border: 1px solid #555;
                }
                QMessageBox QPushButton:pressed {
                    background-color: #333;
                }
            """)
            msg.exec_()
            return
        
        # Proceed with database operations
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()

        if self.id_data == 0:
            cursor.execute("""
                INSERT INTO t_record (type_camera, camera_id, tanggal_record, start_time, end_time)
                VALUES (?, ?, ?, ?, ?)
            """, (cam_type_index + 1, camera_id, date_str, start_time_str, end_time_str))
        else:
            cursor.execute("""
                UPDATE t_record
                SET type_camera = ?, camera_id = ?, tanggal_record = ?, start_time = ?, end_time = ?
                WHERE id = ?
            """, (cam_type_index + 1, camera_id, date_str, start_time_str, end_time_str, self.id_data))
        
        conn.commit()
        cursor.close()
        conn.close()

        self.save_record_clicked.emit()

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_snapshot_edit("8")
#     window.show()
#     sys.exit(app.exec_())
