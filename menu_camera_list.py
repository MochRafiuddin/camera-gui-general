import sys
import os
import subprocess
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QMessageBox
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal
import sqlite3
from sidebar import App_sidebar

class App_home2(QMainWindow):
    add_edit_camera_list_clicked = pyqtSignal(int, int)
    draw_clicked = pyqtSignal(int, int, str)

    def __init__(self):
        super().__init__()
        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()  # Panggil update_layout untuk pengaturan awal
        self.update_content()
        
    def update_content(self):
        self.menu2()

    def menu2(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        # Main Content Area
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )

        add_data_button = QPushButton("Add Data", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1136 * width_ratio),
            int(124 * height_ratio),
            int(87 * width_ratio),
            int(43 * height_ratio),
        )
        add_data_button.clicked.connect(self.emit_menu_add_camera_list_clicked)    

        reset_data_button = QPushButton("Reset Application", self)
        reset_data_button.setStyleSheet(
            """
            background-color: #1eff6d;
            color: black;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        reset_data_button.setGeometry(
            int(1016 * width_ratio),
            int(124 * height_ratio),
            int(107 * width_ratio),
            int(43 * height_ratio),
        )
        reset_data_button.clicked.connect(self.emit_reset_app_clicked)

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(250 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" Camera List ")

        # Table widget
        self.table_widget = QTableWidget(self)
        self.table_widget.setGeometry(
            int(312 * width_ratio),
            int(191 * height_ratio),
            int(921 * width_ratio),
            int(510 * height_ratio),
        )
        self.table_widget.setColumnCount(7)
        self.table_widget.setStyleSheet(
            """
            QTableWidget {
                background-color: #3d3d3d;
                border: none;
                font-family: arial;
                font-size: 18px;
                
            }
            QTableWidget::item {
                padding: 10px;
                border-bottom: 1px solid white; /* Set border color to white */
                color: white;
                font-family: arial;
                font-size: 18px;
            }
            QHeaderView::section {
                background-color: black; /* Set header background to black */
                color: white; 
                border: none;
                font-weight: 400;
                
                padding: 10px;
                font-family: arial;
                font-size: 20px;
                
            }
            
            """
        )

        self.table_widget.setHorizontalHeaderLabels(
            ["No", "Name", "Code", "RTSP", "Location Code", "Type", "Option"]
        )

        header = self.table_widget.horizontalHeader()
        header.setStyleSheet(
            """
            background-color: black; /* Set header background to black */
            color: white; /* Set header text color to white */

           
            border: none;
            """
        )

        self.table_widget.setShowGrid(False)
        self.table_widget.verticalHeader().setVisible(False)

        self.table_widget.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        # Set alignment for specific header sections (kolom 3 dan 4)
        self.table_widget.horizontalHeaderItem(1).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(2).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(3).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(4).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(5).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(6).setTextAlignment(Qt.AlignCenter)

        self.table_widget.horizontalHeader().resizeSection(0, int(35 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(1, int(150 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(2, int(80 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(3, int(450 * width_ratio))  # Atur lebar kolom 3 secara manual
        self.table_widget.horizontalHeader().resizeSection(4, int(100 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(5, int(110 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(6, int(110 * width_ratio))

        # self.table_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.add_data_to_table()

    def add_data_to_table(self):
        # data_camera = []
        camera_area = self.get_data_camera('setting_camera_area')
        camera_gender = self.get_data_camera('setting_camera_gender')
        camera_in_out = self.get_data_camera('setting_camera')
        site_code = self.get_cosntant_by_code('site_code')

        book1 = []
        book2 = self.get_camera_info(book1, camera_in_out, 0, 1, 9, 2, site_code[2], 18)
        book3 = self.get_camera_info(book2, camera_gender, 0, 1, 3, 2, site_code[2], 17)
        books = self.get_camera_info(book3, camera_area, 0, 1, 3, 2, site_code[2], 10)

        # print(books)
        for row, (name, code, rtsp, location_code, type_cam, id_cam) in enumerate(books):
            self.table_widget.insertRow(row)

            no = QTableWidgetItem(f"{row+ 1}")
            self.table_widget.setItem(row, 0, no)

            name_title = QTableWidgetItem(name)
            name_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 1, name_title)

            code_title = QTableWidgetItem(code)
            code_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 2, code_title)

            rtsp_title = QTableWidgetItem(rtsp)
            rtsp_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 3, rtsp_title)
            
            location_code_title = QTableWidgetItem(location_code)
            location_code_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 4, location_code_title) 
            
            if type_cam == 1:
                type_camera = "Visitor"
            elif type_cam == 2:
                type_camera = "Gender"
            elif type_cam == 3:
                type_camera = "Zone"
            elif type_cam == 4:
                type_camera = "Outer Traffic"
            type_cam_title = QTableWidgetItem(type_camera)
            type_cam_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 5, type_cam_title) 

            icon_widget = QWidget()
            # icon_widget.setTextAlignment(Qt.AlignCenter)
            icon_widget.setStyleSheet("QWidget { background-color: transparent;}")

            icon_layout = QHBoxLayout()

            if type_cam != 3:
                clickable_icon_border = QLabel()
                clickable_icon_border.setPixmap(QPixmap("img/border.png").scaled(20, 20))
                # clickable_icon_border.setAlignment(Qt.AlignRight)
                icon_layout.addWidget(clickable_icon_border)
                clickable_icon_border.setStyleSheet("QLabel { background-color: #3d3d3d;}")
                clickable_icon_border.mousePressEvent = lambda event, id_data=id_cam, type_cam=type_cam, type_draw="Border": self.draw_border(id_data, type_cam, type_draw)

                clickable_icon_line = QLabel()
                clickable_icon_line.setPixmap(QPixmap("img/pen.png").scaled(20, 20))
                # clickable_icon_line.setAlignment(Qt.AlignRight)
                icon_layout.addWidget(clickable_icon_line)
                clickable_icon_line.setStyleSheet("QLabel { background-color: #3d3d3d;}")
                clickable_icon_line.mousePressEvent = lambda event, id_data=id_cam, type_cam=type_cam, type_draw="Line": self.draw_border(id_data, type_cam, type_draw)
            else:
                clickable_icon_kosong = QLabel()
                empty_pixmap = QPixmap(20, 20)
                empty_pixmap.fill(Qt.transparent)
                clickable_icon_kosong.setPixmap(empty_pixmap)                
                icon_layout.addWidget(clickable_icon_kosong)

                clickable_icon_area = QLabel()
                clickable_icon_area.setPixmap(QPixmap("img/area.png").scaled(20, 20))
                # clickable_icon_area.setAlignment(Qt.AlignRight)
                icon_layout.addWidget(clickable_icon_area)
                clickable_icon_area.setStyleSheet("QLabel { background-color: #3d3d3d;}")
                clickable_icon_area.mousePressEvent = lambda event, id_data=id_cam, type_cam=type_cam, type_draw="Area": self.draw_border(id_data, type_cam, type_draw)

            clickable_icon_1 = QLabel()
            clickable_icon_1.setPixmap(QPixmap("img/edit.png").scaled(20, 20))
            # clickable_icon_1.setAlignment(Qt.AlignRight)
            icon_layout.addWidget(clickable_icon_1)
            clickable_icon_1.setStyleSheet("QLabel { background-color: #3d3d3d;}")
            clickable_icon_1.mousePressEvent = lambda event, id_data=id_cam, type_cam=type_cam: self.emit_menu_edit_camera_list_clicked(id_data, type_cam)

            clickable_icon_2 = QLabel()
            clickable_icon_2.setPixmap(QPixmap("img/del.png").scaled(20, 20))
            # clickable_icon_2.setAlignment(Qt.AlignLeft)
            clickable_icon_2.setFixedHeight(20)
            icon_layout.addWidget(clickable_icon_2)
            clickable_icon_2.setStyleSheet("QLabel { background-color: #3d3d3d; }")
            clickable_icon_2.mousePressEvent = lambda event, id_data=id_cam, type_cam=type_cam: self.show_delete_confirmation_popup(id_data, type_cam)

            icon_widget.setLayout(icon_layout)

            self.table_widget.setCellWidget(row, 6, icon_widget)

            self.table_widget.setRowHeight(row, 60)
    def clear_table(self):
        self.table_widget.clearContents()  # Menghapus konten tabel
        self.table_widget.setRowCount(0)  # Mengatur jumlah baris tabel menjadi 0

    def get_data_camera(self, table):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM {table} where deleted=1').fetchall()
        return data

    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data

    def get_camera_info(self, data_camera, camera_source, index_id, index_nama, index_code, index_rtsp, site_code, index_type_cam):
        nomor = 1
        # books = [('camera in out 1','A01','rtsp://camera:12345678@192.168.100.140:554/stream1','DKY005',"People-Counting",'1')]
        for cam in camera_source:
            nama_cam = cam[index_nama]
            code_cam = cam[index_code]
            rtsp_cam = cam[index_rtsp]
            id_cam = cam[index_id]
            type_cam = cam[index_type_cam]
            camera_info = (
                nama_cam,
                code_cam,
                rtsp_cam,
                site_code,
                type_cam,
                id_cam,
            )
            nomor +=1
            data_camera.append(camera_info)
        return data_camera
    
    def show_delete_confirmation_popup(self, id_cam, type_cam):
        # Membuat instance QMessageBox
        msg = QMessageBox()
        msg.setWindowTitle("Confirm Data Deletion")
        msg.setText("Are you sure you want to delete the data?")
        msg.setIcon(QMessageBox.Warning)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setDefaultButton(QMessageBox.Cancel)

        # Menampilkan popup dan mendapatkan respons pengguna
        response = msg.exec_()

        # Jika pengguna menekan tombol Ok, maka lakukan aksi penghapusan data
        if response == QMessageBox.Ok:
            print("Data berhasil dihapus! ", type_cam)
            conn = sqlite3.connect('setting.db')
            cursor = conn.cursor()

            if type_cam == 1 or type_cam == 4:
                table_data = "setting_camera"
            elif type_cam == 2:
                table_data = 'setting_camera_gender'
            elif type_cam == 3: 
                table_data = 'setting_camera_area'

            update_query = f"UPDATE {table_data} SET deleted = ? where id = ?"
            cursor.execute(update_query, ('0', id_cam))
            conn.commit()        
            cursor.close()
            conn.close()
            self.clear_table()
            self.add_data_to_table()

    def emit_menu_add_camera_list_clicked(self, event):
        # print("Menu add cam list clicked")
        self.add_edit_camera_list_clicked.emit(0, '')
    
    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data 

    def emit_reset_app_clicked(self, event):
        kill_run_app = self.get_cosntant_by_code("path_kill_and_run_app")
        result = subprocess.run(["powershell", "-ExecutionPolicy", "Bypass", "-File", kill_run_app[2]], check=True, capture_output=False, text=False)


    def emit_menu_edit_camera_list_clicked(self, id_data, type_cam):
        # print("Menu edit cam list clicked")
        self.add_edit_camera_list_clicked.emit(id_data, type_cam)

    def draw_border(self, id_data, type_cam, type_draw):
        # print("Menu edit cam list clicked")
        self.draw_clicked.emit(id_data, type_cam, type_draw)

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_home2()
#     window.show()
#     sys.exit(app.exec_())
