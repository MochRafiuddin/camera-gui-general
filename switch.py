from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QRect, pyqtSignal
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget


class MySwitch(QtWidgets.QPushButton):
    stateChanged = pyqtSignal(bool)  # Sinyal kustom untuk menggantikan stateChanged
    def __init__(self, parent = None):
        super().__init__(parent)
        # print('init')
        self.setCheckable(True)
        self.setMinimumWidth(66)
        self.setMinimumHeight(22)

    def paintEvent(self, event):
        label = "ON" if self.isChecked() else "OFF"
        bg_color = Qt.green if self.isChecked() else Qt.red

        radius = 10
        width = 32
        center = self.rect().center()

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.translate(center)
        painter.setBrush(QtGui.QColor(0,0,0))

        pen = QtGui.QPen(Qt.black)
        pen.setWidth(2)
        painter.setPen(pen)

        painter.drawRoundedRect(QRect(-width, -radius, 2*width, 2*radius), radius, radius)
        painter.setBrush(QtGui.QBrush(bg_color))
        sw_rect = QRect(-radius, -radius, width + radius, 2*radius)
        if not self.isChecked():
            sw_rect.moveLeft(-width)
            # sw_rect.moveTop(3)  # Untuk bawah

        painter.drawRoundedRect(sw_rect, radius, radius)
        painter.drawText(sw_rect, Qt.AlignCenter, label)

    def mousePressEvent(self, event):
        # Kode pemrosesan klik mouse
        self.setChecked(not self.isChecked())  # Toggle status tombol switch
        
        self.stateChanged.emit(self.isChecked())  # Emit sinyal stateChanged dengan status yang baru