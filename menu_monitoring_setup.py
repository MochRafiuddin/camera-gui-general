import sys
import os
import subprocess
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QMessageBox
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal
import sqlite3
from sidebar import App_sidebar
from switch import MySwitch

class App_monitoring_setup(QMainWindow):
    edit_monitoring_clicked = pyqtSignal(int, int)

    def __init__(self):
        super().__init__()
        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()  # Panggil update_layout untuk pengaturan awal
        self.update_content()
        
    def update_content(self):
        self.menu2()

    def menu2(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        # Main Content Area
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(300 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" Monitoring View Setup ")

        # Table widget
        self.table_widget = QTableWidget(self)
        self.table_widget.setGeometry(
            int(312 * width_ratio),
            int(191 * height_ratio),
            int(921 * width_ratio),
            int(510 * height_ratio),
        )
        self.table_widget.setColumnCount(8)
        self.table_widget.setStyleSheet(
            """
            QTableWidget {
                background-color: #3d3d3d;
                border: none;
                font-family: arial;
                font-size: 18px;
                
            }
            QTableWidget::item {
                padding: 10px;
                border-bottom: 1px solid white; /* Set border color to white */
                color: white;
                font-family: arial;
                font-size: 18px;
            }
            QHeaderView::section {
                background-color: black; /* Set header background to black */
                color: white; 
                border: none;
                font-weight: 400;
                
                padding: 10px;
                font-family: arial;
                font-size: 20px;
                
            }
            
            """
        )

        self.table_widget.setHorizontalHeaderLabels(
            ["No", "Code", "Name", "Type", "Display", "Layout", "Add On", "Opsi"]
        )

        header = self.table_widget.horizontalHeader()
        header.setStyleSheet(
            """
            background-color: black; /* Set header background to black */
            color: white; /* Set header text color to white */

           
            border: none;
            """
        )

        self.table_widget.setShowGrid(False)
        self.table_widget.verticalHeader().setVisible(False)

        self.table_widget.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        # Set alignment for specific header sections (kolom 3 dan 4)
        self.table_widget.horizontalHeaderItem(1).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(2).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(3).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(4).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(5).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(6).setTextAlignment(Qt.AlignCenter)

        self.table_widget.horizontalHeader().resizeSection(0, int(35 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(1, int(100 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(2, int(130 * width_ratio))  # Atur lebar kolom 3 secara manual
        self.table_widget.horizontalHeader().resizeSection(3, int(100 * width_ratio))  # Atur lebar kolom 3 secara manual
        self.table_widget.horizontalHeader().resizeSection(4, int(105 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(5, int(90 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(6, int(255 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(7, int(100 * width_ratio))

        # self.table_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.add_data_to_table()

    def add_data_to_table(self):
        # data_camera = []
        camera_area = self.get_data_camera('setting_camera_area')
        camera_gender = self.get_data_camera('setting_camera_gender')
        camera_in_out = self.get_data_camera('setting_camera')
        site_code = self.get_cosntant_by_code('site_code')

        book1 = []
        book2 = self.get_camera_info(book1, camera_in_out, 0, 9, 1, 25, 23, 24, 18)
        book3 = self.get_camera_info(book2, camera_gender, 0, 3, 1, 25, 23, 24, 17)
        books = self.get_camera_info(book3, camera_area, 0, 3, 1, 20, 18, 19, 10)

        # print(books)
        for row, (code, nama, display, layout, add_on, id_cam, type_cam) in enumerate(books):
            self.table_widget.insertRow(row)

            no = QTableWidgetItem(f"{row+ 1}")
            self.table_widget.setItem(row, 0, no)

            code_title = QTableWidgetItem(code)
            code_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 1, code_title)

            nama_title = QTableWidgetItem(nama)
            nama_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 2, nama_title)

            if type_cam == 1:
                type_camera = "Visitor"
            elif type_cam == 2:
                type_camera = "Gender"
            elif type_cam == 3:
                type_camera = "Zone"
            elif type_cam == 4:
                type_camera = "Outer Traffic"
            type_title = QTableWidgetItem(type_camera)
            type_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 3, type_title)
            
            layout_icon_widget = QWidget()
            layout_icon_widget.setStyleSheet("QWidget { background-color: transparent;}")
            layout_widget = QHBoxLayout()
            switch_button_layout = MySwitch()
            if display == 1:
                switch_button_layout.setChecked(True)  # Mengatur status menjadi ON
            else:
                switch_button_layout.setChecked(False) # Mengatur status menjadi OFF            
            switch_button_layout.stateChanged.connect(
                lambda state, id_cam=id_cam, type_cam=type_cam: self.update_database(id_cam, type_cam, state)
            )

            layout_widget.addWidget(switch_button_layout)            
            layout_icon_widget.setLayout(layout_widget)
            self.table_widget.setCellWidget(row, 4, layout_icon_widget)
            self.table_widget.setRowHeight(row, 60)
            
            layout_title = QTableWidgetItem(str(layout))
            layout_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 5, layout_title)

            add_on_title = QTableWidgetItem(str(add_on))
            add_on_title.setTextAlignment(Qt.AlignCenter)
            self.table_widget.setItem(row, 6, add_on_title) 

            layout_icon_widget = QWidget()
            layout_icon_widget.setStyleSheet("QWidget { background-color: transparent;}")
            layout_widget = QHBoxLayout()
            button = QPushButton("Edit")
            button.setStyleSheet(
                """
                    background-color: #1e9fff;
                    color: white;
                    font-family: arial;
                    font-size: 16px;                    
                    height: 20px;
                    margin: 0px;
                    border-radius: 5px; /* Atur radius sudut */
                    opacity: 0px;
                """
            )
            button.mousePressEvent = lambda event, id_data=id_cam, type_cam=type_cam: self.emit_menu_edit_monitoring_clicked(id_data, type_cam)
            layout_widget.addWidget(button)            
            layout_icon_widget.setLayout(layout_widget)
            self.table_widget.setCellWidget(row, 7, layout_icon_widget)
            self.table_widget.setRowHeight(row, 60)

    def clear_table(self):
        self.table_widget.clearContents()  # Menghapus konten tabel
        self.table_widget.setRowCount(0)  # Mengatur jumlah baris tabel menjadi 0

    def get_data_camera(self, table):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM {table} where deleted=1').fetchall()
        return data

    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data

    def get_camera_info(self, data_camera, camera_source, index_id, index_code, index_name, index_display, index_layout, index_add_on, index_type):
        nomor = 1
        # books = [('camera in out 1','A01','rtsp://camera:12345678@192.168.100.140:554/stream1','DKY005',"People-Counting",'1')]
        for cam in camera_source:
            id_cam = cam[index_id]
            code_cam = cam[index_code]
            nama_cam = cam[index_name]
            display_cam = cam[index_display]
            layout_cam = cam[index_layout]
            add_on_cam = cam[index_add_on]
            type_cam = cam[index_type]
            camera_info = (                
                code_cam,
                nama_cam,
                display_cam,
                layout_cam,
                add_on_cam,
                id_cam,
                type_cam
            )
            # print(camera_info)
            nomor +=1
            data_camera.append(camera_info)
        return data_camera
    
    def update_database(self, id_cam, type_cam, status):
        # print(f"{status} {id_cam} {type_cam}")
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        
        if status == True:
            val = 1
        else:
            val = 0

        if type_cam == 1 or type_cam == 4:
            table = "setting_camera"
        elif type_cam == 2:
            table = 'setting_camera_gender'
        elif type_cam == 3: 
            table = 'setting_camera_area'

        update_query = f"UPDATE {table} SET display = ? where id = ?"
        cursor.execute(update_query, (val, id_cam))
        conn.commit()        
        cursor.close()
        conn.close()

    def emit_menu_edit_monitoring_clicked(self, id_data, type_cam):
        # print("Menu edit cam list clicked")
        self.edit_monitoring_clicked.emit(id_data, type_cam)
# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_monitoring_setup()
#     window.show()
#     sys.exit(app.exec_())
