import sys
import os
import subprocess
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QLineEdit
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal

import sqlite3
from sidebar import App_sidebar

class App_snapshot_edit(QMainWindow):
    back_snapshot_clicked = pyqtSignal()
    save_snapshot_clicked = pyqtSignal()

    def __init__(self, id_data):
        super().__init__()
        # self.set_parameter(id_data)

    def set_parameter(self, id_data):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"SELECT * FROM t_snapshot WHERE id = {id_data}")
        data = cursor.fetchone()
        conn.commit()                
        conn.close()
        self.datas = data
        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()
        self.update_form()  # Panggil update_form untuk pengaturan awal


    def update_form(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        # Main Content Area
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(250 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" Edit Snapshot ")

        # Label dan input field untuk username
        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Filename")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 240, 170, 36)
        
        self.filename = QtWidgets.QLineEdit(self)
        self.filename.setFont(QtGui.QFont("Inter", 16))
        self.filename.setGeometry(600, 240, 400, 36)
        self.filename.setText(self.datas[1])
        self.filename.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")
        self.filename.setEnabled(False)

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Camera Type")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 310, 170, 36)
        
        self.cam_type = QtWidgets.QComboBox(self)
        self.cam_type.addItem("Visitor")
        self.cam_type.addItem("Gender")
        self.cam_type.addItem("Zone")
        self.cam_type.addItem("Outer Traffic")
        self.cam_type.setFont(QtGui.QFont("Inter", 16))
        self.cam_type.setGeometry(600, 310, 400, 36)
        self.cam_type.setStyleSheet(
            "QComboBox {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
            "QComboBox::drop-down:button{background-color: #5C6164;width: 40px;}"
            "QComboBox::down-arrow {image: url(img/arrrow.png);width: 40px;height: 36px ;padding: 5px;}"
            "QComboBox::item { color: white; }" # Mengatur warna teks item menjadi putih
        )
        self.cam_type.setCurrentIndex(int(self.datas[2]) - 1)
        self.cam_type.setEnabled(False)

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Deskripsi")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 380, 170, 36)

        self.deskripsi = QtWidgets.QTextEdit(self)
        self.deskripsi.setFont(QtGui.QFont("Inter", 16))
        self.deskripsi.setGeometry(600, 380, 400, 190)
        self.deskripsi.setText(self.datas[5])
        self.deskripsi.setStyleSheet(
            "border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);"
            "padding: 5px;"  # Menambahkan padding agar teks terlihat lebih baik
        )

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Image")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 600, 170, 36)

        if self.datas[2] == "1":
            lokasi_img = f"snapshot/people_counting/{self.datas[1]}"
        elif self.datas[2] == "2":
            lokasi_img = f"snapshot/gender/{self.datas[1]}"
        elif self.datas[2] == "3":
            lokasi_img = f"snapshot/area/{self.datas[1]}"
        elif self.datas[2] == "4":
            lokasi_img = f"snapshot/kiri_kanan/{self.datas[1]}"
        self.label8 = QtWidgets.QLabel(self)
        self.label8.setGeometry(600, 600, 500, 305)
        pixmap = QtGui.QPixmap(lokasi_img)
        self.label8.setPixmap(pixmap)
        self.label8.setScaledContents(True)

        add_data_button = QPushButton("Back", self)
        add_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1175 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        add_data_button.clicked.connect(self.back_button_clicked)

        add_data_button = QPushButton("Save", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1100 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        add_data_button.clicked.connect(self.save_button_clicked)

    def back_button_clicked(self):
        self.back_snapshot_clicked.emit()        

    def save_button_clicked(self):
        print(f"save snapshot {self.datas[0]}")
        deskripsi = self.deskripsi.toPlainText()
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        update_query = "UPDATE t_snapshot SET deskripsi = ? where id = ?"
        cursor.execute(update_query, (deskripsi, self.datas[0]))
        conn.commit()
        cursor.close()
        conn.close()

        self.save_snapshot_clicked.emit()

        

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_snapshot_edit("8")
#     window.show()
#     sys.exit(app.exec_())
