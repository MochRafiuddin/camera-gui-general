from flask import Flask, Response, render_template
import cv2
from multiprocessing.managers import BaseManager

app = Flask(__name__)

class QueueManager(BaseManager):
    pass

QueueManager.register('get_processed_frame_queue')

if __name__ == '__main__':
    manager = QueueManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    manager.connect()
    
    def generate_frames(id_camera, type_camera):
        while True:
            # Use the proxy object directly without calling it
            # print(manager.get_processed_frame_queue(id_camera, type_camera))
            frame = manager.get_processed_frame_queue(id_camera, type_camera).get()            
            _, jpeg = cv2.imencode('.jpg', frame)
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')

    @app.route('/video_feed/<int:id_camera>/<int:type_camera>')
    def video_feed(id_camera, type_camera):
        return Response(generate_frames(id_camera, type_camera), mimetype='multipart/x-mixed-replace; boundary=frame')

    app.run(host='0.0.0.0', port=5001)
