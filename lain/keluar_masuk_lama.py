import cv2
import torch
import time
import numpy as np
import sqlite3
import datetime

class CentroidTracker:
    def __init__(self, max_disappeared=40):
        self.next_object_id = 0
        self.objects = {}
        self.disappeared = {}
        self.max_disappeared = max_disappeared

    def register(self, centroid):
        self.objects[self.next_object_id] = centroid
        self.disappeared[self.next_object_id] = 0
        self.next_object_id += 1

    def deregister(self, object_id):
        del self.objects[object_id]
        del self.disappeared[object_id]

    def update(self, rects):
        if len(rects) == 0:
            for object_id in list(self.disappeared.keys()):
                self.disappeared[object_id] += 1

                if self.disappeared[object_id] > self.max_disappeared:
                    self.deregister(object_id)

            return self.objects, {}  # Mengembalikan dictionary kosong jika tidak ada kotak yang diperbarui

        input_centroids = np.zeros((len(rects), 2), dtype="int")
        input_boxes = {}  # Store bounding boxes for drawing rectangles

        for (i, (startX, startY, endX, endY)) in enumerate(rects):
            cX = int((startX + endX) / 2.0)
            cY = int((startY + endY) / 2.0)
            input_centroids[i] = (cX, cY)
            input_boxes[i] = (startX, startY, endX, endY)  # Store bounding box coordinates

        if len(self.objects) == 0:
            for i in range(0, len(input_centroids)):
                self.register(input_centroids[i])
        else:
            object_centroids = list(self.objects.values())
            object_ids = list(self.objects.keys())
            object_centroids = np.array(object_centroids)
            input_centroids = np.array(input_centroids)

            D = np.linalg.norm(object_centroids[:, np.newaxis] - input_centroids, axis=2)

            rows = D.min(axis=1).argsort()
            cols = D.argmin(axis=1)[rows]

            used_rows = set()
            used_cols = set()

            for (row, col) in zip(rows, cols):
                if row in used_rows or col in used_cols:
                    continue

                object_id = object_ids[row]
                self.objects[object_id] = input_centroids[col]
                self.disappeared[object_id] = 0

                used_rows.add(row)
                used_cols.add(col)

            unused_rows = set(range(0, D.shape[0])).difference(used_rows)
            unused_cols = set(range(0, D.shape[1])).difference(used_cols)

            if D.shape[0] >= D.shape[1]:
                for row in unused_rows:
                    object_id = object_ids[row]
                    self.disappeared[object_id] += 1

                    if self.disappeared[object_id] > self.max_disappeared:
                        self.deregister(object_id)
            else:
                for col in unused_cols:
                    self.register(input_centroids[col])

        # Return object centroids
        return self.objects, input_boxes

class VideoCamera(object):

    def __init__(self):
        # self.video = cv2.VideoCapture(camera[2])  # Ganti 0 dengan alamat URL atau indeks kamera jika menggunakan sumber video lain
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model = torch.hub.load('yolov5', 'custom', 'models/crowdhuman_yolov5m.pt', source='local', force_reload=True).to(device).eval()
        self.device = device
        self.model = model
        self.centroid_tracker = CentroidTracker(max_disappeared=30)
        self.fps = 0        
        self.frame_count = 0
        self.start_time = time.time()

    # def __del__(self):
    #     self.video.release()

    def get_frame(self, frame, id_camera):
        # success, frame = self.video.read()
        self.id_data = id_camera
        self.data = self.get_peopel_hari_ini()
        people_kanan = eval(self.data[6]) if self.data[6] else []
        people_kiri =  eval(self.data[7]) if self.data[7] else []
        array_people_masuk =  eval(self.data[15]) if self.data[15] else []
        in_jembatan = self.data[8]
        people_in = self.data[11]
        people_out = self.data[12]
        enter_jam = self.data[16]
        label = self.data[17]
        data_garis = self.data[4]
        data_roi = self.data[3]
        rasio_width = self.data[21]
        rasio_height = self.data[22]

        height, width, _ = frame.shape
        # Konversi warna BGR ke RGB (PyTorch menggunakan format RGB)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # frame_count = self.frame_count
        # # fps = self.fps
        # frame_count += 1
        self.frame_count += 1
        if self.frame_count >= 15:
            end_time = time.time()
            elapsed_time = end_time - self.start_time
            self.fps = self.frame_count / elapsed_time
            self.frame_count = 0
            self.start_time = time.time()

            # Display FPS on the frame
        cv2.putText(frame, f"FPS: {self.fps:.2f}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        cv2.putText(frame, f"{label}", (10, height - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
        
        # Deteksi objek menggunakan YOLOv5
        garis = eval(data_garis) if data_garis else []
        if len(garis) == 2:
            # center_x = (top_point[0] + bottom_point[0]) // 2
            cv2.line(frame, (int(garis[0][0] * float(rasio_width)), int(garis[0][1] * float(rasio_height))),
                        (int(garis[1][0] * float(rasio_width)), int(garis[1][1] * float(rasio_height))),
                        (0, 0, 0), thickness=2)
        elif len(garis) < 2:
            for line in garis:
                cv2.circle(frame, (int(line[0] * float(rasio_width)), int(line[1] * float(rasio_height))), 4, (0, 0, 0), -1)
        
        roi = eval(data_roi) if data_roi else []
        if len(roi) > 3:
            roi_coords = np.array(roi, np.float64)
            roi_coords[:, 0] *= float(rasio_width)
            roi_coords[:, 1] *= float(rasio_height)
            roi_coords = roi_coords.astype(np.int32)
            roi_coords = roi_coords.reshape((-1, 1, 2))
            # Draw the polygon on the frame
            cv2.polylines(frame, [roi_coords], True, (0, 0, 255), thickness=2)
            # Crop frame to the specified ROI coordinates
            mask = np.zeros_like(frame)
            cv2.fillPoly(mask, [roi_coords], (255, 255, 255))
            roi_frame = cv2.bitwise_and(frame, mask)  # Lakukan deteksi objek pada ROI menggunakan model YOLOv5
            results, input_boxes = self.detect_objects(roi_frame)
            frame = self.draw_boxes(frame, input_boxes)
        else :
            results, input_boxes = self.detect_objects(frame)
            frame = self.draw_boxes(frame, input_boxes)
        # if len(input_boxes) > 0:
        for (object_id, centroid) in results.items():            
            # startX, startY, endX, endY = input_boxes[object_id]
            text = f"ID {object_id}"
            nama_camera =self.data[1]
            cv2.putText(frame, f"ID {object_id}", (centroid[0], centroid[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)            
            if len(garis) == 2:
                if self.is_left(garis[0], garis[1], centroid): #ketika orang berada di sebelah kiri garis
                    in_jembatan, people_in, people_out, array_orang_masuk, enter_jam = self.detection_proces("kiri", object_id, self.data[5], people_kiri, people_kanan, array_people_masuk)
                    self.update_data(in_jembatan, people_in, people_out, people_kanan, people_kiri, array_orang_masuk, enter_jam)
                elif self.is_right(garis[0], garis[1], centroid): #ketika orang berada di sebelah kanan garis
                    in_jembatan, people_in, people_out, array_orang_masuk, enter_jam = self.detection_proces("kanan", object_id, self.data[5], people_kanan, people_kiri, array_people_masuk)
                    self.update_data(in_jembatan, people_in, people_out, people_kanan, people_kiri, array_orang_masuk, enter_jam)
        # Konversi kembali ke BGR untuk tampilan di OpenCV
        frame_bgr = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        # Konversi frame ke format JPEG
        # ret, jpeg = cv2.imencode('.jpg', frame_bgr)
        # return jpeg.tobytes()
        ratio = width / height

        return frame_bgr

    def detect_objects(self, frame):
        results = self.model(frame)
        objects = []

        for index, row in results.pandas().xyxy[0].iterrows():
            confidence = float(row['confidence'])
            class_name = row['name']
            if class_name != "head":
                continue
            if confidence > 0.375:
                x1 = int(row['xmin'])
                y1 = int(row['ymin'])
                x2 = int(row['xmax'])
                y2 = int(row['ymax'])
                class_name = str(row['name'])
                objects.append({'label': class_name, 'confidence': confidence, 'bbox': row})

        # Update CentroidTracker with detected objects' bounding boxes
        bounding_boxes = [(int(obj['bbox']['xmin']), int(obj['bbox']['ymin']), int(obj['bbox']['xmax']), int(obj['bbox']['ymax'])) for obj in objects]
        bounding_boxes_conf = [(int(obj['bbox']['xmin']), int(obj['bbox']['ymin']), int(obj['bbox']['xmax']), int(obj['bbox']['ymax']),float(obj["confidence"])) for obj in objects]
        objects, input_boxes = self.centroid_tracker.update(bounding_boxes)
        return objects, bounding_boxes_conf
    
    def draw_boxes(self, frame, bounding_boxes):
        # Loop through each bounding box
        for bbox in bounding_boxes:
            # Extract coordinates from the bounding box
            x1, y1, x2, y2, conf = bbox
            titik_tengah_x = int((x1 + x2) / 2)
            titik_tengah_y = int((y1 + y2) / 2)

            # Draw the bounding box on the frame
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 2)
            # cv2.putText(frame, f"{str(conf)}", (titik_tengah_x, titik_tengah_y + 15), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)           

        return frame

    def is_left(self, p1, p2, p):
        return (p2[0] - p1[0]) * (p[1] - p1[1]) - (p[0] - p1[0]) * (p2[1] - p1[1]) > 0

    def is_right(self, p1, p2, p):
        return not self.is_left(p1, p2, p)
    
    def update_data(self, new_value, masuk, keluar, people_kanan , people_kiri, array_people_masuk, enter_jam):
        record_id = self.data[0]

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        update_query = "UPDATE setting_camera SET people_kanan = ?, people_kiri = ?, in_jembatan = ?, people_in = ?, people_out = ?, people_masuk = ?, people_in_jam = ? WHERE id = ?"
        
        # Execute the update query
        cursor.execute(update_query, (str(people_kanan) , str(people_kiri), new_value, masuk, keluar, str(array_people_masuk), enter_jam, record_id))
        
        # Commit the changes
        conn.commit()
        
        cursor.close()
        conn.close()

    def get_peopel_hari_ini(self):
        
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        # Create the table if it doesn't exist
        data = cursor.execute(f'SELECT * FROM setting_camera where id="{self.id_data}"').fetchone()

        return data

    def hitung(self,inp, ket, object_id):

        data= self.get_peopel_hari_ini()
        in_jembatan = data[8]
        enter = data[11]
        exit = data[12]
        enter_jam = data[16]
        array_people_masuk =  eval(data[15]) if data[15] else []
        type_cam = data[18]
        if ket == "masuk":
            enter +=  inp
            # if object_id not in array_people_masuk:
            #     enter += inp
            #     enter_jam += inp
            self.add_value_constant(type_cam, ket)
            # print(f"camera {self.data[1]}. masuk {enter}, keluar {exit}")
            self.update_log_txt(f"camera {self.data[1]}. masuk {enter}, keluar {exit}")
            # self.insert_data_log(f"camera {self.data[1]} masuk {enter}, keluar {exit}")
        else :
            # in_jembatan = int(in_jembatan) - inp
            exit += inp 
            self.add_value_constant(type_cam, ket)
            # print(f"camera {self.data[1]}. masuk {enter}, keluar {exit}")
            self.update_log_txt(f"camera {self.data[1]}. masuk {enter}, keluar {exit}")
            # self.insert_data_log(f"camera {self.data[1]} masuk {enter}, keluar {exit}")

        return in_jembatan, enter, exit, enter_jam
    
    def detection_proces(self, side_of_line, object_id, JenisPintu, array_asal, array_lawan, array_people_masuk):
        if object_id not in array_asal: 
            #setelah itu cek ke DB, dia pitu masuk apa keluar
            array_asal.append(object_id)
            # print(array_asal)
        param2 = ""
        data= self.get_peopel_hari_ini()
        in_jembatan = data[8]
        enter = data[11]
        exit = data[12]
        enter_jam = data[16]
        if object_id in array_lawan:
            array_lawan.remove(object_id)
            if side_of_line == "kiri" :
                if JenisPintu == "masuk":
                    param2 = "keluar"
                elif JenisPintu == "keluar":
                    param2 = "masuk"
                    if object_id not in array_people_masuk:
                        array_people_masuk.append(object_id)
            else :
                if JenisPintu == "masuk":
                    param2 = "masuk"
                    if object_id not in array_people_masuk:
                        array_people_masuk.append(object_id)

                elif JenisPintu == "keluar":
                    param2 = "keluar"
            in_jembatan, enter, exit, enter_jam = self.hitung(1, param2, object_id)

        return in_jembatan, enter, exit, array_people_masuk, enter_jam

    def insert_data_log(self, log):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        waktu = time.time()
        created_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(waktu))
        insert_query = "INSERT INTO sistem_logs (log, created_at) VALUES (?, ?)"
        cursor.execute(insert_query, (log, created_at))
        conn.commit()        
        cursor.close()
        conn.close()

    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data
    
    def update_current_deteksi(self, value, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"UPDATE constant SET value = {value} WHERE code = '{code}'")
        conn.commit()
        cursor.close()
        conn.close()
    
    def add_value_constant(self, type, keterangan):
        if int(type) == 1 :
            visitor_daily_in = self.get_cosntant_by_code('visitor_daily_in')
            visitor_daily_out = self.get_cosntant_by_code('visitor_daily_out')
            if keterangan == "masuk":
                # print("updatre masuk")
                self.update_current_deteksi(int(visitor_daily_in[2])+1, 'visitor_daily_in')
            else :
                self.update_current_deteksi(int(visitor_daily_out[2])+1, 'visitor_daily_out')

        elif int(type) == 4 :
            outer_traffic_left = self.get_cosntant_by_code('outer_traffic_left')
            outer_traffic_right = self.get_cosntant_by_code('outer_traffic_right')
            # outer_traffic_total_traffic = self.get_cosntant_by_code('outer_traffic_total_traffic')
            if keterangan == "masuk":
                self.update_current_deteksi(int(outer_traffic_left[2])+1, 'outer_traffic_left')
                self.update_current_deteksi(int(outer_traffic_left[2])+1+int(outer_traffic_right[2]), 'outer_traffic_total_traffic')
            else :
                self.update_current_deteksi(int(outer_traffic_right[2])+1, 'outer_traffic_right')
                self.update_current_deteksi(int(outer_traffic_right[2])+1+int(outer_traffic_left[2]), 'outer_traffic_total_traffic')

    def update_log_txt(self, new_segment):
        log_file = 'detection.txt'
        now = datetime.datetime.today()
        formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")

        with open(log_file, 'a') as f:        
            f.write(f'{formatted_now} -- {new_segment}\n')