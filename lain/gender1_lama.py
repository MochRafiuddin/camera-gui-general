# -*- coding: UTF-8 -*-
import argparse
import time
from pathlib import Path
import sys
import os
import datetime
import numpy as np
import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
import copy
import torchvision.transforms as transforms # Tensor Transformation.
import torchvision.models as models # ResNet-18 PyTorch Model.
from torch import nn # Neural Network Layers
import sqlite3
# import torch.nn.functional as F
import torchvision.transforms.functional as F
# from log_app import MainWindow

class CentroidTracker:
    def __init__(self, max_disappeared=40):
        self.next_object_id = 0
        self.objects = {}
        self.disappeared = {}
        self.max_disappeared = max_disappeared

    def register(self, rect):
        self.objects[self.next_object_id] = rect
        self.disappeared[self.next_object_id] = 0
        self.next_object_id += 1

    def deregister(self, object_id):
        del self.objects[object_id]
        del self.disappeared[object_id]

    def update(self, rects):
        if len(rects) == 0:
            for object_id in list(self.disappeared.keys()):
                self.disappeared[object_id] += 1

                if self.disappeared[object_id] > self.max_disappeared:
                    self.deregister(object_id)

            return self.objects

        input_rects = np.array(rects)

        if len(self.objects) == 0:
            for rect in input_rects:
                self.register(tuple(rect))
        else:
            object_rects = np.array(list(self.objects.values()))

            D = np.zeros((len(input_rects), len(object_rects)))
            for i, rect in enumerate(input_rects):
                cX = rect[0] + rect[2] / 2
                cY = rect[1] + rect[3] / 2
                for j, object_rect in enumerate(object_rects):
                    dX = cX - object_rect[0] - object_rect[2] / 2
                    dY = cY - object_rect[1] - object_rect[3] / 2
                    D[i, j] = np.sqrt(dX * dX + dY * dY)

            if D.size > 0:
                rows = D.min(axis=1).argsort()
                cols = D.argmin(axis=1)[rows]

                used_rows = set()
                used_cols = set()

                for (row, col) in zip(rows, cols):
                    if row in used_rows or col in used_cols:
                        continue

                    if col < len(list(self.objects.keys())):
                        object_id = list(self.objects.keys())[col]
                        self.objects[object_id] = tuple(input_rects[row])
                        self.disappeared[object_id] = 0

                        used_rows.add(row)
                        used_cols.add(col)

                unused_rows = set(range(0, D.shape[0])).difference(used_rows)
                unused_cols = set(range(0, D.shape[1])).difference(used_cols)

                for row in unused_rows:
                    rect = input_rects[row]
                    self.register(tuple(rect))

                for col in unused_cols:
                    if col < len(list(self.objects.keys())):
                        object_id = list(self.objects.keys())[col]
                        self.disappeared[object_id] += 1

                        if self.disappeared[object_id] > self.max_disappeared:
                            self.deregister(object_id)

        return self.objects

class face_gender():
    def __init__(self) -> None:
        # self.data = input_video_path
        # self.video = cv2.VideoCapture(input_video_path[2])  # Ganti 0 dengan alamat URL atau indeks kamera jika menggunakan sumber video lain
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        FaceDetector = torch.hub.load('yolov5', 'custom', 'models/YOLO/Best.onnx', source='local', _verbose=False, force_reload=True)
        FaceDetector.eval().to(device)

        Classes = 9
        Groups = ['00-10', '11-20', '21-30', 
                '31-40', '41-50', '51-60', 
                '61-70', '71-80', '81-90']

        ClassificationModel2 = 'models/ResNet-18/ResNet-18 Age 0.60 + Gender 93.pt'
        FaceClassifier = models.resnet18(pretrained=True)
        FaceClassifier.fc = nn.Linear(512, Classes+2)
        FaceClassifier = nn.Sequential(FaceClassifier, nn.Sigmoid())
        FaceClassifier.load_state_dict(torch.load(ClassificationModel2))
        FaceClassifier.eval()
        FaceClassifier.to(device)

        self.model = FaceDetector
        self.FaceClassifier = FaceClassifier
        self.device = device
        self.centroid_tracker = CentroidTracker(max_disappeared=30)
        # self.log_app = MainWindow()

    # def __del__(self):
    #     self.video.release()

    def scale_coords_landmarks(self, img1_shape, coords, img0_shape, ratio_pad=None):
        # Rescale coords (xyxy) from img1_shape to img0_shape
        if ratio_pad is None:  # calculate from img0_shape
            gain = min(img1_shape[0] / img0_shape[0], img1_shape[1] / img0_shape[1])  # gain  = old / new
            pad = (img1_shape[1] - img0_shape[1] * gain) / 2, (img1_shape[0] - img0_shape[0] * gain) / 2  # wh padding
        else:
            gain = ratio_pad[0][0]
            pad = ratio_pad[1]

        coords[:, [0, 2, 4, 6, 8]] -= pad[0]  # x padding
        coords[:, [1, 3, 5, 7, 9]] -= pad[1]  # y padding
        coords[:, :10] /= gain
        #clip_coords(coords, img0_shape)
        coords[:, 0].clamp_(0, img0_shape[1])  # x1
        coords[:, 1].clamp_(0, img0_shape[0])  # y1
        coords[:, 2].clamp_(0, img0_shape[1])  # x2
        coords[:, 3].clamp_(0, img0_shape[0])  # y2
        coords[:, 4].clamp_(0, img0_shape[1])  # x3
        coords[:, 5].clamp_(0, img0_shape[0])  # y3
        coords[:, 6].clamp_(0, img0_shape[1])  # x4
        coords[:, 7].clamp_(0, img0_shape[0])  # y4
        coords[:, 8].clamp_(0, img0_shape[1])  # x5
        coords[:, 9].clamp_(0, img0_shape[0])  # y5
        return coords

    def show_results(self, img, xyxy, conf, landmarks, class_num, Age="", Gender=""):
        h,w,c = img.shape
        tl = 1 or round(0.002 * (h + w) / 2) + 1  # line/font thickness
        x1 = int(xyxy[0])
        y1 = int(xyxy[1])
        x2 = int(xyxy[2])
        y2 = int(xyxy[3])
        img = img.copy()
        
        cv2.rectangle(img, (x1,y1), (x2, y2), (0,255,0), thickness=tl, lineType=cv2.LINE_AA)

        tf = max(tl - 1, 1)  # font thickness
        label = f"({Age}) - {Gender}: {conf:.2f}"
        cv2.putText(img, label, (x1, y1 - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)
        return img

    def resizeImage(self, IMG):
        # Resize image to 640x640
        IMG = cv2.resize(IMG, (640, 640))
        IMG = cv2.cvtColor(IMG, cv2.COLOR_BGR2RGB)
        return IMG
    
    def extractInfo(self, MyModel, tensorIMG, runOn, Verbosity=False):
        Classes = 9
        Groups = ['00-10', '11-20', '21-30', 
                '31-40', '41-50', '51-60', 
                '61-70', '71-80', '81-90']
        # tensorIMG = torch.from_numpy(tensorIMG).to(runOn)
        tensorLabels = MyModel(tensorIMG)[0]

        Age = torch.argmax(tensorLabels[:Classes])
        Gender = int(torch.argmax(tensorLabels[Classes:]))
        Gender = 'Male' if Gender == 0 else 'Female'

        C1 = float(torch.max(tensorLabels[:Classes]))
        C2 = float(torch.max(tensorLabels[Classes:]))

        if Verbosity: 
            output = [round(float(x), 3) for x in tensorLabels]
            print(output)
        return Groups[Age], Gender, [round(C1, 3), round(C2, 3)]
    
    def extractFace(self, IMG, FaceDetector, threshold, returnFace=True):
        extractedFaces = []
        extractedBoxes = []
        img_np = np.array(IMG)
        img_np = cv2.cvtColor(img_np, cv2.COLOR_RGB2BGR)
        FaceDetections = FaceDetector(img_np)  # No need for .pandas().xyxy[0]
        for detection in FaceDetections.xyxy[0]:
            xmin, ymin, xmax, ymax, confidence = detection[:5]
            if confidence >= threshold:
                bb = [(int(xmin), int(ymin)), (int(xmax), int(ymax))]  # Convert to integers
                if returnFace:
                    currentFace = img_np[int(ymin):int(ymax), int(xmin):int(xmax)]
                    extractedFaces.append(currentFace)
                extractedBoxes.append(bb)

        return extractedFaces, extractedBoxes
    
    def readImage(self, IMG):
        # Assuming IMG is the cropped NumPy array
        img_np = cv2.cvtColor(IMG, cv2.COLOR_RGB2BGR)
        
        # Convert to tensor dengan memastikan tensor dihasilkan di GPU jika tersedia
        tensorIMG = torch.tensor(img_np, device=self.device).permute(2, 0, 1).unsqueeze(0).float() / 255.0
        
        # Lakukan transformasi Resize secara langsung di GPU
        tensorIMG = F.resize(tensorIMG, (200, 200))
        
        # Normalisasi dapat dilakukan setelah transformasi Resize
        tensorIMG = F.normalize(tensorIMG, mean=(0.5,), std=(0.5,))
        
        return tensorIMG
    
    def returnAnalysis(self,IMG, original_shape):
        # facesData = []
        bbs_scale = []
        img_copy = IMG.copy()
        IMG_resized = self.resizeImage(img_copy)
        # IMG_resized = Image.fromarray(IMG_resized)
        faces, bbs = self.extractFace(IMG_resized, self.model, 0.3)

        # Calculate the resize factor
        resize_factor_x = original_shape[1] / IMG_resized.shape[1]
        resize_factor_y = original_shape[0] / IMG_resized.shape[0]

        for face, bb in zip(faces, bbs):
            # Rescale bounding box coordinates
            bbox = ((bb[0][0] * resize_factor_x, bb[0][1] * resize_factor_y),
                (bb[1][0] * resize_factor_x, bb[1][1] * resize_factor_y))
            bbs_scale.append(bbox)
            # tensorIMG = self.readImage(face)            
            # Age, Gender, C = self.extractInfo(self.FaceClassifier, tensorIMG, self.device)            
            # textBox = f'{Age} {Gender}'
            # facesData.append(textBox)
        return bbs_scale
    
    def draw_label(self, img, text, pos, bg_color):
        font_face = cv2.FONT_HERSHEY_SIMPLEX
        scale = 0.4
        color = (255, 255, 255)
        thickness = cv2.FILLED
        margin = 2
        txt_size = cv2.getTextSize(text, font_face, scale, thickness)
        
        end_x = pos[0] + txt_size[0][0] + margin
        end_y = pos[1] - txt_size[0][1] - margin

        cv2.rectangle(img, pos, (end_x, end_y), bg_color, thickness)
        cv2.putText(img, text, pos, font_face, scale, color, 1, cv2.LINE_AA)

    def detect_faces(self, frame, id_data):
        # Load model
        self.id_data = id_data
        self.data = self.get_peopel_hari_ini()
        # print(data)
        people_kiri =  eval(self.data[12]) if self.data[12] else []
        people_kanan = eval(self.data[13]) if self.data[13] else []
        array_people_masuk = eval(self.data[14]) if self.data[14] else []
        rasio_width = self.data[21]
        rasio_height = self.data[22]
        # male_in = data[8]
        # male_out = data[9]
        # female_in = data[10]
        # female_out = data[11]
        # ret, frame = self.video.read()
        original_shape = frame.shape
        half_w0 = original_shape[1] // 2

        # Membuat garis vertikal di tengah gambar
        garis = eval(self.data[18]) if self.data[18] else []
        if len(garis) == 2:
            # center_x = (top_point[0] + bottom_point[0]) // 2
            cv2.line(frame, (int(garis[0][0] * float(rasio_width)), int(garis[0][1] * float(rasio_height))),
                        (int(garis[1][0] * float(rasio_width)), int(garis[1][1] * float(rasio_height))),
                        (0, 0, 0), thickness=2)
        elif len(garis) < 2:
            for line in garis:
                cv2.circle(frame, (int(line[0] * float(rasio_width)), int(line[1] * float(rasio_height))), 4, (0, 0, 0), -1)
        
        roi = eval(self.data[19]) if self.data[19] else []
        if len(roi) > 3:
            roi_coords = np.array(roi, np.float64)
            roi_coords[:, 0] *= float(rasio_width)
            roi_coords[:, 1] *= float(rasio_height)
            roi_coords = roi_coords.astype(np.int32)
            roi_coords = roi_coords.reshape((-1, 1, 2))
            # Draw the polygon on the frame
            cv2.polylines(frame, [roi_coords], True, (0, 0, 255), thickness=2)
            # Crop frame to the specified ROI coordinates
            mask = np.zeros_like(frame)
            cv2.fillPoly(mask, [roi_coords], (255, 255, 255))
            roi_frame = cv2.bitwise_and(frame, mask)  # Lakukan deteksi objek pada ROI menggunakan model YOLOv5
            BoundingBoxes = self.returnAnalysis(roi_frame, original_shape)
        else :            
            BoundingBoxes = self.returnAnalysis(frame, original_shape)
        objects = []
        for bb in BoundingBoxes:
            (x, y), (w, h) = bb
            objects.append({'bbox': (x, y, w, h)})
            # print('Face!')
            # cv2.rectangle(frame, (int(x),int(y)), (int(w),int(h)), (255,0,0), 2)
            
            # self.draw_label(frame, lbl, (int(x),int(y)), (255, 0, 0))
        bounding_boxes = [(int(obj['bbox'][0]), int(obj['bbox'][1]), int(obj['bbox'][2]), int(obj['bbox'][3])) for obj in objects]

        objects = self.centroid_tracker.update(bounding_boxes)
        for (object_id, centroid) in objects.items():            
            x1, y1, x2, y2 = centroid[0], centroid[1], centroid[2], centroid[3]
            x_center = (centroid[0] + centroid[2]) // 2
            y_center = (centroid[1] + centroid[3]) // 2
            titik = (int(x_center), int(y_center))                    
            cropped_image = frame[y1:y2, x1:x2, :]        
            tensorIMG = self.readImage(cropped_image)            
            Age, Gender, C = self.extractInfo(self.FaceClassifier, tensorIMG, self.device)            
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 2)
            cv2.putText(frame, f"ID {object_id} : {Gender}", (x1, y1 - 2), 0, 1 / 3, [225, 255, 255], thickness=1, lineType=cv2.LINE_AA)
            # if preview == 0:
            if self.is_left((int(half_w0),0), (int(half_w0),original_shape[1]), titik): #ketika orang berada di sebelah kiri garis
                # print("kiri")
                male_in, male_out, female_in, female_out = self.detection_proces("kiri", object_id, self.data[15], people_kiri, people_kanan, Gender)
                self.update_data(male_in, male_out, female_in, female_out, people_kanan, people_kiri)
            elif self.is_right((int(half_w0),0), (int(half_w0),original_shape[1]), titik): #ketika orang berada di sebelah kanan garis
                # print("kanan")
                male_in, male_out, female_in, female_out = self.detection_proces("kanan", object_id, self.data[15], people_kanan, people_kiri, Gender)
                self.update_data(male_in, male_out, female_in, female_out, people_kanan, people_kiri)
        return frame
    
    def is_left(self, p1, p2, p):
        return (p2[0] - p1[0]) * (p[1] - p1[1]) - (p[0] - p1[0]) * (p2[1] - p1[1]) > 0

    def is_right(self, p1, p2, p):
        return not self.is_left(p1, p2, p)
    
    def get_peopel_hari_ini(self):
        
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        # Create the table if it doesn't exist
        data = cursor.execute(f'SELECT * FROM setting_camera_gender where id="{self.id_data}"').fetchone()
        # print(data)
        return data
    
    def hitung(self,inp, ket, object_id, class_name):

        data= self.get_peopel_hari_ini()
        array_people_masuk = eval(data[14]) if data[14] else []
        male_in = data[8]
        male_out = data[9]
        female_in = data[10]
        female_out = data[11]

        if ket == "masuk":
            if class_name == "Male":
                male_in = male_in + inp
                self.add_value_constant(ket, class_name)
                # print("add male in")
            elif class_name == "Female":
                female_in = female_in + inp
                self.add_value_constant(ket, class_name)
                # print("add female in")
            # print(f"id masuk :{object_id}, gender {class_name}")
            # print(f"{male_in} - {male_out} - {female_in} - {female_out}")
            self.update_log_txt(f"id masuk :{object_id}, gender {class_name} -- {male_in} - {male_out} - {female_in} - {female_out} ")
            # self.insert_data_log(f"id masuk :{object_id}, gender {class_name}. male_in: {male_in}, male_out: {male_out}, female_in: {female_in}, female_out: {female_out}")
            # self.log_app.show_data(f"id masuk :{object_id}, gender {class_name}. male_in: {male_in}, male_out: {male_out}, female_in: {female_in}, female_out: {female_out}")
            # if object_id not in array_people_masuk:
            #     enter += inp
            #     enter_jam += inp
        else :
            if class_name == "Male":
                male_out = male_out + inp
                self.add_value_constant(ket, class_name)
                # print("add male out")
            elif class_name == "Female":
                female_out = female_out + inp
                self.add_value_constant(ket, class_name)
                # print("add female out")
            # print(f"id keluar :{object_id}, gender {class_name}")
            # print(f"{male_in} - {male_out} - {female_in} - {female_out}")
            self.update_log_txt(f"id masuk :{object_id}, gender {class_name} -- {male_in} - {male_out} - {female_in} - {female_out} ")
            # self.insert_data_log(f"id masuk :{object_id}, gender {class_name}. male_in: {male_in}, male_out: {male_out}, female_in: {female_in}, female_out: {female_out}")
            # self.log_app.show_data(f"id masuk :{object_id}, gender {class_name}. male_in: {male_in}, male_out: {male_out}, female_in: {female_in}, female_out: {female_out}")

        return male_in, male_out, female_in, female_out

    def detection_proces(self, side_of_line, object_id, JenisPintu, array_asal, array_lawan, class_name):
        if object_id not in array_asal: 
            #setelah itu cek ke DB, dia pitu masuk apa keluar
            array_asal.append(object_id)
            # print(array_asal)
        param2 = ""
        data= self.get_peopel_hari_ini()
        male_in = data[8]
        male_out = data[9]
        female_in = data[10]
        female_out = data[11]
        if object_id in array_lawan:
            array_lawan.remove(object_id)
            if side_of_line == "kiri" :
                if JenisPintu == "masuk":
                    param2 = "keluar"
                elif JenisPintu == "keluar":
                    param2 = "masuk"
                    # if object_id not in array_people_masuk:
                    #     array_people_masuk.append(object_id)
            else :
                if JenisPintu == "masuk":
                    param2 = "masuk"
                    # if object_id not in array_people_masuk:
                    #     array_people_masuk.append(object_id)

                elif JenisPintu == "keluar":
                    param2 = "keluar"
            male_in, male_out, female_in, female_out = self.hitung(1, param2, object_id, class_name)

        return male_in, male_out, female_in, female_out
    
    def update_data(self, male_in, male_out, female_in, female_out, people_kanan, people_kiri):
        record_id = self.data[0]

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        update_query = "UPDATE setting_camera_gender SET array_kanan = ?, array_kiri = ?, male_in = ?, male_out = ?, female_in = ?, female_out = ? WHERE id = ?"
        
        # Execute the update query
        cursor.execute(update_query, (str(people_kanan) , str(people_kiri), male_in, male_out, female_in, female_out, record_id))
        
        # Commit the changes
        conn.commit()
        # print("update success")
        
        cursor.close()
        conn.close()
    
    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data
    
    def update_current_deteksi(self, value, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"UPDATE constant SET value = {value} WHERE code = '{code}'")
        conn.commit()
        cursor.close()
        conn.close()
    
    def add_value_constant(self, keterangan, gender):
        gender_male_in = self.get_cosntant_by_code('gender_male_in')
        gender_male_out = self.get_cosntant_by_code('gender_male_out')
        gender_female_in = self.get_cosntant_by_code('gender_female_in')
        gender_female_out = self.get_cosntant_by_code('gender_female_out')
        if keterangan == "masuk": 
            if gender == "Male":
                self.update_current_deteksi(int(gender_male_in[2])+1, 'gender_male_in')
            else:
                self.update_current_deteksi(int(gender_female_in[2])+1, 'gender_female_in')
        else :
            if gender == "Male":
                self.update_current_deteksi(int(gender_male_out[2])+1, 'gender_male_out')
            else:
                self.update_current_deteksi(int(gender_female_out[2])+1, 'gender_female_out')
    
    def update_log_txt(self, new_segment):
        log_file = 'detection.txt'
        now = datetime.datetime.today()
        formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")

        with open(log_file, 'a') as f:        
            f.write(f'{formatted_now} -- {new_segment}\n')
    # def insert_data_log(self, log):
    #     conn = sqlite3.connect('setting.db')
    #     cursor = conn.cursor()
    #     waktu = time.time()
    #     created_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(waktu))
    #     insert_query = "INSERT INTO sistem_logs (log, created_at) VALUES (?, ?)"
    #     cursor.execute(insert_query, (log, created_at))
    #     conn.commit()        
    #     cursor.close()
    #     conn.close()