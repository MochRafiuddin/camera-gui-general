# Path to the SQLite DLL
$sqliteDllPath = "D:\PYTHON\camera-gui-general\sqlite-powersell\System.Data.SQLite.dll"

# Load the SQLite assembly
Add-Type -Path $sqliteDllPath

# Path to the SQLite database file
$databasePath = "D:\PYTHON\camera-gui-general\setting.db"

# Connection string to the SQLite database
$connectionString = "Data Source=$databasePath;Version=3;"

# SQL query to retrieve data
$query = "SELECT * FROM constant where code='record_time';"

# Function to execute the query and retrieve data
function Get-SQLiteData {
    param (
        [string]$connectionString,
        [string]$query
    )

    # Create a new SQLite connection
    $connection = New-Object System.Data.SQLite.SQLiteConnection($connectionString)

    # Open the connection
    $connection.Open()

    # Create a new SQLite command
    $command = $connection.CreateCommand()
    $command.CommandText = $query

    # Execute the command and get the data
    $dataAdapter = New-Object System.Data.SQLite.SQLiteDataAdapter($command)
    $dataSet = New-Object System.Data.DataSet
    $dataAdapter.Fill($dataSet)

    # Close the connection
    $connection.Close()

    # Return the data
    return $dataSet.Tables[0]
}

# Get the data from the SQLite database
$data = Get-SQLiteData -connectionString $connectionString -query $query

# Extract the value from the dataset and store it in a variable
$value = $data.value

# Print the value
Write-Host "The value is: $value"
