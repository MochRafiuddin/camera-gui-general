import cv2

# Membuat gambar kosong berukuran 400x400 piksel dengan 3 channel warna (BGR)
image = cv2.zeros((400, 400, 3), dtype="uint8")

# Koordinat sudut kiri atas dan sudut kanan bawah dari persegi panjang
x1, y1 = 50, 50  # Sudut kiri atas
x2, y2 = 300, 300  # Sudut kanan bawah

# Warna persegi panjang (BGR) dan ketebalan garis
color = (0, 255, 0)  # Warna hijau
thickness = 2  # Ketebalan garis

# Menggambar persegi panjang pada gambar
cv2.rectangle(image, (x1, y1), (x2, y2), color, thickness)

# Menampilkan gambar
cv2.imshow("Image with Rectangle", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
