import cv2
from multiprocessing import Process, Queue
from flask import Flask, Response, render_template
from multiprocessing.managers import BaseManager
# app = Flask(__name__)

num_streams = 2  # Number of RTSP streams
rtsp_urls = [
    'rtsp://admin:Aptikma321@192.168.0.106:554/Streaming/Channels/101/',
]
frame_queues = [Queue(maxsize=10) for _ in range(num_streams)]
processed_frame_queues = [Queue(maxsize=10) for _ in range(num_streams)]  # Dictionary to store processed_frame_queues

def rtsp_stream_reader_single(rtsp_url, frame_queue, stream_id):

    cap = cv2.VideoCapture(rtsp_url)
    frame_id = 0  # Initial frame ID
    while True:
        ret, frame = cap.read()
        if not ret:
            continue
        if not frame_queue.full():
            frame_queue.put((stream_id, frame_id, frame))  # Add stream ID, frame ID and frame to the queue
            frame_id += 1  # Increment frame ID
    cap.release()

def frame_processor(frame_queue, processed_frame_queue):
    while True:
        stream_id, frame_id, frame = frame_queue.get()
        # Add AI processing code here
        processed_frame = frame  # Example: frame processed by AI
        if not processed_frame_queue.full():
            processed_frame_queue.put((stream_id, frame_id, processed_frame))  # Add stream ID, frame ID and processed frame to the queue
        
        print("ss")
        

def generate(id):
    while True:
        _, _, frame = processed_frame_queues[id].get()
        _, jpeg = cv2.imencode('.jpg', frame)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')

# @app.route('/video_feed/<int:stream_id>')
# def video_feed(stream_id):
#     return Response(generate(stream_id), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == "__main__":
    # Create and start the process for processing frames
    processes = []
    for i, rtsp_url in enumerate(rtsp_urls):
        process = Process(target=rtsp_stream_reader_single, args=(rtsp_url, frame_queues[i], i))
        process.start()
        processes.append(process)

        processor_process = Process(target=frame_processor, args=(frame_queues[i], processed_frame_queues[i]))
        processor_process.start()
        processes.append(processor_process)

    class QueueManager(BaseManager):
        pass

    QueueManager.register('get_processed_frame_queues', callable=lambda: processed_frame_queues)
    manager = QueueManager(address=('localhost', 50000), authkey=b'secret')
    server = manager.get_server()
    server.serve_forever()

    # app.run(host='0.0.0.0', port=5000)

    for process in processes:
        process.join()
