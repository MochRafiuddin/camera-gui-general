import openpyxl
from openpyxl.styles import Font, Alignment, PatternFill

# Create a new workbook and select the active worksheet
wb = openpyxl.Workbook()
ws = wb.active

# Define styles
header_font = Font(bold=True, color="FFFFFF")
header_fill = PatternFill(start_color="00C0C0C0", end_color="00C0C0C0", fill_type="solid")
data_fill = PatternFill(start_color="00E5E5E5", end_color="00E5E5E5", fill_type="solid")
center_alignment = Alignment(horizontal="center")
bold_font = Font(bold=True)

# Set column widths
ws.column_dimensions['A'].width = 5
ws.column_dimensions['B'].width = 30
ws.column_dimensions['C'].width = 15
ws.column_dimensions['D'].width = 40
ws.column_dimensions['E'].width = 12
ws.column_dimensions['F'].width = 20
ws.column_dimensions['G'].width = 12
ws.column_dimensions['H'].width = 15
ws.column_dimensions['I'].width = 15
ws.column_dimensions['J'].width = 15
ws.column_dimensions['K'].width = 15
ws.column_dimensions['L'].width = 20
ws.column_dimensions['M'].width = 20
ws.column_dimensions['N'].width = 15
ws.column_dimensions['O'].width = 15

# Add customer data
ws.merge_cells('A2:C2')
ws['A2'] = "CUSTOMER DATA"
ws['A2'].font = bold_font
ws['A3'] = "Lokasi"
ws['B3'] = "Living World Kota Wisata"

# Add system data
ws['A4'] = "System :"
ws['A4'].font = bold_font
ws['B4'] = "Brickstream 2500"

# Add the table header
headers = [
    "Number", "Site", "Device Name", "File Name", "Date", "Recording Time", 
    "Duration (Jam)", "Brickstream IN Count", "Visual IN Count", "Brickstream OUT Count", 
    "Visual OUT Count", "Difference IN Calculation", "Difference OUT Calculation", 
    "Accuracy IN", "Accuracy OUT", "Verify By"
]

# Set header styles and values
for col_num, header in enumerate(headers, start=1):
    cell = ws.cell(row=6, column=col_num, value=header)
    cell.font = header_font
    cell.fill = header_fill
    cell.alignment = center_alignment

# Sample data
data = [
    [1, "Living World Kota Wisata", "Device 1", "2024_05_06_18_00_00_LWKW_PCS", "5/6/2024", "18:00:00 - 19:00:00", "1:00:00", 38, 42, 42, 46, 4, 4, "90.48%", "91.30%", "Adam"],
    [2, "Living World Kota Wisata", "Device 2", "2024_05_06_18_00_00_LWKW_PCS", "5/6/2024", "18:00:00 - 19:00:00", "1:00:00", 119, 120, 82, 82, 1, 0, "99.17%", "100.00%", "Adam"],
    [3, "Living World Kota Wisata", "Device 3", "2024_05_06_18_00_00_LWKW_PCS", "5/6/2024", "18:00:00 - 19:00:00", "1:00:00", 51, 92, 100, 114, 41, 14, "55.43%", "87.72%", "Adam"]
]

# Add data to the worksheet
for row_num, row_data in enumerate(data, start=7):
    for col_num, cell_value in enumerate(row_data, start=1):
        cell = ws.cell(row=row_num, column=col_num, value=cell_value)
        if col_num == 1 or row_num == 1:
            cell.font = bold_font
        if row_num % 2 == 0:
            cell.fill = data_fill
        cell.alignment = center_alignment

# Save the workbook
wb.save("People_Counter_Verification.xlsx")
