from multiprocessing.managers import BaseManager
import time

# Definisikan kelas Manager yang mewarisi dari BaseManager
class TimeManager(BaseManager):
    pass

# Registrasikan queue yang ingin dibagi
TimeManager.register('get_queue')

if __name__ == '__main__':
    # Koneksi ke server manager
    manager = TimeManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    manager.connect()

    # Dapatkan queue yang dibagikan
    time_queue = manager.get_queue()

    # Loop untuk terus mengambil waktu saat ini dari queue
    try:
        while True:
            if not time_queue.empty():
                current_time = time_queue.get()
                print("Current time from server:", current_time)
            time.sleep(1)
    except KeyboardInterrupt:
        print("Client stopped.")
