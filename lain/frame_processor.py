import cv2
from multiprocessing import Queue

def frame_processor(frame_queues, processed_frame_queues):
    while True:
        for i, frame_queue in enumerate(frame_queues):
            if not frame_queue.empty():
                stream_id, frame_id, frame = frame_queue.get()
                # Add AI processing code here
                processed_frame = frame  # Example: frame processed by AI
                if not processed_frame_queues[stream_id].full():
                    processed_frame_queues[stream_id].put((stream_id, frame_id, processed_frame))  # Add stream ID, frame ID and processed frame to the queue
