from multiprocessing.managers import BaseManager
import time
from queue import Queue
import threading

# Fungsi untuk mengisi waktu saat ini ke dalam queue
def update_time(queue):
    while True:
        current_time = time.strftime("%Y-%m-%d %H:%M:%S")
        if queue.empty():
            queue.put(current_time)
        time.sleep(1)

# Definisikan kelas Manager yang mewarisi dari BaseManager
class TimeManager(BaseManager):
    pass

# Membuat objek queue
time_queue = Queue()

# Registrasikan queue yang ingin dibagi
TimeManager.register('get_queue', callable=lambda: time_queue)

if __name__ == '__main__':
    # Setup server manager
    manager = TimeManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    
    # Jalankan server manager
    server = manager.get_server()
    print("Server started at address ('127.0.0.1', 50000)")

    # Jalankan fungsi update_time dalam thread baru
    thread = threading.Thread(target=update_time, args=(time_queue,))
    thread.start()

    server.serve_forever()
    thread.join()
