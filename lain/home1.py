import sys
import os
import subprocess
from PyQt5.QtWidgets import QApplication, QMainWindow, QStackedWidget, QMessageBox
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal, QTimer
# from list_camera import MainWindow
from menu_home import App_home1
from menu_camera_list import App_home2
from menu_zone import App_home3
from menu_snapshot import App_home4
from menu_camera_list_add_edit import App_camera_list_add_edit
from menu_snapshot_edit import App_snapshot_edit
from menu_monitoring_setup import App_monitoring_setup
from menu_monitoring_setup_edit import App_monitoring_edit
from sidebar import App_sidebar

# from list_camera_monitoring import MainWindowMonitoring


class App_home(QMainWindow):
    closed = pyqtSignal()
    def __init__(self, path):
        super().__init__()

        self.setWindowTitle("Control Panel")
        self.setStyleSheet("background-color: #222222;")
        self.showFullScreen()  # Tampilkan dalam mode layar penuh        
        self.stacked_widget = QStackedWidget(self)
        self.setCentralWidget(self.stacked_widget)
        
        # self.sidebar = App_sidebar()

        # self.list_camera = MainWindow()
        # self.stacked_widget.addWidget(self.list_camera)

        self.menu_home = App_home1()
        self.stacked_widget.addWidget(self.menu_home)

        self.menu_camera_list = App_home2()
        self.stacked_widget.addWidget(self.menu_camera_list)
        self.menu_add_camera_list = App_camera_list_add_edit(0, '')
        self.stacked_widget.addWidget(self.menu_add_camera_list)

        self.menu_zone_configuration = App_home3(0, '', '')
        self.stacked_widget.addWidget(self.menu_zone_configuration)  

        self.menu_snapshot = App_home4()
        self.stacked_widget.addWidget(self.menu_snapshot)

        self.menu_edit_snapshot = App_snapshot_edit(0)
        self.stacked_widget.addWidget(self.menu_edit_snapshot)

        self.menu_monitoring_setup = App_monitoring_setup()
        self.stacked_widget.addWidget(self.menu_monitoring_setup)

        self.App_monitoring_edit = App_monitoring_edit(0,0)
        self.stacked_widget.addWidget(self.App_monitoring_edit)
        
        self.menu_record_camera = App_home4()
        self.stacked_widget.addWidget(self.menu_snapshot)

        # list Camera
        # self.list_camera.menu_control.connect(self.show_menu_home)
        
        # home
        self.menu_home.sidebar.close_window.connect(self.close_window)
        self.menu_home.sidebar.minimize_window.connect(self.minimize_window)

        self.menu_home.sidebar.monitoring_view_setup_clicked.connect(self.show_monitoring_view_setup)
        self.menu_home.sidebar.camera_preview_clicked.connect(self.show_camera_preview)
        self.menu_home.sidebar.monitoring_camera_preview_clicked.connect(self.show_monitoring_camera_preview)
        self.menu_home.sidebar.menu_camera_list_clicked.connect(self.show_menu_camera_list)
        self.menu_home.sidebar.zone_configuration_clicked.connect(self.show_menu_zone_configuration)
        self.menu_home.sidebar.snapshot_clicked.connect(self.show_menu_snapshot)
        
        # camera list
        self.menu_camera_list.sidebar.close_window.connect(self.close_window)
        self.menu_camera_list.sidebar.minimize_window.connect(self.minimize_window)

        self.menu_camera_list.sidebar.monitoring_view_setup_clicked.connect(self.show_monitoring_view_setup)
        self.menu_camera_list.sidebar.camera_preview_clicked.connect(self.show_camera_preview)
        self.menu_camera_list.sidebar.monitoring_camera_preview_clicked.connect(self.show_monitoring_camera_preview)
        self.menu_camera_list.sidebar.home_clicked.connect(self.show_menu_home)
        self.menu_camera_list.sidebar.zone_configuration_clicked.connect(self.show_menu_zone_configuration)
        self.menu_camera_list.sidebar.snapshot_clicked.connect(self.show_menu_snapshot)
        self.menu_camera_list.add_edit_camera_list_clicked.connect(self.show_menu_add_camera_list)
        self.menu_camera_list.draw_clicked.connect(self.show_menu_zone_configuration)
        self.menu_add_camera_list.back_add_camera_list_clicked.connect(self.show_menu_camera_list)
        self.menu_add_camera_list.save_add_camera_list_clicked.connect(self.show_menu_camera_list)

        # zone
        self.menu_zone_configuration.sidebar.close_window.connect(self.close_window)
        self.menu_zone_configuration.sidebar.minimize_window.connect(self.minimize_window)

        self.menu_zone_configuration.sidebar.monitoring_view_setup_clicked.connect(self.show_monitoring_view_setup)
        self.menu_zone_configuration.sidebar.monitoring_camera_preview_clicked.connect(self.show_monitoring_camera_preview)
        self.menu_zone_configuration.sidebar.camera_preview_clicked.connect(self.show_camera_preview)
        self.menu_zone_configuration.sidebar.home_clicked.connect(self.show_menu_home)
        self.menu_zone_configuration.sidebar.menu_camera_list_clicked.connect(self.show_menu_camera_list)
        self.menu_zone_configuration.sidebar.snapshot_clicked.connect(self.show_menu_snapshot)
        self.menu_zone_configuration.back_zone_configuration_clicked.connect(self.show_menu_camera_list)
        self.menu_zone_configuration.save_zone_configuration_clicked.connect(self.show_menu_camera_list)
        
        # snapshot
        self.menu_snapshot.sidebar.close_window.connect(self.close_window)
        self.menu_snapshot.sidebar.minimize_window.connect(self.minimize_window)

        self.menu_snapshot.sidebar.monitoring_view_setup_clicked.connect(self.show_monitoring_view_setup)
        self.menu_snapshot.sidebar.monitoring_camera_preview_clicked.connect(self.show_monitoring_camera_preview)
        self.menu_snapshot.sidebar.camera_preview_clicked.connect(self.show_camera_preview)
        self.menu_snapshot.sidebar.home_clicked.connect(self.show_menu_home)
        self.menu_snapshot.sidebar.menu_camera_list_clicked.connect(self.show_menu_camera_list)
        self.menu_snapshot.sidebar.zone_configuration_clicked.connect(self.show_menu_zone_configuration)
        self.menu_snapshot.edit_snapshot_clicked.connect(self.show_menu_edit_snapshot)
        self.menu_edit_snapshot.back_snapshot_clicked.connect(self.show_menu_snapshot)
        self.menu_edit_snapshot.save_snapshot_clicked.connect(self.show_menu_snapshot)
        
        # monitoring setup
        self.menu_monitoring_setup.sidebar.close_window.connect(self.close_window)
        self.menu_monitoring_setup.sidebar.minimize_window.connect(self.minimize_window)

        # self.menu_monitoring_setup.sidebar.monitoring_view_setup_clicked.connect(self.show_monitoring_view_setup)
        self.menu_monitoring_setup.sidebar.monitoring_camera_preview_clicked.connect(self.show_monitoring_camera_preview)
        self.menu_monitoring_setup.sidebar.camera_preview_clicked.connect(self.show_camera_preview)
        self.menu_monitoring_setup.sidebar.home_clicked.connect(self.show_menu_home)
        self.menu_monitoring_setup.sidebar.menu_camera_list_clicked.connect(self.show_menu_camera_list)
        self.menu_monitoring_setup.sidebar.snapshot_clicked.connect(self.show_menu_snapshot)
        self.menu_monitoring_setup.sidebar.zone_configuration_clicked.connect(self.show_menu_zone_configuration)
        self.menu_monitoring_setup.edit_monitoring_clicked.connect(self.show_menu_edit_monitoring)
        self.App_monitoring_edit.back_monitoring_clicked.connect(self.show_monitoring_view_setup)
        self.App_monitoring_edit.save_monitoring_clicked.connect(self.show_monitoring_view_setup)


        self.path = path
        self.process = subprocess.Popen(["python", path])
        # self.process.wait()
        # self.close_current_window()
    
    def close_window(self):
        msg = QMessageBox()
        msg.setWindowTitle("Confirm close Window")
        msg.setText("Are you sure you want to close Window?")
        msg.setIcon(QMessageBox.Warning)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setDefaultButton(QMessageBox.Cancel)

        # Menampilkan popup dan mendapatkan respons pengguna
        response = msg.exec_()

        # Jika pengguna menekan tombol Ok, maka lakukan aksi penghapusan data
        if response == QMessageBox.Ok:
            self.close()

    def minimize_window(self):
        self.showMinimized()

    def closeEvent(self, event):
        self.process.kill()
        self.closed.emit()
        event.accept()

    def show_menu_home(self):        
        self.stacked_widget.setCurrentWidget(self.menu_home)

    def show_menu_camera_list(self):        
        self.menu_camera_list.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_camera_list)

    def show_menu_add_camera_list(self, id_data, type_cam):        
        self.menu_add_camera_list.set_parameter(id_data, type_cam)        
        self.stacked_widget.setCurrentWidget(self.menu_add_camera_list)

    def show_menu_zone_configuration(self, id_data, type_cam, type_draw):
        self.menu_zone_configuration.update_content(id_data, type_cam, type_draw)
        self.stacked_widget.setCurrentWidget(self.menu_zone_configuration)
    
    def show_menu_snapshot(self):
        self.menu_snapshot.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_snapshot)

    def show_menu_edit_snapshot(self, id_data):
        self.menu_edit_snapshot.set_parameter(id_data)
        self.stacked_widget.setCurrentWidget(self.menu_edit_snapshot)
    
    def show_camera_preview(self):
        # print("Close window function called")  # Pemeriksaan apakah fungsi ini dipanggil
        # self.close()  # Tutup jendela 
        current_widget = self.stacked_widget.currentWidget()
        if hasattr(current_widget, 'sidebar') and hasattr(current_widget.sidebar, 'show_loading'):
            current_widget.sidebar.show_loading()
        else:
            print("Current widget does not have sidebar or show_loading method.")
        
        QTimer.singleShot(1, self.open_camera_preview)

    def open_camera_preview(self):
        current_directory = os.path.dirname(os.path.abspath(__file__))            
        panel_path = os.path.join(current_directory, "list_camera1.pyc")
        # subprocess.Popen(["python", panel_path])
        from list_camera1 import MainWindow
        self.list_camera = MainWindow(panel_path)
        self.list_camera.show()
        self.list_camera.closed.connect(self.close)
        self.close()
    
    def show_monitoring_camera_preview(self):
        current_widget = self.stacked_widget.currentWidget()
        # if hasattr(current_widget, 'sidebar') and hasattr(current_widget.sidebar, 'show_loading'):
        if hasattr(current_widget, 'sidebar') and hasattr(current_widget.sidebar, 'show_loading'):
            current_widget.sidebar.show_loading()        
            # if current_widget.sidebar.loading_text.isVisible():
            #     print("self.loading_text is visible")
            # else:
            #     print("self.loading_text is not visible")
        else:
            print("Current widget does not have sidebar or show_loading method.")
        
        QTimer.singleShot(1, self.open_monitoring)        
        # self.menu_home.sidebar.show_loading()
        # self.MainWindowMonitoring.update_layout()                
        # self.stacked_widget.setCurrentWidget(self.MainWindowMonitoring)

    def open_monitoring(self):
        current_directory = os.path.dirname(os.path.abspath(__file__))            
        panel_path = os.path.join(current_directory, "list_camera_monitoring1.pyc")
        # subprocess.Popen(["python", panel_path])
        from list_camera_monitoring1 import MainWindowMonitoring
        self.list_camera_monitoring = MainWindowMonitoring(panel_path)
        self.list_camera_monitoring.show()
        self.list_camera_monitoring.closed.connect(self.close)
        self.close()
        
    
    def show_monitoring_view_setup(self):      
        self.menu_monitoring_setup.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_monitoring_setup)

    def show_menu_edit_monitoring(self, id_data, type_cam):
        self.App_monitoring_edit.set_parameter(id_data, type_cam)
        self.stacked_widget.setCurrentWidget(self.App_monitoring_edit)

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_home('')
#     window.show()
#     sys.exit(app.exec_())
