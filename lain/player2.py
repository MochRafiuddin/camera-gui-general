import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QFrame, QVBoxLayout, QHBoxLayout, QPushButton, QSlider
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtCore import Qt

class CameraLayout(QMainWindow):
    def __init__(self):
        super().__init__()
        self.update_layout()
        self.setWindowTitle("Control Panel")
        self.setStyleSheet("background-color: #222222;")
        self.showFullScreen()

    def minimizeWindow(self, event):
        # Minimize the window
        self.minimize_window.emit()

    def on_close_button_clicked(self, event):
        print("Close button clicked")  # Pastikan bahwa ini tercetak
        self.close_window.emit()
        
    def update_layout(self): 
        screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = screen_geometry.width() / 1280
        self.height_ratio = screen_geometry.height() / 832       
        # top bar
        top_bar = QLabel(self)
        top_bar.setStyleSheet("background-color: #000000;")
        top_bar.setGeometry(0, 0, screen_geometry.width(), int(63 * self.height_ratio))

        self.icon1_label = QLabel(self)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.setGeometry(
            int(1206 * self.width_ratio),
            int(17 * self.height_ratio),
            int(24 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        ) 
        # self.icon1_label.setScaledContents(True)
    
        self.icon1_label.mousePressEvent = self.minimizeWindow

        self.icon2_label = QLabel(self)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon2_label.setGeometry(
            int(1232 * self.width_ratio),
            int(15 * self.height_ratio),
            int(33 * self.width_ratio),
            int(33 * self.height_ratio),
        )
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        ) 
        self.icon3_label = QLabel(self)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon3_label.setGeometry(
            int(1252 * self.width_ratio),
            int(9 * self.height_ratio),
            int(45 * self.width_ratio),
            int(45 * self.height_ratio),
        )
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  
        self.icon3_label.mousePressEvent = self.on_close_button_clicked
        ###############################################################

        control_panel_label = QLabel("Control Panel", self)
        control_panel_label.setStyleSheet("color: white; background-color: #000000;")
        control_panel_label.setFont(
            QFont("Times", int(23 * min(self.width_ratio, self.height_ratio)))
        )
        control_panel_label.setGeometry(
            int(30 * self.width_ratio),
            int(10 * self.height_ratio),
            int(190 * self.width_ratio),
            int(45 * self.height_ratio),
        )

        display_vidio = QLabel(self)
        display_vidio.setStyleSheet("color: white; background-color: #3d3d3d")
        display_vidio.setGeometry(
            int(20 * self.width_ratio),
            int(90 * self.height_ratio),
            int(970 * self.width_ratio),
            int(680 * self.height_ratio),
        )

        label_in = QLabel("Real Count In", self)
        label_in.setStyleSheet("color: white;")
        label_in.setFont(QFont("Times", 18))
        label_in.setAlignment(Qt.AlignCenter)
        label_in.setGeometry(
            int(1080 * self.width_ratio),
            int(100 * self.height_ratio),
            int(110 * self.width_ratio),
            int(35 * self.height_ratio),
        )
        value_in = QLabel("0", self)
        value_in.setStyleSheet("color: white;")
        value_in.setFont(QFont("Times", 18))
        value_in.setAlignment(Qt.AlignCenter)
        value_in.setGeometry(
            int(1080 * self.width_ratio),
            int(125 * self.height_ratio),
            int(110 * self.width_ratio),
            int(35 * self.height_ratio),
        )
        
        label_out = QLabel("Real Count Out", self)
        label_out.setStyleSheet("color: white;")
        label_out.setFont(QFont("Times", 18))
        label_out.setAlignment(Qt.AlignCenter)
        label_out.setGeometry(
            int(1080 * self.width_ratio),
            int(150 * self.height_ratio),
            int(110 * self.width_ratio),
            int(35 * self.height_ratio),
        )

        value_out = QLabel("0", self)
        value_out.setStyleSheet("color: white;")
        value_out.setFont(QFont("Times", 18))
        value_out.setAlignment(Qt.AlignCenter)
        value_out.setGeometry(
            int(1080 * self.width_ratio),
            int(175 * self.height_ratio),
            int(110 * self.width_ratio),
            int(35 * self.height_ratio),
        )

        prev_button = QPushButton("◀", self)
        prev_button.setStyleSheet("color: white; background-color: #3d3d3d")        
        prev_button.setFont(QFont("Times", 18))
        prev_button.setGeometry(
            int(1015 * self.width_ratio),
            int(250 * self.height_ratio),
            int(75 * self.width_ratio),
            int(30 * self.height_ratio),
        )
        self.play_button = QPushButton("▶", self)
        self.play_button.setStyleSheet("color: white; background-color: #3d3d3d")        
        self.play_button.setFont(QFont("Times", 18))
        self.play_button.setGeometry(
            int(1100 * self.width_ratio),
            int(250 * self.height_ratio),
            int(75 * self.width_ratio),
            int(30 * self.height_ratio),
        )
        self.play_button.clicked.connect(self.toggle_play_stop)

        next_button = QPushButton("▶▶", self)
        next_button.setStyleSheet("color: white; background-color: #3d3d3d")        
        next_button.setFont(QFont("Times", 18))
        next_button.setGeometry(
            int(1185 * self.width_ratio),
            int(250 * self.height_ratio),
            int(75 * self.width_ratio),
            int(30 * self.height_ratio),
        )

        # Bottom frame for playback bar
        bottom_frame = QFrame(self)
        bottom_frame.setGeometry(0, int(770 * self.height_ratio), screen_geometry.width(), int(50 * self.height_ratio))
        # bottom_frame.setStyleSheet("border: 2px solid white;")

        playback_bar_layout = QVBoxLayout(bottom_frame)
        playback_bar = QSlider(Qt.Horizontal)
        playback_bar.setStyleSheet("height: 50px;")
        playback_bar_layout.addWidget(playback_bar)

    def increment_real_count_in(self, event):
        if event.button() == Qt.RightButton:
            current_value = int(self.real_count_in_value.text())
            self.real_count_in_value.setText(str(current_value + 1))

    def increment_real_count_out(self, event):
        if event.button() == Qt.LeftButton:
            current_value = int(self.real_count_out_value.text())
            self.real_count_out_value.setText(str(current_value + 1))

    def toggle_play_stop(self):
        if self.play_button.text() == '▶':
            self.play_button.setText('■')
        else:
            self.play_button.setText('▶')

    def minimize_window(self, event):
        self.showMinimized()

    def on_close_button_clicked(self, event):
        self.close()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = CameraLayout()
    window.show()
    sys.exit(app.exec_())
