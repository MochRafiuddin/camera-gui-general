import sys
import os
import subprocess
import cv2
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QGridLayout, QWidget, QVBoxLayout, QHBoxLayout
from PyQt5.QtGui import QPixmap, QFont, QImage
from PyQt5.QtCore import Qt, QProcess, QTimer, pyqtSlot, pyqtSignal, QEvent
import sqlite3
import time
import torch
import torchvision.models as models # ResNet-18 PyTorch Model.
from torch import nn # Neural Network Layers
from keluar_masuk import VideoCamera
from gender1 import face_gender
from area import CountObject
from modal_snapshot import PopupForm
import numpy as np
from home import App_home

class MainWindow(QMainWindow):
    closed = pyqtSignal()
    # menu_control = pyqtSignal()
    def __init__(self, path):
        super().__init__()
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model = torch.hub.load('yolov5', 'custom', 'models/crowdhuman_yolov5m.pt', source='local', force_reload=True).to(self.device).eval()

        self.FaceDetector = torch.hub.load('yolov5', 'custom', 'models/YOLO/Best.onnx', source='local', _verbose=False, force_reload=True)
        self.FaceDetector.eval().to(self.device)

        Classes = 9
        Groups = ['00-10', '11-20', '21-30', 
                '31-40', '41-50', '51-60', 
                '61-70', '71-80', '81-90']

        ClassificationModel2 = 'models/ResNet-18/ResNet-18 Age 0.60 + Gender 93.pt'
        self.FaceClassifier = models.resnet18(pretrained=True)
        self.FaceClassifier.fc = nn.Linear(512, Classes+2)
        self.FaceClassifier = nn.Sequential(self.FaceClassifier, nn.Sigmoid())
        self.FaceClassifier.load_state_dict(torch.load(ClassificationModel2))
        self.FaceClassifier.eval()
        self.FaceClassifier.to(self.device)
        self.setStyleSheet("background-color: #222222;")
        self.update_layout()  # Panggil update_layout untuk pengaturan awal
        self.showFullScreen()  # Tampilkan dalam mode layar penuh

        self.path = path
        self.process = subprocess.Popen(["python", path])
        # self.process.wait()
        # self.close_current_window()
    

    def closeEvent(self, event):        
        self.process.kill()
        self.closed.emit()
        event.accept()

    def update_layout(self):
        self.screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = self.screen_geometry.width() / 1280
        self.height_ratio = self.screen_geometry.height() / 832
        
        # Membuat widget-grid utama sebagai central widget
        central_widget = QWidget()
        self.setCentralWidget(central_widget)
        self.grid_layout = QGridLayout(central_widget)
        self.grid_layout.setSpacing(10)  # Menambahkan spasi antar widget

        # Set start height pada baris pertama
        height_awal = 150 * self.height_ratio
        self.grid_layout.setRowMinimumHeight(0, int(height_awal))

        # List nama gambar dan event handler
        cam_array = [4, 9, 16, 25, 36, 49, 64, 81, 100]
        cam_dimension = [[2, 2], [3, 3], [4, 4], [5, 5], [6, 6], [7, 7], [8, 8], [9, 9], [10, 10]]
        camera_area = self.get_data_camera('setting_camera_area')
        camera_gender = self.get_data_camera('setting_camera_gender')
        camera_in_out = self.get_data_camera('setting_camera')
        site_code = self.get_cosntant_by_code('site_code')

        camera1 = []
        camera2 = self.get_camera_info(camera1, camera_in_out, 0, 1, 9, 2, site_code[2], 18)
        camera3 = self.get_camera_info(camera2, camera_gender, 0, 1, 3, 2, site_code[2], 17)
        self.cameras = self.get_camera_info(camera3, camera_area, 0, 1, 3, 2, site_code[2], 10)
        self.image_labels = []

        # total_cam = len(image_names)  # Assign your total_cam value
        total_cam = len(self.cameras) # Assign your total_cam value
        nearest = self.find_nearest_value(cam_array, total_cam)
        index = cam_array.index(nearest)
        print(f"Nearest: {nearest}")
        dimension = cam_dimension[index]

        baris = dimension[0]
        kolom = dimension[1]
        not_found = nearest - total_cam
        print(f"Total baris: {baris}")
        print(f"Expected Column: {kolom}\n\n\n")
        i=0
        self.captures = []
        for url in self.cameras:
            capture = cv2.VideoCapture(url[2])
            self.captures.append(capture)
        self.model_detaksi = []

        # Membuat QTimer untuk mengambil frame secara periodik
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.display_frames)
        self.timer.start(25)  # Set interval waktu dalam milidetik (misalnya: 100ms)

        for row in range(baris):
            for col in range(kolom):
                if total_cam > 0:
                    # Tambahkan label gambar
                    image_label = QLabel(self)
                    image_label.setStyleSheet("border: none;")
                    image_label.setScaledContents(True)

                    # Menghitung ukuran label sesuai dengan rasio layar
                    label_height = self.screen_geometry.height() // baris - 105
                    label_width = self.screen_geometry.width() // kolom - 10
                    image_label.setFixedSize(label_width, label_height)

                    # Buat layout vertikal untuk image_label
                    label_layout = QVBoxLayout(image_label)

                    # Buat layout horizontal untuk tombol
                    button_layout = QHBoxLayout()

                    # Tambahkan tombol snapshot di pojok kanan atas
                    snapshot_button = QPushButton("Snapshot", self)
                    snapshot_button.setStyleSheet("background-color: #1e9fff; color: white;")
                    # snapshot_button.clicked.connect(lambda state, url=cameras[i][2]: self.take_snapshot(url))
                    snapshot_button.clicked.connect(lambda state, no_cam=i, capture=self.captures[i], id_cam=self.cameras[i][5], type_camera=self.cameras[i][4]: self.take_snapshot(no_cam, capture, id_cam, type_camera))
                    snapshot_button.setFixedSize(80, 30)  # Ukuran tombol

                    # Tambahkan tombol snapshot ke dalam layout horizontal
                    button_layout.addWidget(snapshot_button)

                    # Tambahkan layout horizontal ke dalam layout vertikal
                    label_layout.addLayout(button_layout)
                    label_layout.setAlignment(Qt.AlignTop | Qt.AlignRight)  # Atur alignment untuk layout vertikal

                    # Menambahkan label ke grid layout dengan indeks baris dan kolom
                    self.grid_layout.addWidget(image_label, row + 1, col)

                    # Menambahkan label ke list
                    self.image_labels.append(image_label)                    
                    # print(self.cameras[row][4])
                    # if self.cameras[i][4] == 1 or self.cameras[i][4] == 4:
                    #     load_detaksi = VideoCamera(self.model, self.device)
                    # elif self.cameras[i][4] == 2:
                    #     load_detaksi = face_gender(self.device, self.FaceDetector, self.FaceClassifier)
                    # elif self.cameras[i][4] == 3:                        
                    #     load_detaksi = CountObject(self.model, self.cameras[i][5])

                    # self.model_detaksi.append(load_detaksi)
                    i += 1
                    total_cam -= 1
                elif not_found > 0:
                    empty_label = QLabel("Not Found", self)
                    empty_label.setStyleSheet("border: 1px solid #1e9fff;color: white;")
                    empty_label.setAlignment(Qt.AlignCenter)
                    font_size = min(empty_label.width() // 1, empty_label.height() // 1)
                    font = QFont()
                    font.setPointSize(font_size)
                    empty_label.setFont(font)
                    self.grid_layout.addWidget(empty_label, row + 1, col)
                    not_found -= 1
        #######################################################
        self.top_navbar()

    def display_frames(self):
        for i, capture in enumerate(self.captures):
            ret, frame = capture.read()
            if ret:
                # if self.cameras[i][4] == 1 or self.cameras[i][4] == 4:                    
                #     frame = self.model_detaksi[i].get_frame(frame, self.cameras[i][5], 1)
                # elif self.cameras[i][4] == 2:
                #     frame = self.model_detaksi[i].detect_faces(frame, self.cameras[i][5], 1)
                # elif self.cameras[i][4] == 3:
                #     frame = self.model_detaksi[i].process_video(frame, self.cameras[i][5], 1)

                # Ubah frame OpenCV menjadi format yang dapat ditampilkan di QLabel
                frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = frame_rgb.shape
                bytes_per_line = ch * w
                q_image = QImage(frame_rgb.data, w, h, bytes_per_line, QImage.Format_RGB888)

                # Tampilkan frame di dalam label yang sesuai
                if i < len(self.image_labels):
                    pixmap = QPixmap.fromImage(q_image)
                    self.image_labels[i].setPixmap(pixmap)

    def take_snapshot(self,no_cam,  capture, id_cam, type_camera):
        # Ambil frame yang ditampilkan dari label
        pixmap = self.image_labels[no_cam].pixmap()

        # Konversi QPixmap menjadi QImage
        q_image = pixmap.toImage()

        # Ubah QImage menjadi array numpy
        buffer = q_image.bits().asstring(q_image.byteCount())
        frame = np.frombuffer(buffer, dtype=np.uint8).reshape(q_image.height(), q_image.width(), 4)

        # Konversi frame menjadi format BGR (OpenCV)
        # frame_bgr = cv2.cvtColor(frame, cv2.COLOR_RGBA2BGR)
        folder_path = ''
        if type_camera == 1:
            folder_path = f'img/people_counting'
            typecam = 1
        elif type_camera == 2:
            folder_path = f'img/gender'
            typecam = 2
        elif type_camera == 3:
            folder_path = f'img/area'
            typecam = 3
        elif type_camera == 4:
            folder_path = f'img/kiri_kanan'
            typecam = 4

        waktu = time.time()                         
        filename = f"image_{typecam}_{time.strftime('%Y%m%d%H%M%S', time.localtime(waktu))}.jpg"
        
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        
        cv2.imwrite(f"{folder_path}/{filename}", frame)

        popup = PopupForm(filename, typecam)
        popup.exec_()

        print(f"Snapshot saved as {folder_path}/{filename}")

    def top_navbar(self):
        top_bar = QLabel(self)
        top_bar.setStyleSheet("background-color: #000000;")
        top_bar.setGeometry(0, 0, self.screen_geometry.width(), int(63 * self.height_ratio))

        self.icon1_label = QLabel(self)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.setGeometry(
            int(1206 * self.width_ratio),
            int(17 * self.height_ratio),
            int(24 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        ) 
        # self.icon1_label.setScaledContents(True)
    
        self.icon1_label.mousePressEvent = self.minimizeWindow

        self.icon2_label = QLabel(self)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon2_label.setGeometry(
            int(1232 * self.width_ratio),
            int(15 * self.height_ratio),
            int(33 * self.width_ratio),
            int(33 * self.height_ratio),
        )
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        ) 
    
        self.icon3_label = QLabel(self)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon3_label.setGeometry(
            int(1252 * self.width_ratio),
            int(9 * self.height_ratio),
            int(45 * self.width_ratio),
            int(45 * self.height_ratio),
        )
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  

        self.icon3_label.mousePressEvent = self.openKeluarWindow

        control_panel_label = QLabel("All Camera Preview", self)
        control_panel_label.setStyleSheet("color: white; background-color: #000000;")
        control_panel_label.setFont(
            QFont("Times", int(23 * min(self.width_ratio, self.height_ratio)))
        )
        control_panel_label.setGeometry(
            int(30 * self.width_ratio),
            int(10 * self.height_ratio),
            int(350 * self.width_ratio),
            int(45 * self.height_ratio),
        ) 
        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(30 * self.width_ratio),
            int(90 * self.height_ratio),
            int(470 * self.width_ratio),
            int(44 * self.height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: #222222; border: 3px solid white; color: white; font-family: Inter; font-size: 18px; text-align: left; border-radius: 10px; font-weight: 400"
        )
        self.GButton_12.setText("  you can manage all previewed camera via control panel")

        self.GButton_21 = QPushButton(self)
        self.GButton_21.setGeometry(
            int(1070 * self.width_ratio),
            int(80 * self.height_ratio),
            int(191 * self.width_ratio),
            int(51 * self.height_ratio),
        )
        self.GButton_21.setStyleSheet(
            "background-color: #1e9fff; color: white; font-family: Times; font-size: 16px; border-radius: 10px;"
        )
        self.GButton_21.setText("Back to Control Panel")
        self.GButton_21.clicked.connect(self.GButton_21_command)

    def closeEvent(self, event):
        # Hentikan semua tangkapan saat jendela ditutup
        for capture in self.captures:
            capture.release()
        self.timer.stop()
        event.accept()

    def find_nearest_value(self ,array, target):
        array.sort()
        nearest_value = array[0]
        min_difference = abs(target - array[0])

        for i in range(1, len(array)):
            difference = abs(target - array[i])
            before_diff = array[i - 1]

            if target > before_diff:
                nearest_value = array[i]
                min_difference = difference

        return nearest_value
    
    def change_background_color_enter(self, event, idx):
        # Setel gaya pada gambar tertentu sesuai dengan indeksnya
        for i, image_label in enumerate(self.image_labels):
            if i == idx:
                image_label.setStyleSheet("border: 1px solid #1e9fff;")
            else:
                image_label.setStyleSheet("border: none;")

    def change_background_color_leave(self, event, idx):
        # Setel gaya pada gambar tertentu sesuai dengan indeksnya
        for i, image_label in enumerate(self.image_labels):
            if i == idx:
                image_label.setStyleSheet("border: none;")
            else:
                image_label.setStyleSheet("border: none;")

    def GButton_21_command(self):
        for capture in self.captures:
            capture.release()
        self.timer.stop()
        current_directory = os.path.dirname(os.path.abspath(__file__))            
        panel_path = os.path.join(current_directory, "home.pyc")            
        # subprocess.Popen(["python", panel_path])
        self.home = App_home(panel_path)
        self.home.show()
        self.home.closed.connect(self.close)
        self.close()
        # event.accept()
        # self.menu_control.emit()

    def minimizeWindow(self, event):
        self.showMinimized()

    def openKeluarWindow(self, event):
        self.close()
        # current_directory = os.path.dirname(os.path.abspath(__file__))
        # panel_path = os.path.join(current_directory, "keluar.py")
        # subprocess.Popen(["python", panel_path])

    def get_camera_info(self, data_camera, camera_source, index_id, index_nama, index_code, index_rtsp, site_code, index_type_cam):
        nomor = 1
        # books = [('camera in out 1','A01','rtsp://camera:12345678@192.168.100.140:554/stream1','DKY005',"People-Counting",'1')]
        for cam in camera_source:
            nama_cam = cam[index_nama]
            code_cam = cam[index_code]
            rtsp_cam = cam[index_rtsp]
            id_cam = cam[index_id]
            type_cam = cam[index_type_cam]
            camera_info = (
                nama_cam,
                code_cam,
                rtsp_cam,
                site_code,
                type_cam,
                id_cam,
            )
            nomor +=1
            # print(camera_info)
            data_camera.append(camera_info)
        return data_camera
    
    def get_data_camera(self, table):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM {table} where deleted=1').fetchall()
        return data

    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = MainWindow('')
#     window.show()
#     sys.exit(app.exec_())
