from multiprocessing import Queue
from multiprocessing.managers import BaseManager
import cv2
import threading

num_streams = 2
rtsp_urls = [
    'rtsp://admin:Aptikma321@192.168.0.106:554/Streaming/Channels/101/',
    'rtsp://admin:Aptikma321@192.168.0.108:554/Streaming/Channels/101/',
]
frame_queues = [Queue(maxsize=10) for _ in range(len(rtsp_urls))]
processed_frame_queues = [(i, Queue(maxsize=10)) for i in range(num_streams)]

def rtsp_stream_reader_single(rtsp_url, frame_queue):
    cap = cv2.VideoCapture(rtsp_url)
    while True:
        ret, frame = cap.read()
        if not ret:
            continue
        for i, queue in enumerate(frame_queues):
            if queue is frame_queue:
                frame_queue.put((frame, i))  # Adding id_stream
                break  
    cap.release()

def frame_processor(frame_queue, processed_frame_queue):
    while True:
        frame, id_stream = frame_queue.get()  # Retrieving id_stream
        # Add AI processing code here
        processed_frame = frame  
        if not processed_frame_queue.full():
            processed_frame_queue.put((processed_frame, id_stream))  # Adding id_stream  

class QueueManager(BaseManager):
    pass

QueueManager.register('get_processed_frame_queue', callable=lambda i: processed_frame_queues[i][1])

if __name__ == "__main__":
    threads = []
    for i, rtsp_url in enumerate(rtsp_urls):
        stream_thread = threading.Thread(target=rtsp_stream_reader_single, args=(rtsp_urls[i], frame_queues[i]))
        stream_thread.start()
        threads.append(stream_thread)

        processor_thread = threading.Thread(target=frame_processor, args=(frame_queues[i], processed_frame_queues[i][1]))  # Passing the queue, not the tuple
        processor_thread.start()
        threads.append(processor_thread)

    manager = QueueManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    server = manager.get_server()
    print("Server started at address ('127.0.0.1', 50000)")
    
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()
    threads.append(server_thread)
    
    for thread in threads:
        thread.join()
