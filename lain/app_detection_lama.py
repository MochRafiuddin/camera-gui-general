from multiprocessing import Queue
from multiprocessing.managers import BaseManager
import cv2
import threading
import sqlite3
from keluar_masuk import VideoCamera
from area import CountObject
from gender1 import face_gender
import datetime
import os
import sys

# Connect to SQLite database and fetch settings
conn = sqlite3.connect('setting.db')
cursor = conn.cursor()
query = """
    SELECT id, path, type_camera FROM setting_camera WHERE deleted = 1
    UNION ALL    
    SELECT id, path, type_camera FROM setting_camera_area WHERE deleted = 1
    UNION ALL    
    SELECT id, path, type_camera FROM setting_camera_gender WHERE deleted = 1;
"""
cursor.execute(query)
results = cursor.fetchall()
conn.close()

frame_queues = [Queue(maxsize=10) for _ in range(len(results))]
processed_frame_queues = [(row[0], row[2], Queue(maxsize=10)) for i, row in enumerate(results)]  # id, type_camera, Queue

def initialize_log_txt():
    log_file = 'detection.txt'
    now = datetime.datetime.today()
    formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")
    if os.path.exists(log_file):
        with open(log_file, 'a') as f:
            f.write(f'# Start Detection - {formatted_now}\n')
    else:
        with open(log_file, 'w') as f:
            f.write(f'# Start Detection - {formatted_now}\n')

def update_log_txt(new_segment):
    log_file = 'detection.txt'
    now = datetime.datetime.today()
    formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")

    with open(log_file, 'a') as f:        
        f.write(f'{formatted_now} -- {new_segment}\n')

def get_setting_camera_by_id(table, id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM {table} where deleted=1 and id={id_cam}').fetchone()
    return data

def rtsp_stream_reader_single(rtsp_url, frame_queue, id_cam):
    cap = cv2.VideoCapture(rtsp_url)
    while True:
        ret, frame = cap.read()
        if not ret:
            update_status_camera(id_cam, 2, 0)
            continue
        frame_queue.put(frame)  # Just putting the frame
    cap.release()

def frame_processor_inout(frame_queue, processed_frame_queue, id_camera):
    camera_keluar_masuk = VideoCamera()
    while True:
        frame = frame_queue.get()  # Retrieving the frame
        frame_ai = camera_keluar_masuk.get_frame(frame, id_camera)
        # Add AI processing code here
        processed_frame = frame_ai  
        if not processed_frame_queue.full():
            processed_frame_queue.put(processed_frame)  # Just putting the processed frame

def frame_processor_area(frame_queue, processed_frame_queue, id_camera):
    camera_area = CountObject(id_camera)
    while True:
        frame = frame_queue.get()  # Retrieving the frame
        frame_ai = camera_area.process_video(frame)
        # Add AI processing code here
        processed_frame = frame_ai  
        if not processed_frame_queue.full():
            processed_frame_queue.put(processed_frame)  # Just putting the processed frame

def frame_processor_gender(frame_queue, processed_frame_queue, id_camera):
    camera_gender = face_gender()
    while True:
        frame = frame_queue.get()  # Retrieving the frame
        frame_ai = camera_gender.detect_faces(frame, id_camera)
        # Add AI processing code here
        processed_frame = frame_ai  
        if not processed_frame_queue.full():
            processed_frame_queue.put(processed_frame)  # Just putting the processed frame

def reset_array_camera_in_out(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    update_query = "UPDATE setting_camera SET people_kanan = ?, people_kiri = ?, people_masuk = ? where id = ?"
    cursor.execute(update_query, ('[]', '[]', '[]', id_cam))
    conn.commit()
    cursor.close()
    conn.close()
    # print(f"array camera in_out id: {id_cam}, reset successfully")
    update_log_txt(f"array camera in_out id: {id_cam}, reset successfully")

def reset_array_camera_gender(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()    
   
    update_query = "UPDATE setting_camera_gender SET array_kanan = ?, array_kiri = ? where id = ?"
    cursor.execute(update_query,('[]', '[]', id_cam))
    conn.commit()
    
    cursor.close()
    conn.close()
    # print(f"array camera gender id: {id_cam},reset successfully")
    update_log_txt(f"array camera gender id: {id_cam},reset successfully")

def update_status_camera(id, type_camera, status_camera):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    if type_camera == 1:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    elif type_camera == 2:        
        update_query = "UPDATE setting_camera_gender SET status_camera = ? where id = ?"
    elif type_camera == 3:
        update_query = "UPDATE setting_camera_area SET status_camera = ? where id = ?"
    elif type_camera == 4:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    cursor.execute(update_query,(status_camera,id))
    conn.commit()

    cursor.close()
    conn.close()


def restart_file():
    python = sys.executable
    os.execl(python, python, *sys.argv)

class QueueManager(BaseManager):
    pass

def get_processed_frame_queue(stream_id, type_cam):
    idx = 0
    for index, (id_cam, t_cam, queue) in enumerate(processed_frame_queues):
        if id_cam == stream_id and t_cam == type_cam:
            return queue
    return None

QueueManager.register('get_processed_frame_queue', callable=lambda id_cam, type_cam: get_processed_frame_queue(id_cam, type_cam))

if __name__ == "__main__":
    initialize_log_txt()
    threads = []
    for i, (id, path, type_camera) in enumerate(results):
        print(id, type_camera, path)
        stream_thread = threading.Thread(target=rtsp_stream_reader_single, args=(path, frame_queues[i], id))
        stream_thread.start()
        threads.append(stream_thread)

        if type_camera == 1 or type_camera == 4:
            reset_array_camera_in_out(id)
            processor_thread = threading.Thread(target=frame_processor_inout, args=(frame_queues[i], processed_frame_queues[i][2], id))  # Correct indexing
            processor_thread.start()
            threads.append(processor_thread)
        elif type_camera == 2:
            processor_thread = threading.Thread(target=frame_processor_gender, args=(frame_queues[i], processed_frame_queues[i][2], id))  # Correct indexing
            processor_thread.start()
            threads.append(processor_thread)
        elif type_camera == 3:
            processor_thread = threading.Thread(target=frame_processor_area, args=(frame_queues[i], processed_frame_queues[i][2], id))  # Correct indexing
            processor_thread.start()
            threads.append(processor_thread)

    manager = QueueManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    server = manager.get_server()
    print("Server started at address ('127.0.0.1', 50000)")

    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()
    threads.append(server_thread)
    
    for thread in threads:
        thread.join()
