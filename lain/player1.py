import tkinter as tk
from tkinter import ttk

def increment_real_count_in(event):
    current_value = int(real_count_in_value.cget("text"))
    real_count_in_value.config(text=str(current_value + 1))

def increment_real_count_out(event):
    current_value = int(real_count_out_value.cget("text"))
    real_count_out_value.config(text=str(current_value + 1))

def toggle_play_stop():
    if play_button.config('text')[-1] == '▶':
        play_button.config(text='■')
    else:
        play_button.config(text='▶')

def main():
    root = tk.Tk()
    root.title("Camera Layout")
    root.state('zoomed')  # Make the window full-size

    # Top frame for IP address and camera type
    top_frame = ttk.Frame(root)
    top_frame.pack(side=tk.TOP, fill=tk.X)

    ip_label = ttk.Label(top_frame, text="IP Address")
    ip_label.pack(side=tk.LEFT, padx=10, pady=10)

    camera_type_label = ttk.Label(top_frame, text="Tipe Kamera")
    camera_type_label.pack(side=tk.RIGHT, padx=10, pady=10)

    # Middle frame for video preview and real count
    middle_frame = ttk.Frame(root)
    middle_frame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    # Frame for video preview (white area)
    video_preview_frame = ttk.Frame(middle_frame, relief=tk.SUNKEN)
    video_preview_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True, padx=10, pady=10)

    # Canvas for video preview
    video_preview = tk.Canvas(video_preview_frame, bg="white")
    video_preview.pack(fill=tk.BOTH, expand=True)

    # Frame for real count and playback controls
    controls_frame = ttk.Frame(middle_frame)
    controls_frame.pack(side=tk.RIGHT, fill=tk.Y, padx=10, pady=10)

    real_count_in_label = ttk.Label(controls_frame, text="Real Count - In")
    real_count_in_label.pack(pady=(20, 0))

    global real_count_in_value
    real_count_in_value = ttk.Label(controls_frame, text="0")
    real_count_in_value.pack()

    real_count_out_label = ttk.Label(controls_frame, text="Real Count - Out")
    real_count_out_label.pack(pady=(20, 0))

    global real_count_out_value
    real_count_out_value = ttk.Label(controls_frame, text="0")
    real_count_out_value.pack()

    prev_button = ttk.Button(controls_frame, text="◀")
    prev_button.pack(side=tk.LEFT, padx=5, pady=20)

    global play_button
    play_button = ttk.Button(controls_frame, text="▶", command=toggle_play_stop)
    play_button.pack(side=tk.LEFT, padx=5, pady=20)

    next_button = ttk.Button(controls_frame, text="▶▶")
    next_button.pack(side=tk.LEFT, padx=5, pady=20)

    # Bottom frame for playback bar
    bottom_frame = ttk.Frame(root)
    bottom_frame.pack(side=tk.BOTTOM, fill=tk.X, padx=10, pady=10)

    playback_bar = ttk.Scale(bottom_frame, orient='horizontal')
    playback_bar.pack(fill=tk.X)

    # Bind right and left click events
    real_count_in_value.bind("<Button-3>", increment_real_count_in)
    real_count_out_value.bind("<Button-1>", increment_real_count_out)

    root.mainloop()

if __name__ == "__main__":
    main()
