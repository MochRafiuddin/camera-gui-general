from xml.etree.ElementTree import Element, SubElement, tostring
import xml.etree.ElementTree as ET
from flask_cors import CORS
from keluar_masuk import VideoCamera
from area import CountObject
from gender1 import face_gender
import torch
import sqlite3
import time
import threading
import cv2
import json
import requests
import time
import torchvision.models as models # ResNet-18 PyTorch Model.
from torch import nn # Neural Network Layers
import concurrent.futures
import datetime
import sys
import traceback
import os

# os.environ["CUDA_LAUNCH_BLOCKING"] = "1"
# os.environ["TORCH_USE_CUDA_DSA"] = "1"

torch.backends.cudnn.benchmark=True
torch.hub._validate_not_a_forked_repo=lambda a,b,c: True
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = torch.hub.load('yolov5', 'custom', 'models/crowdhuman_yolov5m.pt', source='local', force_reload=True).to(device).eval()
# model = torch.hub.load('ultralytics/yolov5', 'custom', 'crowdhuman_yolov5m.pt', force_reload=True).to(device).eval()

# FaceDetector = torch.hub.load('ultralytics/yolov5', 'custom', 'models/YOLO/Best.onnx', _verbose=False, force_reload=True)
FaceDetector = torch.hub.load('yolov5', 'custom', 'models/YOLO/Best.onnx', source='local', _verbose=False, force_reload=True)
FaceDetector.eval().to(device)

Classes = 9
Groups = ['00-10', '11-20', '21-30', 
        '31-40', '41-50', '51-60', 
        '61-70', '71-80', '81-90']

ClassificationModel2 = 'models/ResNet-18/ResNet-18 Age 0.60 + Gender 93.pt'
FaceClassifier = models.resnet18(pretrained=True)
FaceClassifier.fc = nn.Linear(512, Classes+2)
FaceClassifier = nn.Sequential(FaceClassifier, nn.Sigmoid())
FaceClassifier.load_state_dict(torch.load(ClassificationModel2))
FaceClassifier.eval()
FaceClassifier.to(device)

conn = sqlite3.connect('setting.db')
cursor = conn.cursor()

def get_data_camera(table):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM {table} where deleted=1').fetchall()
    return data

def get_setting_camera_by_id(table, id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM {table} where deleted=1 and id={id_cam}').fetchone()
    return data

def get_cosntant_by_code(code):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
    return data 

def insert_data(data_xml, type_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    insert_query = "INSERT INTO app_logs (xml, is_sync, type_cam) VALUES (?, ?, ?)"
    cursor.execute(insert_query, (data_xml, 0,type_cam))
    conn.commit()        
    cursor.close()
    conn.close()

def simpan_log_area():
    conn1 = sqlite3.connect('setting.db')
    cursor1 = conn1.cursor()
    time_interval_secondary = get_cosntant_by_code('time_interval_secondary')
    site_code = get_cosntant_by_code('site_code')
    while True:
        camera_all = cursor1.execute(f'SELECT * FROM setting_camera_area').fetchall()        
        for obj in camera_all:
            device_code = obj[3]  # Assuming device_code is in column index 1
            location_code = obj[4]  # Assuming location_code is in column index 2
            status_camera = obj[12]  # Assuming location_code is in column index 2
            end_date = time.time() 
            start_date = end_date - float(time_interval_secondary[2])
            jam_start = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_date))
            jam_end = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(end_date))
            detail = eval(obj[9]) if obj[9] else []
            type_camera = 3
            root = Element('Metrics', site_code=f"{site_code[2]}", device_code=f"{device_code}", location_code=f"{site_code[2]}", start_date=f"{jam_start}", end_date=f"{jam_end}", jumlah_data=f'{len(detail)}', status=f'{status_camera}')
            for detail_item in detail:                
                detail_elem = SubElement(root, 'detail')                
                SubElement(detail_elem, 'area_code').text = str(detail_item['area_code'])
                SubElement(detail_elem, 'total_people').text = str(detail_item['people'])

            data = tostring(root, encoding='unicode')            
            insert_data(data, type_camera)
        # push_api()
        time.sleep(int(time_interval_secondary[2]))

def xml_inout_kanan_kiri(device_code, location_code, status_camera, people_in, people_out, date, jam_start, jam_end, type_camera, site_code, site_name, time_interval):
    tipe_xml = ''
    if int(type_camera) == 1:
        tipe_xml = '1'
    elif int(type_camera) == 4:
        tipe_xml = '2'
    # print(f"ssssssssssssssssssssssssssss ccccccccccccccccccccccccccccccccc{tipe_xml}")
    json_data = f'''
            {{
                "@attributes": {{
                    "from":"1",
                    "SiteId": "{site_code[2]}",
                    "Sitename": "{site_name[2]}",
                    "DeviceId": "A01",
                    "Devicename": "{device_code}",
                    "DivisionId": "LB",
                    "type": "{tipe_xml}"
                }},
                "Properties": {{
                    "Version": "5",
                    "TransmitTime": "1702868646",
                    "MacAddress": "00:b0:9d:e9:fe:de",
                    "IpAddress": "192.168.0.10",
                    "HostName": "Cam-15335134",
                    "HttpPort": "80",
                    "HttpsPort": "443",
                    "Timezone": "7",
                    "TimezoneName": "(GMT 07:00) Bangkok, Hanoi, Jakarta",
                    "DST": "0",
                    "HwPlatform": "2300",
                    "SerialNumber": "15335134",
                    "DeviceType": "0",
                    "SwRelease": "5.7.14.568"
                }},
                "ReportData": {{
                    "@attributes": {{
                        "Interval": "{time_interval[2]}"
                    }},
                    "Report": {{
                        "@attributes": {{
                            "Date": "{date}"
                        }},
                        "Object": {{
                            "@attributes": {{
                                "Id": "0",
                                "DeviceId": "A01",
                                "Devicename": "Device 1",
                                "ObjectType": "0",
                                "Name": "Device 1"
                            }},
                            "Count": {{
                                "@attributes": {{
                                    "StartTime": "{jam_start}",
                                    "EndTime": "{jam_end}",
                                    "UnixStartTime": "1702864800",
                                    "Enters": "{people_in}",
                                    "Exits": "{people_out}",
                                    "Status": "{status_camera}"
                                }}
                            }}
                        }}
                    }}
                }}
            }}
            ''' 
    # Konversi JSON menjadi Python dict
    data = json.loads(json_data)
    # Buat elemen root
    root = ET.Element('Metrics', data['@attributes'])
    root = ET.Element('Metrics', data['@attributes'])
    # Buat elemen Properties
    properties = ET.SubElement(root, 'Properties')
    for key, value in data['Properties'].items():
        prop = ET.SubElement(properties, key)
        prop.text = value
    # Buat elemen ReportData
    report_data = ET.SubElement(root, 'ReportData', data['ReportData']['@attributes'])
    report = ET.SubElement(report_data, 'Report', data['ReportData']['Report']['@attributes'])
    # Buat elemen Object dan Count di dalam Report
    object_elem = ET.SubElement(report, 'Object', data['ReportData']['Report']['Object']['@attributes'])
    count = ET.SubElement(object_elem, 'Count', data['ReportData']['Report']['Object']['Count']['@attributes'])
    data_xml = tostring(root, encoding='unicode')

    return data_xml

def simpan_log_in_out():
    conn1 = sqlite3.connect('setting.db')
    cursor1 = conn1.cursor()
    time_interval = get_cosntant_by_code('time_interval')
    site_code = get_cosntant_by_code('site_code')
    site_name = get_cosntant_by_code('site_name')
    while True:
        camera_all = cursor1.execute(f'SELECT * FROM setting_camera').fetchall()        
        for obj in camera_all:
            device_code = obj[9]  # Assuming device_code is in column index 1
            location_code = obj[10]  # Assuming location_code is in column index 2
            status_camera = obj[19]  # Assuming location_code is in column index 2
            people_in = obj[11]
            people_out = obj[12]
            end = time.time() 
            start = end - float(time_interval[2])
            type_camera = obj[18]
            date = time.strftime('%Y-%m-%d', time.localtime(start))
            jam_start = time.strftime('%H:%M:%S', time.localtime(start))
            jam_end = time.strftime('%H:%M:%S', time.localtime(end))
            # Data JSON
            data_xml= xml_inout_kanan_kiri(device_code, location_code, status_camera, people_in, people_out, date, jam_start, jam_end, type_camera, site_code, site_name, time_interval)
            # print(data_xml)
            insert_data(data_xml, type_camera)
            reset_data_in_out()
        # push_api()
        time.sleep(int(time_interval[2]))

def reset_data_gender():
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()    
    camera_gender = get_data_camera('setting_camera_gender')    
    for index, camera_data in enumerate(camera_gender):
        update_query = "UPDATE setting_camera_gender SET male_in = ?, male_out = ?, female_in = ?, female_out = ? where id = ?"
        cursor.execute(update_query,(0, 0, 0, 0, camera_data[0]))
        conn.commit()
    cursor.close()
    conn.close()
    # print("reset in out camera successfully")

def reset_data_in_out():
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    camera_in_out = get_data_camera('setting_camera')
    for index, camera_data in enumerate(camera_in_out):
        update_query = "UPDATE setting_camera SET people_in = ?, people_out = ? where id = ?"
        cursor.execute(update_query,(0, 0, camera_data[0]))
        conn.commit()
    cursor.close()
    conn.close()
    # print("reset in out camera successfully")

def simpan_log_gender():
    conn1 = sqlite3.connect('setting.db')
    cursor1 = conn1.cursor()
    time_interval = get_cosntant_by_code('time_interval')
    site_code = get_cosntant_by_code('site_code')
    while True:
        camera_all = cursor1.execute(f'SELECT * FROM setting_camera_gender').fetchall()        
        for obj in camera_all:
            device_code = obj[3]  # Assuming device_code is in column index 1
            location_code = site_code[2]  
            status_camera = obj[16] 
            male_in = obj[8]  
            male_out = obj[9]  
            female_in = obj[10]  
            female_out = obj[11]  
            end_date = time.time() 
            start_date = end_date - float(time_interval[2])
            jam_start = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_date))
            jam_end = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(end_date))
            type_camera = 2
            root = Element('Metrics', site_code=f"{site_code[2]}", device_code=f"{device_code}", location_code=f"{location_code}", start_date=f"{jam_start}", end_date=f"{jam_end}", jumlah_data="1", status=f"{status_camera}")
            detail_elem = SubElement(root, 'detail')                                   
            SubElement(detail_elem, 'male_in').text = str(male_in)
            SubElement(detail_elem, 'male_out').text = str(male_out)
            SubElement(detail_elem, 'female_in').text = str(female_in)
            SubElement(detail_elem, 'female_out').text = str(female_out)

            data = tostring(root, encoding='unicode')            
            # print(data)
            insert_data(data, type_camera)
            reset_data_gender()
        time.sleep(int(time_interval[2]))

def send_xml(id_data,data_xml,type_cam):
    if type_cam == 1:
        link = get_cosntant_by_code("url_api_xml_in_out")
        url = link[2]
    elif type_cam == 2:
        link = get_cosntant_by_code("url_api_xml_gender")
        url = link[2]
    elif type_cam == 3:
        link = get_cosntant_by_code("url_api_xml_area")
        url = link[2]
    elif type_cam == 4:
        link = get_cosntant_by_code("url_api_xml_in_out")
        url = link[2]
    headers = {
        'Content-Type': 'application/xml'
    }
    response = requests.post(url, data=data_xml, headers=headers)
    # print(response)
    # print(response.text)
    # Periksa status respons dari API
    if response.status_code == 200:
        print(f"id {id_data} : berhasil dikirim ke API.")
        update_sync_log(id_data)
    else:
        print(f"id {id_data} : Gagal mengirim data ke API. Status code: {response.status_code}")

def sync_log_inout_gender():
    conn1 = sqlite3.connect('setting.db')
    cursor1 = conn1.cursor()
    time_interval_sync = get_cosntant_by_code('time_interval_sync')
    while True:
        log_all = cursor1.execute(f'SELECT * FROM app_logs where is_sync = 0 and type_cam in(1,2,4)').fetchall()
        for obj in log_all:
            id_data = obj[0]
            data_xml = obj[1]
            type_cam = obj[3]
            send_xml(id_data,data_xml,type_cam)

        time.sleep(int(time_interval_sync[2]))

def sync_log_area():
    conn1 = sqlite3.connect('setting.db')
    cursor1 = conn1.cursor()
    time_interval_sync_secondary = get_cosntant_by_code('time_interval_sync_secondary')
    while True:
        cek_date()

        log_all = cursor1.execute(f'SELECT * FROM app_logs where is_sync = 0 and type_cam = 3').fetchall()
        for obj in log_all:
            id_data = obj[0]
            data_xml = obj[1]
            type_cam = obj[3]
            send_xml(id_data,data_xml,type_cam)

        time.sleep(int(time_interval_sync_secondary[2]))

def reset_array_camera_in_out(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()    
    camera_in_out = get_data_camera('setting_camera')
    for index, camera_data in enumerate(camera_in_out):
        update_query = "UPDATE setting_camera SET people_kanan = ?, people_kiri = ?, people_masuk = ? where id = ?"
        cursor.execute(update_query, ('[]', '[]', '[]', id_cam))
        conn.commit()

    cursor.close()
    conn.close()
    print(f"array camera in_out id: {id_cam}, reset successfully")

def reset_array_camera_gender(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()    
    camera_gender = get_data_camera('setting_camera_gender')    
    for index, camera_data in enumerate(camera_gender):
        update_query = "UPDATE setting_camera_gender SET array_kanan = ?, array_kiri = ? where id = ?"
        cursor.execute(update_query,('[]', '[]', id_cam))
        conn.commit()
    cursor.close()
    conn.close()
    print(f"array camera gender id: {id_cam},reset successfully")

def update_sync_log(id):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    
    update_query = "UPDATE app_logs SET is_sync = ? where id = ?"
    cursor.execute(update_query,(1,id))
    conn.commit()

    cursor.close()
    conn.close()
    # print("array camera reset successfully")

def update_status_camera(id, type_camera, status_camera):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    if type_camera == 1:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    elif type_camera == 2:        
        update_query = "UPDATE setting_camera_gender SET status_camera = ? where id = ?"
    elif type_camera == 3:
        update_query = "UPDATE setting_camera_area SET status_camera = ? where id = ?"
    elif type_camera == 4:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    cursor.execute(update_query,(status_camera,id))
    conn.commit()

    cursor.close()
    conn.close()

def insert_error_camera(id, error_type, camera_type, device_code):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    tipe_camera = ''
    if camera_type == 1:
        tipe_camera = "camera people_counting"
    elif camera_type == 2:
        tipe_camera = "camera gender"
    elif camera_type == 3:
        tipe_camera = "camera area"
    elif camera_type == 4:
        tipe_camera = "camera kiri kanan"

    if error_type == 1:
        keterangan = f"{tipe_camera}, device code : {device_code} mati"

    waktu = time.time()
    created_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(waktu))

    insert_query = "INSERT INTO error_logs (keterangan, error_type, device_code, created_at) VALUES (?, ?, ?, ?)"
    cursor.execute(insert_query,(keterangan, error_type, device_code, created_at))
    conn.commit()
    cursor.close()
    conn.close()

def cek_date():
    tanggal_sekarang = datetime.date.today()
    current_date = get_cosntant_by_code('current_date')
    tanggal_batas_string = current_date[2]
    tanggal_current = datetime.datetime.strptime(tanggal_batas_string, "%Y-%m-%d").date()
    # print(f"{tanggal_sekarang} - {tanggal_current}")
    if tanggal_sekarang > tanggal_current:
        update_constant(tanggal_sekarang, 'current_date')
        update_constant(0, 'outer_traffic_total_traffic')
        update_constant(0, 'outer_traffic_right')
        update_constant(0, 'outer_traffic_left')
        update_constant(0, 'gender_female_out')
        update_constant(0, 'gender_female_in')
        update_constant(0, 'gender_male_out')
        update_constant(0, 'gender_male_in')
        update_constant(0, 'visitor_daily_out')
        update_constant(0, 'visitor_daily_in')
        update_constant(0, 'zone_total_all_zones')

    return True

def update_constant(value, code):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    update_query = "UPDATE constant SET value = ? WHERE code = ?"                                    
    cursor.execute(update_query, (value, code))
    conn.commit()
    print("update constant current")

def restart_file():
    python = sys.executable
    os.execl(python, python, *sys.argv)

# camera keluar masuk
def gen(camera, id_cam, device_code, url, capture):
    # capture = cv2.VideoCapture(url)
    while True:
        try:
            ret, frame = capture.read()
            if ret:  # Check if reading was successful
                frame = camera.get_frame(frame, id_cam, 0)
                if frame is not None:
                    if hasattr(frame, 'shape') and frame.shape is not None:
                        update_status_camera(id_cam, 1, 1)
                        # Convert frame to bytes
                        frame_bytes = cv2.imencode('.jpg', frame)[1].tobytes()
                        # Include ratio in the response as bytes
                        yield (b'--frame\r\n'
                            b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
                    else:
                        print("frame kosong 1")
                        restart_file()
                else:
                    print("frame kosong 2")
                    restart_file()
            else:
                print("frame kosong 3")
                restart_file()
        except Exception as e:
            update_status_camera(id_cam, 1, 0)            
            print(f"{device_code} Terjadi kesalahan saat mengambil frame:", e)            
            print("Restarting application...")            
            restart_file()

def video_thread_in_out(camera_data):
    threads = []
    for cam_data in camera_data:
        capture = cv2.VideoCapture(cam_data[2])
        thread = threading.Thread(target=process_camera_frames_in_out, args=(cam_data, capture))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

def process_camera_frames_in_out(cam_data, capture):
    reset_array_camera_in_out(cam_data[0])    
    camera = VideoCamera(model, device)
    frame_generator = gen(camera, cam_data[0], cam_data[9], cam_data[2], capture)
    while True:
        frame_bytes = next(frame_generator)
        if frame_bytes is not None:
            # Lakukan sesuatu dengan frame_bytes
            pass      

# camera gender
def gen_face(vid_cap, id_cam, device_code, url, capture):
    # capture = cv2.VideoCapture(url)
    while True:
        try:
            ret, frame = capture.read()
            if ret:
                frame = vid_cap.detect_faces(frame, id_cam, 0)
                if frame is not None:
                    update_status_camera(id_cam, 2, 1)
                    # print(id_cam)
                    # Convert frame to bytes
                    frame_bytes = cv2.imencode('.jpg', frame)[1].tobytes()
                    # Include ratio in the response as bytes

                    yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
                else:
                    restart_file()
            else:
                restart_file()
        except Exception as e:
            update_status_camera(id_cam, 2, 0)
            # insert_error_camera(id_cam, 1, 2, device_code)
            print("Terjadi kesalahan saat mengambil frame:", e)
            # Memberikan nilai None jika terjadi kesalahan saat mengambil frame
            restart_file()

def video_thread_gender(camera_data):
    threads = []
    for cam_data in camera_data:
        capture = cv2.VideoCapture(cam_data[2])
        thread = threading.Thread(target=process_camera_frames_gender, args=(cam_data, capture))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

def process_camera_frames_gender(cam_data, capture):
    reset_array_camera_gender(cam_data[0])    
    camera = face_gender(device, FaceDetector, FaceClassifier)
    frame_generator = gen_face(camera, cam_data[0], cam_data[3], cam_data[2], capture)
    while True:
        frame_bytes = next(frame_generator)
        if frame_bytes is not None:
            # Lakukan sesuatu dengan frame_bytes
            pass

# camera area
def generate_frames(camera, id_cam, device_code, url, capture):
    # capture = cv2.VideoCapture(url)
    while True:
        try:
            ret, frame = capture.read()
            if ret:
                frame = camera.process_video(frame, id_cam, 0)
                if frame is not None:
                    update_status_camera(id_cam, 3, 1)
                    # Convert frame to bytes
                    frame_bytes = cv2.imencode('.jpg', frame)[1].tobytes()
                    # Include ratio in the response as bytes

                    yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
                else:
                    restart_file()
            else:
                restart_file()
        except Exception as e:
            update_status_camera(id_cam, 3, 0)
            # insert_error_camera(id_cam, 1, 3, device_code)
            print("Terjadi kesalahan saat mengambil frame:", e)
            # Memberikan nilai None jika terjadi kesalahan saat mengambil frame
            yield None

def video_thread_area(camera_data):
    threads = []
    for cam_data in camera_data:
        capture = cv2.VideoCapture(cam_data[2])
        thread = threading.Thread(target=process_camera_frames_area, args=(cam_data, capture))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

def process_camera_frames_area(cam_data, capture):
    # reset_array_camera_gender(cam_data[0])    
    camera = CountObject(model, cam_data[0])
    frame_generator = generate_frames(camera, cam_data[0], cam_data[3], cam_data[2], capture)
    while True:
        frame_bytes = next(frame_generator)
        if frame_bytes is not None:
            # Lakukan sesuatu dengan frame_bytes
            pass

def start_all_threads():
    camera_in_out_data = get_data_camera('setting_camera')
    camera_gender_data = get_data_camera('setting_camera_gender')
    camera_area_data = get_data_camera('setting_camera_area')

    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Memulai semua fungsi dalam executor
        executor.submit(video_thread_in_out, camera_in_out_data)
        executor.submit(video_thread_gender, camera_gender_data)
        executor.submit(video_thread_area, camera_area_data)
        executor.submit(simpan_log_in_out)
        executor.submit(simpan_log_gender)
        executor.submit(simpan_log_area)
        executor.submit(sync_log_inout_gender)
        executor.submit(sync_log_area)

# Memulai semua thread
start_all_threads()