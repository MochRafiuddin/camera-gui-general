import os

def replace_autocast_usage(file_path):
    with open(file_path, 'r',encoding='utf-8') as file:
        content = file.read()
    new_content = content.replace('torch.cuda.amp.autocast', 'torch.amp.autocast(\'cuda\'')
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write(new_content)

def replace_in_directory(directory):
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith('.py'):
                replace_autocast_usage(os.path.join(root, file))

# Ganti 'path_to_yolov5' dengan path ke direktori YOLOv5 Anda
replace_in_directory('yolov5')
