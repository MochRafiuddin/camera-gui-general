import sys
import os
import time
import subprocess
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal, QTimer
# from snapshot import App_snapshot

class App_sidebar(QMainWindow):
    menu_camera_list_clicked = pyqtSignal()
    zone_configuration_clicked = pyqtSignal(int, str, str)
    home_clicked = pyqtSignal()
    snapshot_clicked = pyqtSignal()
    camera_preview_clicked = pyqtSignal()
    monitoring_camera_preview_clicked = pyqtSignal()
    monitoring_view_setup_clicked = pyqtSignal()
    close_window = pyqtSignal()  # Atur sinyal close_window
    minimize_window = pyqtSignal()  # Atur sinyal close_window

    def __init__(self):
        super().__init__()
        self.update_layout()
        # self.hide_loading()   

    def emit_menu_home_clicked(self, event):
        # print("Menu home clicked")
        self.home_clicked.emit()

    def emit_menu_camera_list_clicked(self, event):
        # print("press camara")
        self.menu_camera_list_clicked.emit()

    def emit_zone_configuration_clicked(self, event):
        # print("press zona")
        self.zone_configuration_clicked.emit(0, '', '')

    def emit_snapshot_clicked(self, event):
        # print("Menu home clicked")
        self.snapshot_clicked.emit()

    def emit_camera_preview_clicked(self, event):
        # print("Menu home clicked")
        # self.show_loading()
        # QTimer.singleShot(1, print('loding'))
        # QTimer.singleShot(1, self.camera_preview_clicked.emit)
        self.camera_preview_clicked.emit()
        # self.hide_loading()   

    
    def emit_monitoring_camera_preview_clicked(self, event):
        # self.show_loading()   
        # QTimer.singleShot(1, print('loding'))
        self.monitoring_camera_preview_clicked.emit()
        # self.hide_loading()   
    
    def emit_monitoring_view_setup_clicked(self, event):
        # print("Menu home clicked")
        self.monitoring_view_setup_clicked.emit()

    # Fungsi yang akan dipanggil saat kursor masuk ke rectangle_label
    def home_hover(self, event):
        self.button_menu_home.setStyleSheet(
            "color: white; background-color: #2a2a2a; border: 2px solid #1C72B1; border-radius: 5px;"
        )

    # Fungsi yang akan dipanggil saat kursor meninggalkan rectangle_label
    def home_leave(self, event):
        self.button_menu_home.setStyleSheet(
            "color: white; background-color: #2a2a2a; border-radius: 5px;"
        )
    ###################################################################################################################

    # Fungsi yang akan dipanggil saat kursor masuk ke rectangle_label
    def camera_list_hover(self, event):
        self.button_menu_camera_list.setStyleSheet(
            "color: white; background-color: #2a2a2a; border: 2px solid #1C72B1; border-radius: 5px;"
        )

    # Fungsi yang akan dipanggil saat kursor meninggalkan rectangle_label
    def camera_list_leave(self, event):
        self.button_menu_camera_list.setStyleSheet(
            "color: white; background-color: #2a2a2a; border-radius: 5px;"
        )
    ###############################################################################################################################
    
    # Fungsi yang akan dipanggil saat kursor masuk ke rectangle_label
    def zone_configuration_hover(self, event):
        self.button_zone_configuration.setStyleSheet(
            "color: white; background-color: #2a2a2a; border: 2px solid #1C72B1; border-radius: 5px;"
        )
    # Fungsi yang akan dipanggil saat kursor meninggalkan rectangle_label
    def zone_configuration_leave(self, event):
        self.button_zone_configuration.setStyleSheet("color: white; background-color: #2a2a2a; border-radius: 5px;")
    
    def snapshot_hover(self, event):
        self.button_snapshot.setStyleSheet(
            "color: white; background-color: #2a2a2a; border: 2px solid #1C72B1; border-radius: 5px;"
        )
    # Fungsi yang akan dipanggil saat kursor meninggalkan rectangle_label
    def snapshot_leave(self, event):
        self.button_snapshot.setStyleSheet("color: white; background-color: #2a2a2a; border-radius: 5px;")

    def camera_preview_hover(self, event):
        self.button_camera_preview.setStyleSheet(
            "color: white; background-color: #2a2a2a; border: 2px solid #1C72B1; border-radius: 5px;"
        )
    # Fungsi yang akan dipanggil saat kursor meninggalkan rectangle_label
    def camera_preview_leave(self, event):
        self.button_camera_preview.setStyleSheet("color: white; background-color: #2a2a2a; border-radius: 5px;")
    
    def monitoring_camera_preview_hover(self, event):
        self.button_monitoring_camera_preview.setStyleSheet(
            "color: white; background-color: #2a2a2a; border: 2px solid #1C72B1; border-radius: 5px;"
        )
    # Fungsi yang akan dipanggil saat kursor meninggalkan rectangle_label
    def monitoring_camera_preview_leave(self, event):
        self.button_monitoring_camera_preview.setStyleSheet("color: white; background-color: #2a2a2a; border-radius: 5px;")
    
    def monitoring_view_setup_hover(self, event):
        self.button_monitoring_view_setup.setStyleSheet(
            "color: white; background-color: #2a2a2a; border: 2px solid #1C72B1; border-radius: 5px;"
        )
    # Fungsi yang akan dipanggil saat kursor meninggalkan rectangle_label
    def monitoring_view_setup_leave(self, event):
        self.button_monitoring_view_setup.setStyleSheet("color: white; background-color: #2a2a2a; border-radius: 5px;")

    ########################################################################################Proses jendela
    def minimizeWindow(self, event):
        # Minimize the window
        self.minimize_window.emit()

    def on_close_button_clicked(self, event):
        print("Close button clicked")  # Pastikan bahwa ini tercetak
        self.close_window.emit()
        # self.close()
        # Open keluar.py window
        # current_directory = os.path.dirname(os.path.abspath(__file__))
        # # Bentuk jalur absolut ke panel.py
        # panel_path = os.path.join(current_directory, "keluar.py")

        # # Mulai proses untuk menjalankan panel.py
        # subprocess.Popen(["python", panel_path])
    ##############################################################################################

    def update_layout(self):
        screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = screen_geometry.width() / 1280
        self.height_ratio = screen_geometry.height() / 832

        # Sidebar
        sidebar = QLabel(self)
        sidebar.setStyleSheet("background-color: #2a2a2a;")
        sidebar.setGeometry(0, 0, int(248 * self.width_ratio), screen_geometry.height())

        self.button_menu_home = QLabel(self)  ## home 1 punya tombol
        self.button_menu_home.setStyleSheet(
                "background-color: #2a2a2a; border-radius: 5px;"
        )
        self.button_menu_home.setGeometry(
            int(6 * self.width_ratio),
            int(149 * self.height_ratio),
            int(240 * self.width_ratio),
            int(48 * self.height_ratio),
        )
        self.button_menu_home.mousePressEvent = self.emit_menu_home_clicked
        self.button_menu_home.enterEvent = self.home_hover
        self.button_menu_home.leaveEvent = self.home_leave
        home_label = QLabel(self)
        home_icon = QPixmap("img/3.png")
        home_label.setPixmap(home_icon.scaled(20, 20, Qt.KeepAspectRatio))
        home_label.setStyleSheet("background-color: #2a2a2a")
        home_label.setGeometry(
            int(10 * self.width_ratio),
            int(160 * self.height_ratio),
            int(100 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        home_text = QLabel(self)
        home_text.setText("About")
        home_text.setStyleSheet(
            "color: white; background-color: #2a2a2a; font-weight: 400;"
        )
        home_text.setFont(QFont("Times", int(14 * min(self.width_ratio, self.height_ratio))))
        home_text.setGeometry(
            int(35 * self.width_ratio),
            int(160 * self.height_ratio),
            int(100 * self.width_ratio),
            int(24 * self.height_ratio),
        )

        ## menu 2 punya tombol
        self.button_menu_camera_list = QLabel(self)
        self.button_menu_camera_list.setStyleSheet(
            "background-color: #2a2a2a; border-radius: 5px;"
        )
        self.button_menu_camera_list.setGeometry(
            int(6 * self.width_ratio),
            int(203 * self.height_ratio),
            int(240 * self.width_ratio),
            int(48 * self.height_ratio),
        )
        self.button_menu_camera_list.mousePressEvent = self.emit_menu_camera_list_clicked
        self.button_menu_camera_list.enterEvent = self.camera_list_hover
        self.button_menu_camera_list.leaveEvent = self.camera_list_leave

        menu1_label = QLabel(self)
        menu1_icon = QPixmap("img/4.png")
        menu1_label.setPixmap(menu1_icon.scaled(20, 20, Qt.KeepAspectRatio))
        menu1_label.setStyleSheet("background-color: #2a2a2a;")
        menu1_label.setGeometry(
            int(10 * self.width_ratio),
            int(214 * self.height_ratio),
            int(100 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        menu1_text = QLabel(self)
        menu1_text.setText("Camera List")
        menu1_text.setStyleSheet(
            "color: white; background-color: #2a2a2a; font-weight: 400;"
        )
        menu1_text.setFont(QFont("Times", int(14 * min(self.width_ratio, self.height_ratio))))
        menu1_text.setGeometry(
            int(35 * self.width_ratio),
            int(214 * self.height_ratio),
            int(90 * self.width_ratio),
            int(24 * self.height_ratio),
        )

        ## menu 3 punya tombol
        self.button_zone_configuration = QLabel(self)  
        self.button_zone_configuration.setStyleSheet(
            "background-color: #2a2a2a; border-radius: 5px;"
        )
        self.button_zone_configuration.setGeometry(
            int(6 * self.width_ratio),
            int(259 * self.height_ratio),
            int(240 * self.width_ratio),
            int(48 * self.height_ratio),
        )

        # Menghubungkan event hover ke fungsi
        self.button_zone_configuration.mousePressEvent = self.emit_zone_configuration_clicked
        self.button_zone_configuration.enterEvent = self.zone_configuration_hover
        self.button_zone_configuration.leaveEvent = self.zone_configuration_leave
        menu2_label = QLabel(self)
        menu2_icon = QPixmap("img/1.png")
        menu2_label.setPixmap(menu2_icon.scaled(20, 20, Qt.KeepAspectRatio))
        menu2_label.setStyleSheet("background-color: #2a2a2a;")
        menu2_label.setGeometry(
            int(10 * self.width_ratio),
            int(270 * self.height_ratio),
            int(100 * self.width_ratio),
            int(24 * self.height_ratio),
        )

        menu2_text = QLabel(self)
        menu2_text.setText("Zone Configuration")
        menu2_text.setStyleSheet(
            "color: white; background-color: #2a2a2a; font-weight: 400;"
        )
        menu2_text.setFont(QFont("Times", int(14 * min(self.width_ratio, self.height_ratio))))
        menu2_text.setGeometry(
            int(35 * self.width_ratio),
            int(270 * self.height_ratio),
            int(135 * self.width_ratio),
            int(24 * self.height_ratio),
        )

        ## menu 4 punya tombol
        self.button_snapshot = QLabel(self)  
        self.button_snapshot.setStyleSheet(
            "background-color: #2a2a2a; border-radius: 5px;"
        )
        self.button_snapshot.setGeometry(
            int(6 * self.width_ratio),
            int(322 * self.height_ratio),
            int(240 * self.width_ratio),
            int(48 * self.height_ratio),
        )
        self.button_snapshot.mousePressEvent = self.emit_snapshot_clicked
        self.button_snapshot.enterEvent = self.snapshot_hover
        self.button_snapshot.leaveEvent = self.snapshot_leave
        menu2_label = QLabel(self)
        menu2_icon = QPixmap("img/2.png")
        menu2_label.setPixmap(menu2_icon.scaled(20, 20, Qt.KeepAspectRatio))
        menu2_label.setStyleSheet("background-color: #2a2a2a;")
        menu2_label.setGeometry(
            int(10 * self.width_ratio),
            int(333 * self.height_ratio),
            int(100 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        menu2_text = QLabel(self)
        menu2_text.setText("Snapshot")
        menu2_text.setStyleSheet(
            "color: white; background-color: #2a2a2a; font-weight: 400;"
        )
        menu2_text.setFont(QFont("Times", int(14 * min(self.width_ratio, self.height_ratio))))
        menu2_text.setGeometry(
            int(35 * self.width_ratio),
            int(333 * self.height_ratio),
            int(65 * self.width_ratio),
            int(24 * self.height_ratio),
        )

        ## menu 5 punya tombol
        # self.button_camera_preview = QLabel(self)  
        # self.button_camera_preview.setStyleSheet(
        #     "background-color: #2a2a2a; border-radius: 5px;"
        # )
        # self.button_camera_preview.setGeometry(
        #     int(6 * self.width_ratio),
        #     int(385 * self.height_ratio),
        #     int(240 * self.width_ratio),
        #     int(48 * self.height_ratio),
        # )
        # self.button_camera_preview.mousePressEvent = self.emit_camera_preview_clicked
        # self.button_camera_preview.enterEvent = self.camera_preview_hover
        # self.button_camera_preview.leaveEvent = self.camera_preview_leave
        # menu5_label = QLabel(self)
        # menu5_icon = QPixmap("img/cameras.png")
        # menu5_label.setPixmap(menu5_icon.scaled(20, 20, Qt.KeepAspectRatio))
        # menu5_label.setStyleSheet("background-color: #2a2a2a;")
        # menu5_label.setGeometry(
        #     int(10 * self.width_ratio),
        #     int(396 * self.height_ratio),
        #     int(100 * self.width_ratio),
        #     int(24 * self.height_ratio),
        # )
        # menu5_text = QLabel(self)
        # menu5_text.setText("All Camera")
        # menu5_text.setStyleSheet(
        #     "color: white; background-color: #2a2a2a; font-weight: 400;"
        # )
        # menu5_text.setFont(QFont("Times", int(14 * min(self.width_ratio, self.height_ratio))))
        # menu5_text.setGeometry(
        #     int(35 * self.width_ratio),
        #     int(396 * self.height_ratio),
        #     int(120 * self.width_ratio),
        #     int(24 * self.height_ratio),
        # )

        # ## menu 6 punya tombol
        # self.button_monitoring_camera_preview = QLabel(self)  
        # self.button_monitoring_camera_preview.setStyleSheet(
        #     "background-color: #2a2a2a; border-radius: 5px;"
        # )
        # self.button_monitoring_camera_preview.setGeometry(
        #     int(6 * self.width_ratio),
        #     int(448 * self.height_ratio),
        #     int(240 * self.width_ratio),
        #     int(48 * self.height_ratio),
        # )
        # self.button_monitoring_camera_preview.mousePressEvent = self.emit_monitoring_camera_preview_clicked
        # self.button_monitoring_camera_preview.enterEvent = self.monitoring_camera_preview_hover
        # self.button_monitoring_camera_preview.leaveEvent = self.monitoring_camera_preview_leave
        # menu6_label = QLabel(self)
        # menu6_icon = QPixmap("img/monitoring.png")
        # menu6_label.setPixmap(menu6_icon.scaled(20, 20, Qt.KeepAspectRatio))
        # menu6_label.setStyleSheet("background-color: #2a2a2a;")
        # menu6_label.setGeometry(
        #     int(10 * self.width_ratio),
        #     int(459 * self.height_ratio),
        #     int(100 * self.width_ratio),
        #     int(24 * self.height_ratio),
        # )
        # menu6_text = QLabel(self)
        # menu6_text.setText("Monitoring View")
        # menu6_text.setStyleSheet(
        #     "color: white; background-color: #2a2a2a; font-weight: 400;"
        # )
        # menu6_text.setFont(QFont("Times", int(14 * min(self.width_ratio, self.height_ratio))))
        # menu6_text.setGeometry(
        #     int(35 * self.width_ratio),
        #     int(459 * self.height_ratio),
        #     int(135 * self.width_ratio),
        #     int(24 * self.height_ratio),
        # )

        ## menu 7 punya tombol
        self.button_monitoring_view_setup = QLabel(self)  
        self.button_monitoring_view_setup.setStyleSheet(
            "background-color: #2a2a2a; border-radius: 5px;"
        )
        # self.button_monitoring_view_setup.setGeometry(
        #     int(6 * self.width_ratio),
        #     int(501 * self.height_ratio),
        #     int(240 * self.width_ratio),
        #     int(48 * self.height_ratio),
        # )
        self.button_monitoring_view_setup.setGeometry(
            int(6 * self.width_ratio),
            int(385 * self.height_ratio),
            int(240 * self.width_ratio),
            int(48 * self.height_ratio),
        )
        self.button_monitoring_view_setup.mousePressEvent = self.emit_monitoring_view_setup_clicked
        self.button_monitoring_view_setup.enterEvent = self.monitoring_view_setup_hover
        self.button_monitoring_view_setup.leaveEvent = self.monitoring_view_setup_leave
        menu7_label = QLabel(self)
        menu7_icon = QPixmap("img/setup.png")
        menu7_label.setPixmap(menu7_icon.scaled(20, 20, Qt.KeepAspectRatio))
        menu7_label.setStyleSheet("background-color: #2a2a2a;")
        # menu7_label.setGeometry(
        #     int(10 * self.width_ratio),
        #     int(514 * self.height_ratio),
        #     int(100 * self.width_ratio),
        #     int(24 * self.height_ratio),
        # )
        menu7_label.setGeometry(
            int(10 * self.width_ratio),
            int(396 * self.height_ratio),
            int(100 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        menu7_text = QLabel(self)
        menu7_text.setText("Monitoring View Setup")
        menu7_text.setStyleSheet(
            "color: white; background-color: #2a2a2a; font-weight: 400;"
        )
        menu7_text.setFont(QFont("Times", int(14 * min(self.width_ratio, self.height_ratio))))
        # menu7_text.setGeometry(
        #     int(35 * self.width_ratio),
        #     int(514 * self.height_ratio),
        #     int(158 * self.width_ratio),
        #     int(24 * self.height_ratio),
        # )
        menu7_text.setGeometry(
            int(35 * self.width_ratio),
            int(396 * self.height_ratio),
            int(158 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        

        self.loading_text = QLabel(self)
        self.loading_text.setText("loading . . .")
        self.loading_text.setStyleSheet(
            "color: black; background-color: orange; font-weight: 400; border: 1px solid orange;"
        )
        self.loading_text.setFont(QFont("Times", int(10 * min(self.width_ratio, self.height_ratio))))
        self.loading_text.setGeometry(
            int(35 * self.width_ratio),
            int(100 * self.height_ratio),
            int(135 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        self.loading_text.setAlignment(Qt.AlignCenter)  # Mengatur alignment grid layout ke atas
        self.loading_text.setVisible(False)  # Menampilkan teks loading

        
        # top bar
        top_bar = QLabel(self)
        top_bar.setStyleSheet("background-color: #000000;")
        top_bar.setGeometry(0, 0, screen_geometry.width(), int(63 * self.height_ratio))

        self.icon1_label = QLabel(self)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.setGeometry(
            int(1206 * self.width_ratio),
            int(17 * self.height_ratio),
            int(24 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        ) 
        # self.icon1_label.setScaledContents(True)
    
        self.icon1_label.mousePressEvent = self.minimizeWindow

        self.icon2_label = QLabel(self)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon2_label.setGeometry(
            int(1232 * self.width_ratio),
            int(15 * self.height_ratio),
            int(33 * self.width_ratio),
            int(33 * self.height_ratio),
        )
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        ) 

    
        self.icon3_label = QLabel(self)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon3_label.setGeometry(
            int(1252 * self.width_ratio),
            int(9 * self.height_ratio),
            int(45 * self.width_ratio),
            int(45 * self.height_ratio),
        )
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  
        self.icon3_label.mousePressEvent = self.on_close_button_clicked
        ###############################################################

        control_panel_label = QLabel("Control Panel", self)
        control_panel_label.setStyleSheet("color: white; background-color: #000000;")
        control_panel_label.setFont(
            QFont("Times", int(23 * min(self.width_ratio, self.height_ratio)))
        )
        control_panel_label.setGeometry(
            int(30 * self.width_ratio),
            int(10 * self.height_ratio),
            int(190 * self.width_ratio),
            int(45 * self.height_ratio),
        )

    def show_loading(self):
        print("tampil loagin")
        self.loading_text.setVisible(True)
    
    def hide_loading(self):
        print("hide loagin")
        self.loading_text.setVisible(False)

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_sidebar()
#     window.show()
#     sys.exit(app.exec_())
