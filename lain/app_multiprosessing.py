from xml.etree.ElementTree import Element, SubElement, tostring
import xml.etree.ElementTree as ET
from flask_cors import CORS
from keluar_masuk import VideoCamera
from area import CountObject
from gender1 import face_gender
import torch
import sqlite3
import time
import cv2
import json
import requests
import time
import torchvision.models as models # ResNet-18 PyTorch Model.
from torch import nn # Neural Network Layers
import datetime
import sys
import traceback
import os
import threading
import concurrent.futures
import multiprocessing


# os.environ["CUDA_LAUNCH_BLOCKING"] = "1"
# os.environ["TORCH_USE_CUDA_DSA"] = "1"

torch.backends.cudnn.benchmark=True
torch.hub._validate_not_a_forked_repo=lambda a,b,c: True

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = torch.hub.load('yolov5', 'custom', 'models/crowdhuman_yolov5m.pt', source='local', force_reload=True).to(device).eval()
# model = torch.hub.load('ultralytics/yolov5', 'custom', 'crowdhuman_yolov5m.pt', force_reload=True).to(device).eval()

# FaceDetector = torch.hub.load('ultralytics/yolov5', 'custom', 'models/YOLO/Best.onnx', _verbose=False, force_reload=True)
FaceDetector = torch.hub.load('yolov5', 'custom', 'models/YOLO/Best.onnx', source='local', _verbose=False, force_reload=True)
FaceDetector.eval().to(device)

Classes = 9
Groups = ['00-10', '11-20', '21-30', 
        '31-40', '41-50', '51-60', 
        '61-70', '71-80', '81-90']

ClassificationModel2 = 'models/ResNet-18/ResNet-18 Age 0.60 + Gender 93.pt'
FaceClassifier = models.resnet18(pretrained=True)
FaceClassifier.fc = nn.Linear(512, Classes+2)
FaceClassifier = nn.Sequential(FaceClassifier, nn.Sigmoid())
FaceClassifier.load_state_dict(torch.load(ClassificationModel2))
FaceClassifier.eval()
FaceClassifier.to(device)

conn = sqlite3.connect('setting.db')
cursor = conn.cursor()

def initialize_log_txt():
    log_file = 'detection.txt'
    now = datetime.datetime.today()
    formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")
    if os.path.exists(log_file):
        with open(log_file, 'a') as f:
            f.write(f'# Start Detection - {formatted_now}\n')
    else:
        with open(log_file, 'w') as f:
            f.write(f'# Start Detection - {formatted_now}\n')

def update_log_txt(new_segment):
    log_file = 'detection.txt'
    now = datetime.datetime.today()
    formatted_now = now.strftime("%Y-%m-%d %H:%M:%S")

    with open(log_file, 'a') as f:        
        f.write(f'{formatted_now} -- {new_segment}\n')

def get_data_camera(table):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM {table} where deleted=1').fetchall()
    return data

def get_setting_camera_by_id(table, id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM {table} where deleted=1 and id={id_cam}').fetchone()
    return data

def get_cosntant_by_code(code):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
    return data 

def reset_array_camera_in_out(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()    
    camera_in_out = get_data_camera('setting_camera')
    for index, camera_data in enumerate(camera_in_out):
        update_query = "UPDATE setting_camera SET people_kanan = ?, people_kiri = ?, people_masuk = ? where id = ?"
        cursor.execute(update_query, ('[]', '[]', '[]', id_cam))
        conn.commit()

    cursor.close()
    conn.close()
    # print(f"array camera in_out id: {id_cam}, reset successfully")
    update_log_txt(f"array camera in_out id: {id_cam}, reset successfully")

def reset_array_camera_gender(id_cam):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()    
    camera_gender = get_data_camera('setting_camera_gender')    
    for index, camera_data in enumerate(camera_gender):
        update_query = "UPDATE setting_camera_gender SET array_kanan = ?, array_kiri = ? where id = ?"
        cursor.execute(update_query,('[]', '[]', id_cam))
        conn.commit()
    cursor.close()
    conn.close()
    # print(f"array camera gender id: {id_cam},reset successfully")
    update_log_txt(f"array camera gender id: {id_cam},reset successfully")

def update_status_camera(id, type_camera, status_camera):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    if type_camera == 1:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    elif type_camera == 2:        
        update_query = "UPDATE setting_camera_gender SET status_camera = ? where id = ?"
    elif type_camera == 3:
        update_query = "UPDATE setting_camera_area SET status_camera = ? where id = ?"
    elif type_camera == 4:
        update_query = "UPDATE setting_camera SET status_camera = ? where id = ?"
    cursor.execute(update_query,(status_camera,id))
    conn.commit()

    cursor.close()
    conn.close()

def insert_error_camera(id, error_type, camera_type, device_code):
    conn = sqlite3.connect('setting.db')
    cursor = conn.cursor()
    tipe_camera = ''
    if camera_type == 1:
        tipe_camera = "camera people_counting"
    elif camera_type == 2:
        tipe_camera = "camera gender"
    elif camera_type == 3:
        tipe_camera = "camera area"
    elif camera_type == 4:
        tipe_camera = "camera kiri kanan"

    if error_type == 1:
        keterangan = f"{tipe_camera}, device code : {device_code} mati"

    waktu = time.time()
    created_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(waktu))

    insert_query = "INSERT INTO error_logs (keterangan, error_type, device_code, created_at) VALUES (?, ?, ?, ?)"
    cursor.execute(insert_query,(keterangan, error_type, device_code, created_at))
    conn.commit()
    cursor.close()
    conn.close()

def restart_file():
    python = sys.executable
    os.execl(python, python, *sys.argv)

# camera keluar masuk
def gen(camera, id_cam, device_code, url, capture):
    # capture = cv2.VideoCapture(url)
    while True:
        try:
            ret, frame = capture.read()
            if ret:  # Check if reading was successful
                frame = camera.get_frame(frame, id_cam, 0)
                if frame is not None:
                    if hasattr(frame, 'shape') and frame.shape is not None:
                        update_status_camera(id_cam, 1, 1)
                        # Convert frame to bytes
                        frame_bytes = cv2.imencode('.jpg', frame)[1].tobytes()
                        # Include ratio in the response as bytes
                        yield (b'--frame\r\n'
                            b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
                    else:
                        # print("frame kosong 1")
                        update_log_txt(f"camera in_out id: {id_cam}, frame kosong 1, reset file")
                        restart_file()
                else:
                    # print("frame kosong 2")
                    update_log_txt(f"camera in_out id: {id_cam}, frame kosong 2, reset file")
                    restart_file()
            else:
                # print("frame kosong 3")
                update_log_txt(f"camera in_out id: {id_cam}, frame kosong 3, reset file")
                restart_file()
        except Exception as e:
            update_status_camera(id_cam, 1, 0)            
            # print(f"{device_code} Terjadi kesalahan saat mengambil frame:", e)            
            # print("Restarting application...")            
            update_log_txt(f"camera in_out id: {id_cam}--{device_code}, Terjadi kesalahan saat mengambil frame:", e)
            restart_file()

def video_process_in_out(camera_data):
    capture = cv2.VideoCapture(camera_data[2])
    process_camera_frames_in_out(camera_data, capture)

def process_camera_frames_in_out(cam_data, capture):
    reset_array_camera_in_out(cam_data[0])    
    camera = VideoCamera(model, device)
    frame_generator = gen(camera, cam_data[0], cam_data[9], cam_data[2], capture)
    while True:
        frame_bytes = next(frame_generator)
        if frame_bytes is not None:
            # Lakukan sesuatu dengan frame_bytes
            pass      

# camera gender
def gen_face(vid_cap, id_cam, device_code, url, capture):
    # capture = cv2.VideoCapture(url)
    while True:
        try:
            ret, frame = capture.read()
            if ret:
                frame = vid_cap.detect_faces(frame, id_cam, 0)
                if frame is not None:
                    update_status_camera(id_cam, 2, 1)                    
                    # Convert frame to bytes
                    frame_bytes = cv2.imencode('.jpg', frame)[1].tobytes()
                    # Include ratio in the response as bytes

                    yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
                else:
                    update_log_txt(f"camera gender id: {id_cam}, frame kosong 2, reset file")
                    restart_file()
            else:
                update_log_txt(f"camera gender id: {id_cam}, frame kosong 3, reset file")
                restart_file()
        except Exception as e:
            update_status_camera(id_cam, 2, 0)
            # insert_error_camera(id_cam, 1, 2, device_code)
            # print("Terjadi kesalahan saat mengambil frame:", e)
            # Memberikan nilai None jika terjadi kesalahan saat mengambil frame
            update_log_txt(f"camera gender id: {id_cam}--{device_code}, Terjadi kesalahan saat mengambil frame:", e)
            restart_file()

def video_process_gender(camera_data):
    capture = cv2.VideoCapture(camera_data[2])
    process_camera_frames_gender(camera_data, capture)

def process_camera_frames_gender(cam_data, capture):
    reset_array_camera_gender(cam_data[0])    
    camera = face_gender(device, FaceDetector, FaceClassifier)
    frame_generator = gen_face(camera, cam_data[0], cam_data[3], cam_data[2], capture)
    while True:
        frame_bytes = next(frame_generator)
        if frame_bytes is not None:
            # Lakukan sesuatu dengan frame_bytes
            pass

# camera area
def generate_frames(camera, id_cam, device_code, url, capture):
    # capture = cv2.VideoCapture(url)
    while True:
        try:
            ret, frame = capture.read()
            if ret:
                frame = camera.process_video(frame, id_cam, 0)
                if frame is not None:
                    update_status_camera(id_cam, 3, 1)
                    # Convert frame to bytes
                    frame_bytes = cv2.imencode('.jpg', frame)[1].tobytes()
                    # Include ratio in the response as bytes

                    yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
                else:
                    update_log_txt(f"camera area id: {id_cam}, frame kosong 2, reset file")
                    restart_file()
            else:
                update_log_txt(f"camera area id: {id_cam}, frame kosong 3, reset file")
                restart_file()
        except Exception as e:
            update_status_camera(id_cam, 3, 0)
            # insert_error_camera(id_cam, 1, 3, device_code)
            # print("Terjadi kesalahan saat mengambil frame:", e)
            # Memberikan nilai None jika terjadi kesalahan saat mengambil frame
            update_log_txt(f"camera area id: {id_cam}--{device_code}, Terjadi kesalahan saat mengambil frame:", e)
            restart_file()

def video_process_area(camera_data):
    capture = cv2.VideoCapture(camera_data[2])
    process_camera_frames_area(camera_data, capture)

def process_camera_frames_area(cam_data, capture):
    # reset_array_camera_gender(cam_data[0])    
    camera = CountObject(model, cam_data[0])
    frame_generator = generate_frames(camera, cam_data[0], cam_data[3], cam_data[2], capture)
    while True:
        frame_bytes = next(frame_generator)
        if frame_bytes is not None:
            # Lakukan sesuatu dengan frame_bytes
            pass

def start_all_processes():
    camera_in_out_data = get_data_camera('setting_camera')
    camera_gender_data = get_data_camera('setting_camera_gender')
    camera_area_data = get_data_camera('setting_camera_area')

    processes = []
    for cam_data in camera_in_out_data:
        process = multiprocessing.Process(target=video_process_in_out, args=(cam_data,))
        process.start()
        processes.append(process)

    for cam_data in camera_gender_data:
        process = multiprocessing.Process(target=video_process_gender, args=(cam_data,))
        process.start()
        processes.append(process)

    for cam_data in camera_area_data:
        process = multiprocessing.Process(target=video_process_area, args=(cam_data,))
        process.start()
        processes.append(process)

    for process in processes:
        if process.is_alive():  
            process.join()
        else:
            print("Process", process, "belum dimulai.")

if __name__ == '__main__':
    initialize_log_txt()
    start_all_processes()