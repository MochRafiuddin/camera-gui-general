import cv2
import torch
import time
import numpy as np

class CentroidTracker:
    def __init__(self, max_disappeared=40):
        self.next_object_id = 0
        self.objects = {}
        self.disappeared = {}
        self.max_disappeared = max_disappeared

    def register(self, centroid):
        self.objects[self.next_object_id] = centroid
        self.disappeared[self.next_object_id] = 0
        self.next_object_id += 1

    def deregister(self, object_id):
        del self.objects[object_id]
        del self.disappeared[object_id]

    def update(self, rects):
        if len(rects) == 0:
            for object_id in list(self.disappeared.keys()):
                self.disappeared[object_id] += 1

                if self.disappeared[object_id] > self.max_disappeared:
                    self.deregister(object_id)

            return self.objects, {}

        input_centroids = np.zeros((len(rects), 2), dtype="int")
        input_boxes = {}

        for (i, (startX, startY, endX, endY)) in enumerate(rects):
            cX = int((startX + endX) / 2.0)
            cY = int((startY + endY) / 2.0)
            input_centroids[i] = (cX, cY)
            input_boxes[i] = (startX, startY, endX, endY)

        if len(self.objects) == 0:
            for i in range(0, len(input_centroids)):
                self.register(input_centroids[i])
        else:
            object_centroids = list(self.objects.values())
            object_ids = list(self.objects.keys())
            object_centroids = np.array(object_centroids)
            input_centroids = np.array(input_centroids)

            D = np.linalg.norm(object_centroids[:, np.newaxis] - input_centroids, axis=2)

            rows = D.min(axis=1).argsort()
            cols = D.argmin(axis=1)[rows]

            used_rows = set()
            used_cols = set()

            for (row, col) in zip(rows, cols):
                if row in used_rows or col in used_cols:
                    continue

                object_id = object_ids[row]
                self.objects[object_id] = input_centroids[col]
                self.disappeared[object_id] = 0

                used_rows.add(row)
                used_cols.add(col)

            unused_rows = set(range(0, D.shape[0])).difference(used_rows)
            unused_cols = set(range(0, D.shape[1])).difference(used_cols)

            if D.shape[0] >= D.shape[1]:
                for row in unused_rows:
                    object_id = object_ids[row]
                    self.disappeared[object_id] += 1

                    if self.disappeared[object_id] > self.max_disappeared:
                        self.deregister(object_id)
            else:
                for col in unused_cols:
                    self.register(input_centroids[col])

        return self.objects, input_boxes

class VideoCamera:
    def __init__(self):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model = torch.hub.load('yolov5', 'custom', 'models/crowdhuman_yolov5m.pt', source='local', force_reload=True).to(device).eval()
        self.device = device
        self.model = model
        self.centroid_tracker = CentroidTracker(max_disappeared=30)
        self.fps = 0        
        self.frame_count = 0
        self.start_time = time.time()
        self.last_time_insert = time.time()
        
        self.data_camera = {
            'people_kanan': [],
            'people_kiri': [],
            'in_jembatan': 0,
            'people_in': 0,
            'people_out': 0,
            'enter_jam': 0,
            'label': '',
            'data_garis': [(810, 27), (761, 690)],  # Example line coordinates
            'data_roi': [(23, 21), (24, 690), (1375, 691), (1373, 18), (70, 59)],  # Example ROI
            'rasio_width': 0.9175627240143369,
            'rasio_height': 1.0098176718092566,
            'in_side': 'left'  # 'left' or 'right'
        }

        self.tracked_objects = {}
        self.people_in = 0
        self.people_out = 0

    def get_frame(self, frame, id_camera, data_record):
        self.id_data = id_camera
        data = self.data_camera

        people_kanan = data['people_kanan']
        people_kiri = data['people_kiri']
        array_people_masuk = data.get('people_masuk', [])
        in_jembatan = data['in_jembatan']
        people_in = data['people_in']
        people_out = data['people_out']
        enter_jam = data['enter_jam']
        label = data['label']
        data_garis = data['data_garis']
        data_roi = data['data_roi']
        rasio_width = data['rasio_width']
        rasio_height = data['rasio_height']
        in_side = data['in_side']

        height, width, _ = frame.shape
        self.frame_count += 1
        if self.frame_count >= 15:
            end_time = time.time()
            elapsed_time = end_time - self.start_time
            self.fps = self.frame_count / elapsed_time
            self.frame_count = 0
            self.start_time = time.time()

        cv2.putText(frame, f"FPS: {self.fps:.2f}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
        cv2.putText(frame, f"{label}", (10, height - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        garis = data_garis
        if len(garis) == 2:
            garis = [list(point) for point in garis]
            garis[0][0] = int(garis[0][0] * float(rasio_width))
            garis[0][1] = int(garis[0][1] * float(rasio_height))
            garis[1][0] = int(garis[1][0] * float(rasio_width))
            garis[1][1] = int(garis[1][1] * float(rasio_height))
            cv2.line(frame, (garis[0][0], garis[0][1]), (garis[1][0], garis[1][1]), (0, 0, 0), thickness=2)
        elif len(garis) < 2:
            for line in garis:
                cv2.circle(frame, (int(line[0]), int(line[1])), 4, (0, 0, 0), -1)

        roi = data_roi
        if len(roi) > 3:
            roi_coords = np.array(roi, np.float64)
            roi_coords[:, 0] *= float(rasio_width)
            roi_coords[:, 1] *= float(rasio_height)
            roi_coords = roi_coords.astype(np.int32)
            roi_coords = roi_coords.reshape((-1, 1, 2))
            cv2.polylines(frame, [roi_coords], True, (0, 0, 255), thickness=2)
            mask = np.zeros_like(frame)
            cv2.fillPoly(mask, [roi_coords], (255, 255, 255))
            roi_frame = cv2.bitwise_and(frame, mask)
            results, input_boxes = self.detect_objects(roi_frame)
            frame = self.draw_boxes(frame, input_boxes)
        else:
            results, input_boxes = self.detect_objects(frame)
            frame = self.draw_boxes(frame, input_boxes)

        frame_vidio = frame.copy()

        for (object_id, centroid) in results.items():
            text = f"ID {object_id}"
            cv2.putText(frame, f"ID {object_id}", (centroid[0], centroid[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            cv2.putText(frame_vidio, f"ID {object_id}", (centroid[0], centroid[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

            if len(garis) == 2:
                if object_id not in self.tracked_objects:
                    if self.is_left(garis[0], garis[1], centroid):
                        self.tracked_objects[object_id] = ("left", False)
                    elif self.is_right(garis[0], garis[1], centroid):
                        self.tracked_objects[object_id] = ("right", False)
                else:
                    current_position = "left" if self.is_left(garis[0], garis[1], centroid) else "right"
                    last_position, has_crossed = self.tracked_objects[object_id]

                    if in_side == "left":
                        if last_position == "left" and current_position == "right" and not has_crossed:
                            self.people_in += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "right" and current_position == "left" and not has_crossed:
                            self.people_out += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "left" and current_position == "right" and has_crossed:
                            self.people_out -= 1
                            self.tracked_objects[object_id] = (current_position, False)
                        elif last_position == "right" and current_position == "left" and has_crossed:
                            self.people_in -= 1
                            self.tracked_objects[object_id] = (current_position, False)
                    else:
                        if last_position == "right" and current_position == "left" and not has_crossed:
                            self.people_in += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "left" and current_position == "right" and not has_crossed:
                            self.people_out += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "right" and current_position == "left" and has_crossed:
                            self.people_out -= 1
                            self.tracked_objects[object_id] = (current_position, False)
                        elif last_position == "left" and current_position == "right" and has_crossed:
                            self.people_in -= 1
                            self.tracked_objects[object_id] = (current_position, False)

        cv2.putText(frame_vidio, f"in: {self.people_in}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
        cv2.putText(frame_vidio, f"out: {self.people_out}", (10, 80), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        cv2.putText(frame, f"in: {self.people_in}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
        cv2.putText(frame, f"out: {self.people_out}", (10, 80), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        current_time = time.time()
        if (current_time - self.last_time_insert) > 60:  # Ubah sesuai interval waktu yang diinginkan
            self.last_time_insert = current_time

        return frame, frame_vidio

    def detect_objects(self, frame):
        with torch.amp.autocast('cuda'):
            results = self.model(frame)
        objects = []

        for index, row in results.pandas().xyxy[0].iterrows():
            confidence = float(row['confidence'])
            class_name = row['name']
            if class_name != "head":
                continue
            if confidence > 0.375:
                x1 = int(row['xmin'])
                y1 = int(row['ymin'])
                x2 = int(row['xmax'])
                y2 = int(row['ymax'])
                class_name = str(row['name'])
                objects.append({'label': class_name, 'confidence': confidence, 'bbox': row})

        bounding_boxes = [(int(obj['bbox']['xmin']), int(obj['bbox']['ymin']), int(obj['bbox']['xmax']), int(obj['bbox']['ymax'])) for obj in objects]
        bounding_boxes_conf = [(int(obj['bbox']['xmin']), int(obj['bbox']['ymin']), int(obj['bbox']['xmax']), int(obj['bbox']['ymax']), float(obj["confidence"])) for obj in objects]
        objects, input_boxes = self.centroid_tracker.update(bounding_boxes)
        return objects, bounding_boxes_conf

    def draw_boxes(self, frame, bounding_boxes):
        for bbox in bounding_boxes:
            x1, y1, x2, y2, conf = bbox
            titik_tengah_x = int((x1 + x2) / 2)
            titik_tengah_y = int((y1 + y2) / 2)
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 2)
        return frame

    def is_left(self, p1, p2, p):
        return (p2[0] - p1[0]) * (p[1] - p1[1]) - (p[0] - p1[0]) * (p2[1] - p1[1]) > 0

    def is_right(self, p1, p2, p):
        return not self.is_left(p1, p2, p)