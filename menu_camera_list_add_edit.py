import sys
import os
import subprocess
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QLineEdit,
    QMessageBox
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal
# from home import App_home
import sqlite3
from sidebar import App_sidebar

class App_camera_list_add_edit(QMainWindow):
    back_add_camera_list_clicked = pyqtSignal()
    save_add_camera_list_clicked = pyqtSignal()

    def __init__(self, id_data, type_camera):
        super().__init__()
        self.set_parameter(id_data, type_camera)

    def set_parameter(self, parameter1, parameter2):
        # Display the parameter        
        site_code = self.get_cosntant_by_code('site_code')
        self.sub_title = 'Add Camera'
        self.id_cam = ''
        self.nama_cam = ''
        self.code_cam = ''
        self.rtsp_cam = ''
        self.location_code = site_code
        self.type_cam = ''
        self.id_data = parameter1
        self.type_camera = parameter2
        if self.id_data != 0:
            camera = self.get_camera_by_id(parameter1, parameter2)
            # print(f'{self.id_data} -- {self.type_camera}')
            # print(f'{camera}')
            self.sub_title = 'Edit Camera'
            if self.type_camera == 1 or self.type_camera == 4:
                self.id_cam = camera[0]
                self.nama_cam = camera[1]
                self.code_cam = camera[9]
                self.rtsp_cam = camera[2]                
                self.type_cam = 1
            elif self.type_camera == 2:
                self.id_cam = camera[0]
                self.nama_cam = camera[1]
                self.code_cam = camera[3]
                self.rtsp_cam = camera[2]                
                self.type_cam = 2
            elif self.type_camera == 3: 
                self.id_cam = camera[0]
                self.nama_cam = camera[1]
                self.code_cam = camera[3]
                self.rtsp_cam = camera[2]                
                self.type_cam = 3

        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()  # Panggil update_layout untuk pengaturan awal
        self.menu2()


    def menu2(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)

        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )
        
        self.detail_notif_visible = False
        self.title_notif = QPushButton(self)
        self.title_notif.setGeometry(
            int(485 * width_ratio),
            int(112 * height_ratio),
            int(210 * width_ratio),
            int(35 * height_ratio),
        )
        self.title_notif.setStyleSheet(
            "background-color: rgba(0,0,0,0); border: 1px solid white; color: white; font-family: Inter; font-size: 12px; text-align: left; border-radius: 10px;"
        )
        self.title_notif.setText("  Please restart the application after adding the camera.")
        self.title_notif.setVisible(False)

        info_label1 = QLabel(self)
        info_icon = QPixmap("img/info.png")
        info_label1.setPixmap(info_icon.scaled(25, 25, Qt.KeepAspectRatio))   
        info_label1.setStyleSheet("background-color: rgba(0,0,0,0);")
        info_label1.setGeometry(
            int(455 * width_ratio),
            int(112 * height_ratio),
            int(90 * width_ratio),
            int(20 * height_ratio),
        )
        info_label1.setMouseTracking(True)
        # Connect the mouse press event of info_label1 to toggle_detail_rtsp method
        info_label1.mousePressEvent = self.toggle_info_add_notif

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(170 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(f" {self.sub_title} ")

        # Label dan input field untuk username
        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Location Code")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 250, 170, 40)
        
        self.input_location_code = QtWidgets.QLineEdit(self)
        self.input_location_code.setFont(QtGui.QFont("Inter", 16))
        self.input_location_code.setGeometry(600, 250, 700, 40)
        self.input_location_code.setText(self.location_code[2])
        self.input_location_code.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")
        self.input_location_code.setEnabled(False)

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Camera Type")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 350, 170, 40)
        
        self.input_cam_type = QtWidgets.QComboBox(self)
        self.input_cam_type.addItem("Visitor")
        self.input_cam_type.addItem("Gender")
        self.input_cam_type.addItem("Zone")
        self.input_cam_type.addItem("Outer Traffic")
        self.input_cam_type.setFont(QtGui.QFont("Inter", 16))
        self.input_cam_type.setGeometry(600, 350, 700, 40)
        self.input_cam_type.setStyleSheet(
            "QComboBox {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
            "QComboBox::drop-down:button{background-color: #5C6164;width: 40px;}"
            "QComboBox::down-arrow {image: url(img/arrrow.png);width: 40px;height: 36px ;padding: 5px;}"
            "QComboBox::item { color: white; }" # Mengatur warna teks item menjadi putih
        )

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Name")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 450, 170, 40)

        self.input_name = QtWidgets.QLineEdit(self)
        self.input_name.setFont(QtGui.QFont("Inter", 16))
        self.input_name.setGeometry(600, 450, 700, 40)
        self.input_name.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Code")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 550, 170, 40)

        self.input_code = QtWidgets.QLineEdit(self)
        self.input_code.setFont(QtGui.QFont("Inter", 16))
        self.input_code.setGeometry(600, 550, 700, 40)
        self.input_code.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")
        # self.input_location_code.setText(self.location_code[2])
        # self.input_location_code.setEnabled(False)        

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("RTSP")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 650, 170, 40)

        self.input_rtsp = QtWidgets.QLineEdit(self)
        self.input_rtsp.setFont(QtGui.QFont("Inter", 16))
        self.input_rtsp.setGeometry(600, 650, 700, 40)
        self.input_rtsp.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")
        
        info_label = QLabel(self)
        info_icon = QPixmap("img/info.png")
        info_label.setPixmap(info_icon.scaled(35, 35, Qt.KeepAspectRatio))   
        info_label.setStyleSheet("background-color: rgba(0,0,0,0);")
        info_label.setGeometry(
            int(880 * width_ratio),
            int(503 * height_ratio),
            int(100 * width_ratio),
            int(30 * height_ratio),
        )
        info_label.setMouseTracking(True)
        # Connect the mouse press event of info_label to toggle_detail_rtsp method
        info_label.mousePressEvent = self.toggle_detail_rtsp

        self.detail_rtsp_visible = False
        self.detail_rtsp = QtWidgets.QLabel(self)
        self.detail_rtsp.setFont(QtGui.QFont("Inter", 15))
        self.detail_rtsp.setText("RTSP (Real Time Streaming Protocol). you can get it by following your camera manual book. For Each brand has different standards. The final results of the RTSP that will be input is like : rtsp://username:password@ip_camera/stream1")
        self.detail_rtsp.setStyleSheet("color: white; background-color: rgba(0,0,0,0); font-size:12px")
        self.detail_rtsp.setGeometry(600, 670, 700, 100)
        self.detail_rtsp.setWordWrap(True)
        self.detail_rtsp.setVisible(False)

        if self.id_cam != '':
            self.input_cam_type.setCurrentIndex(int(self.type_camera) - 1)
            self.input_cam_type.setEnabled(False)
            self.input_name.setText(self.nama_cam)            
            self.input_code.setText(self.code_cam)            
            self.input_rtsp.setText(self.rtsp_cam)

        back_data_button = QPushButton("Back", self)
        back_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        back_data_button.setGeometry(
            int(1175 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        back_data_button.clicked.connect(self.back_button_clicked)

        add_data_button = QPushButton("Save", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1100 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        add_data_button.clicked.connect(self.save_button_clicked)

    def toggle_info_add_notif(self, event):
        # Toggle the visibility of detail_rtsp        
        self.detail_notif_visible = not self.detail_notif_visible
        self.title_notif.setVisible(self.detail_notif_visible)

    def toggle_detail_rtsp(self, event):
        # Toggle the visibility of detail_rtsp
        self.detail_rtsp_visible = not self.detail_rtsp_visible
        self.detail_rtsp.setVisible(self.detail_rtsp_visible)

    def get_cosntant_by_code(self, code):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT * FROM constant WHERE code="{code}"').fetchone()
        return data

    def get_camera_by_id(self, id_data, type_cam):
        table = ''        
        if type_cam == 1 or type_cam == 4:
            table = "setting_camera"
        elif type_cam == 2:
            table = 'setting_camera_gender'
        elif type_cam == 3: 
            table = 'setting_camera_area'

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"SELECT * FROM {table} WHERE id = {id_data}")
        data = cursor.fetchone()
        conn.commit()                
        conn.close()
        return data
    
    def save_camera(self, type_camera, nama, rtsp, kode):
        table = ''
        if type_camera == 1:
            table = "setting_camera"
            tampilan_monitoring = 2
        elif type_camera == 2:
            table = 'setting_camera_gender'
            tampilan_monitoring = 4
        elif type_camera == 3: 
            table = 'setting_camera_area'
            tampilan_monitoring = 1
        elif type_camera == 4: 
            table = 'setting_camera'
            tampilan_monitoring = 3

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        insert_query = f"INSERT INTO {table} (nama, path, device_code, jenis_tampilan, type_camera) VALUES (?, ?, ?, ?, ?)"
        cursor.execute(insert_query,(nama, rtsp, kode, tampilan_monitoring, type_camera))
        conn.commit()
        cursor.close()
        conn.close()
    
    def update_camera(self, type_camera, nama, rtsp, kode):
        table = ''
        if type_camera == 1:
            table = "setting_camera"
        elif type_camera == 2:
            table = 'setting_camera_gender'
        elif type_camera == 3: 
            table = 'setting_camera_area'
        elif type_camera == 4: 
            table = 'setting_camera'

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        update_query = f"UPDATE {table} SET nama = ?, path = ?, device_code = ? WHERE id = ?"
        cursor.execute(update_query, (nama, rtsp, kode, self.id_cam))
        conn.commit()
        cursor.close()
        conn.close()

    def back_button_clicked(self):        
        self.back_add_camera_list_clicked.emit()

    def save_button_clicked(self):
        # Membuat instance QMessageBox
        value_location_code = self.input_location_code.text()
        value_cam_type = self.input_cam_type.currentIndex()
        value_name = self.input_name.text()
        value_code = self.input_code.text()
        value_rtsp = self.input_rtsp.text()
        if value_name != '' and value_code != '' and value_rtsp != '':
            msg = QMessageBox()
            msg.setWindowTitle("Confirm Save Data")
            msg.setText("Please restart the application after adding all the cameras.")
            msg.setIcon(QMessageBox.Warning)
            msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            msg.setDefaultButton(QMessageBox.Cancel)

            # Menampilkan popup dan mendapatkan respons pengguna
            response = msg.exec_()

            # Jika pengguna menekan tombol Ok, maka lakukan aksi penghapusan data
            if response == QMessageBox.Ok:
                if self.id_cam == '':
                    # print(f"save camera")
                    self.save_camera(value_cam_type + 1, value_name, value_rtsp, value_code)
                else :
                    self.update_camera(value_cam_type + 1, value_name, value_rtsp, value_code)

                self.save_add_camera_list_clicked.emit()


# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_snapshot_edit("8")
#     window.show()
#     sys.exit(app.exec_())
