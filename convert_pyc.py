import py_compile
import os
import glob

files_to_compile = ['app_detection.py', 'app_sync.py', 'area.py', 'gender1.py', 'home.py', 'keluar_masuk.py', 'list_camera_monitoring1.py', 'list_camera1.py', 'loginfix.py', 'menu_camera_list_add_edit.py', 'menu_camera_list.py', 'menu_home.py', 'menu_monitoring_setup_edit.py', 'menu_monitoring_setup.py', 'menu_snapshot_edit.py', 'menu_snapshot.py', 'menu_zone.py', 'modal_snapshot.py', 'sidebar.py', 'switch.py', 'menu_record_camera.py', 'menu_record_camera_edit.py', "player_record.py"]

for file in files_to_compile:
    # Kompilasi file
    py_compile.compile(file)
    
    # Dapatkan nama file asli tanpa ekstensi
    base_name = os.path.splitext(os.path.basename(file))[0]
    
    # Temukan file .pyc yang dihasilkan dalam direktori __pycache__
    pyc_file_pattern = os.path.join('__pycache__', f'{base_name}.cpython-*.pyc')
    pyc_files = glob.glob(pyc_file_pattern)
    
    for pyc_file in pyc_files:
        # Buat nama file baru tanpa bagian .cpython-311
        new_pyc_file = os.path.join('__pycache__', f'{base_name}.pyc')
        
        # Ubah nama file
        os.rename(pyc_file, new_pyc_file)