import sys
import os
import subprocess
from PyQt5.QtWidgets import QApplication, QMainWindow, QStackedWidget, QMessageBox
from PyQt5.QtCore import Qt, QProcess, pyqtSignal, QTimer
from menu_home import App_home1
from menu_camera_list import App_home2
from menu_zone import App_home3
from menu_snapshot import App_home4
from menu_camera_list_add_edit import App_camera_list_add_edit
from menu_snapshot_edit import App_snapshot_edit
from menu_monitoring_setup import App_monitoring_setup
from menu_monitoring_setup_edit import App_monitoring_edit
from menu_record_camera import App_record_camera
from menu_record_camera_edit import App_record_camera_edit
from list_camera1 import MainWindow
from list_camera_monitoring1 import MainWindowMonitoring
from player_record import CameraLayout

class App_home(QMainWindow):
    closed = pyqtSignal()

    def __init__(self, path):
        super().__init__()
        self.setWindowTitle("Control Panel")
        self.setStyleSheet("background-color: #222222;")
        self.showFullScreen()
        
        self.stacked_widget = QStackedWidget(self)
        self.setCentralWidget(self.stacked_widget)

        self.path = path
        self.process = subprocess.Popen(["python", path])

        self.setup_ui()

    def setup_ui(self):
        self.menu_home = App_home1()
        self.menu_camera_list = App_home2()
        self.menu_add_camera_list = App_camera_list_add_edit(0, '')
        self.menu_zone_configuration = App_home3(0, '', '')
        self.menu_snapshot = App_home4()
        self.menu_edit_snapshot = App_snapshot_edit(0)
        self.menu_monitoring_setup = App_monitoring_setup()
        self.menu_monitoring_edit = App_monitoring_edit(0, 0)

        self.menu_record_camera = App_record_camera()
        self.menu_record_camera_edit = App_record_camera_edit(0)

        self.menu_preview_all_camera = MainWindow()
        self.menu_preview_monitoring_camera = MainWindowMonitoring()
        self.menu_preview_record_camera = CameraLayout()

        widgets = [
            self.menu_home, self.menu_camera_list, self.menu_add_camera_list,
            self.menu_zone_configuration, self.menu_snapshot, self.menu_edit_snapshot,
            self.menu_monitoring_setup, self.menu_monitoring_edit, self.menu_record_camera, self.menu_record_camera_edit,
            self.menu_preview_all_camera, self.menu_preview_monitoring_camera, self.menu_preview_record_camera
        ]

        for widget in widgets:
            self.stacked_widget.addWidget(widget)
        
        self.setup_signals()

    def setup_signals(self):
        menus = [
            self.menu_home, self.menu_camera_list, self.menu_zone_configuration,
            self.menu_snapshot, self.menu_monitoring_setup, self.menu_record_camera
        ]

        for menu in menus:
            menu.sidebar.close_window.connect(self.close_window)
            menu.sidebar.minimize_window.connect(self.minimize_window)
            menu.sidebar.home_clicked.connect(self.show_menu_home)
            menu.sidebar.menu_camera_list_clicked.connect(self.show_menu_camera_list)
            menu.sidebar.zone_configuration_clicked.connect(self.show_menu_zone_configuration)
            menu.sidebar.monitoring_view_setup_clicked.connect(self.show_monitoring_view_setup)
            menu.sidebar.snapshot_clicked.connect(self.show_menu_snapshot)
            menu.sidebar.record_camera_clicked.connect(self.show_record_camera)

            menu.sidebar.camera_preview_clicked.connect(self.show_camera_preview)
            menu.sidebar.monitoring_camera_preview_clicked.connect(self.show_monitoring_camera_preview)
        
        self.menu_camera_list.add_edit_camera_list_clicked.connect(self.show_menu_add_camera_list)
        self.menu_camera_list.draw_clicked.connect(self.show_menu_zone_configuration)
        self.menu_add_camera_list.back_add_camera_list_clicked.connect(self.show_menu_camera_list)
        self.menu_add_camera_list.save_add_camera_list_clicked.connect(self.show_menu_camera_list)
        self.menu_zone_configuration.back_zone_configuration_clicked.connect(self.show_menu_camera_list)
        self.menu_zone_configuration.save_zone_configuration_clicked.connect(self.show_menu_camera_list)
        self.menu_snapshot.edit_snapshot_clicked.connect(self.show_menu_edit_snapshot)
        self.menu_edit_snapshot.back_snapshot_clicked.connect(self.show_menu_snapshot)
        self.menu_edit_snapshot.save_snapshot_clicked.connect(self.show_menu_snapshot)
        self.menu_monitoring_setup.edit_monitoring_clicked.connect(self.show_menu_edit_monitoring)
        self.menu_monitoring_edit.back_monitoring_clicked.connect(self.show_monitoring_view_setup)
        self.menu_monitoring_edit.save_monitoring_clicked.connect(self.show_monitoring_view_setup)
        self.menu_record_camera.add_edit_record_clicked.connect(self.show_menu_edit_record_camera)
        self.menu_record_camera_edit.back_record_clicked.connect(self.show_record_camera)
        self.menu_record_camera_edit.save_record_clicked.connect(self.show_record_camera)

        self.menu_record_camera.preview_record_clicked.connect(self.show_preview_record_camera)
        self.menu_preview_record_camera.back_record_clicked.connect(self.show_record_camera)
        self.menu_preview_record_camera.minimize_clicked.connect(self.minimize_window)
        self.menu_preview_record_camera.close_clicked.connect(self.close_window)

        self.menu_preview_monitoring_camera.control_panel_clicked.connect(self.show_menu_home)
        self.menu_preview_monitoring_camera.minimize_clicked.connect(self.minimize_window)
        self.menu_preview_monitoring_camera.close_clicked.connect(self.close_window)

        self.menu_preview_all_camera.control_panel_clicked.connect(self.show_menu_home)
        self.menu_preview_all_camera.add_record_camera_clicked.connect(self.show_menu_edit_record_camera)
        self.menu_preview_all_camera.minimize_clicked.connect(self.minimize_window)
        self.menu_preview_all_camera.close_clicked.connect(self.close_window)

    def close_window(self):
        msg = QMessageBox()
        msg.setWindowTitle("Confirm close Window")
        msg.setText("Are you sure you want to close Window?")
        msg.setIcon(QMessageBox.Warning)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        response = msg.exec_()

        if response == QMessageBox.Ok:
            self.close()

    def minimize_window(self):
        self.showMinimized()

    def closeEvent(self, event):
        self.process.kill()
        self.closed.emit()
        event.accept()

    def show_menu_home(self):
        self.stacked_widget.setCurrentWidget(self.menu_home)

    def show_menu_camera_list(self):
        self.menu_camera_list.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_camera_list)

    def show_menu_add_camera_list(self, id_data, type_cam):
        self.menu_add_camera_list.set_parameter(id_data, type_cam)
        self.stacked_widget.setCurrentWidget(self.menu_add_camera_list)

    def show_menu_zone_configuration(self, id_data, type_cam, type_draw):
        self.menu_zone_configuration.update_content(id_data, type_cam, type_draw)
        self.stacked_widget.setCurrentWidget(self.menu_zone_configuration)

    def show_menu_snapshot(self):
        self.menu_snapshot.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_snapshot)

    def show_menu_edit_snapshot(self, id_data):
        self.menu_edit_snapshot.set_parameter(id_data)
        self.stacked_widget.setCurrentWidget(self.menu_edit_snapshot)

    def show_camera_preview(self):
        self.menu_preview_all_camera.update_layout()
        self.stacked_widget.setCurrentWidget(self.menu_preview_all_camera)
    def show_monitoring_camera_preview(self):
        self.menu_preview_monitoring_camera.update_layout()
        self.stacked_widget.setCurrentWidget(self.menu_preview_monitoring_camera)

    # def show_camera_preview(self):
    #     self._show_loading(self.open_camera_preview)
    # def open_camera_preview(self):
    #     self._open_external_window("list_camera1.pyc", "list_camera1", "MainWindow")
    # def show_monitoring_camera_preview(self):
    #     self._show_loading(self.open_monitoring)
    # def open_monitoring(self):
    #     self._open_external_window("list_camera_monitoring1.pyc", "list_camera_monitoring1", "MainWindowMonitoring")

    def show_monitoring_view_setup(self):
        self.menu_monitoring_setup.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_monitoring_setup)

    def show_menu_edit_monitoring(self, id_data, type_cam):
        self.menu_monitoring_edit.set_parameter(id_data, type_cam)
        self.stacked_widget.setCurrentWidget(self.menu_monitoring_edit)

    def show_record_camera(self):
        self.menu_record_camera.update_content()
        self.stacked_widget.setCurrentWidget(self.menu_record_camera)

    def show_preview_record_camera(self, id_data):
        self.menu_preview_record_camera.update_layout(id_data)
        self.stacked_widget.setCurrentWidget(self.menu_preview_record_camera)
    # def show_preview_record(self):
    #     self._show_loading(self.open_preview_record)
    # def open_preview_record(self):
    #     self._open_external_window("player.pyc", "player", "CameraLayout")

    def show_menu_edit_record_camera(self, id_data, id_cam, type_cam):
        self.menu_record_camera_edit.set_parameter(id_data, id_cam, type_cam)
        self.stacked_widget.setCurrentWidget(self.menu_record_camera_edit)
    
    def _show_loading(self, callback):
        current_widget = self.stacked_widget.currentWidget()
        if hasattr(current_widget, 'sidebar') and hasattr(current_widget.sidebar, 'show_loading'):
            current_widget.sidebar.show_loading()
            QTimer.singleShot(1, callback)
        else:
            print("Current widget does not have sidebar or show_loading method.")

    # def _open_external_window(self, script_name, module_name, class_name):
    #     current_directory = os.path.dirname(os.path.abspath(__file__))
    #     panel_path = os.path.join(current_directory, script_name)
    #     module = __import__(module_name)
    #     cls = getattr(module, class_name)
    #     window = cls(panel_path)
    #     window.show()
    #     window.closed.connect(self.close)
    #     self.close()

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_home('')
#     window.show()
#     sys.exit(app.exec_())
