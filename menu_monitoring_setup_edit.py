import sys
import os
import subprocess
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QLineEdit
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal

import sqlite3
from sidebar import App_sidebar

class App_monitoring_edit(QMainWindow):
    back_monitoring_clicked = pyqtSignal()
    save_monitoring_clicked = pyqtSignal()

    def __init__(self, id_data, type_cam):
        super().__init__()
        # self.set_parameter(id_data)

    def set_parameter(self, id_data, type_cam):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        self.id_data = id_data
        if type_cam == 1 or type_cam == 4:
            self.table = "setting_camera"
        elif type_cam == 2:
            self.table = 'setting_camera_gender'
        elif type_cam == 3: 
            self.table = 'setting_camera_area'
        cursor.execute(f"SELECT nama, add_on FROM {self.table} WHERE id = {self.id_data}")
        data = cursor.fetchone()
        conn.commit()                
        conn.close()
        self.datas = data
        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()
        self.update_form()  # Panggil update_form untuk pengaturan awal


    def update_form(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        # Main Content Area
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(250 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" Edit Add On ")

        # Label dan input field untuk username
        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Name")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 240, 170, 36)
        
        self.filename = QtWidgets.QLineEdit(self)
        self.filename.setFont(QtGui.QFont("Inter", 16))
        self.filename.setGeometry(600, 240, 500, 36)
        self.filename.setText(self.datas[0])
        self.filename.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);")
        self.filename.setEnabled(False)

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Add On")
        self.label3.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label3.setGeometry(450, 330, 170, 36)

        self.deskripsi = QtWidgets.QTextEdit(self)
        self.deskripsi.setFont(QtGui.QFont("Inter", 16))
        self.deskripsi.setGeometry(600, 330, 500, 190)
        self.deskripsi.setText(self.datas[1])
        self.deskripsi.setStyleSheet(
            "border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);"
            "padding: 5px;"  # Menambahkan padding agar teks terlihat lebih baik
        )

        add_data_button = QPushButton("Back", self)
        add_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1175 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        add_data_button.clicked.connect(self.back_button_clicked)

        add_data_button = QPushButton("Save", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1100 * width_ratio),
            int(730 * height_ratio),
            int(60 * width_ratio),
            int(36 * height_ratio),
        )
        add_data_button.clicked.connect(self.save_button_clicked)

    def back_button_clicked(self):
        self.back_monitoring_clicked.emit()        

    def save_button_clicked(self):
        # print(f"save snapshot {self.datas[0]}")
        deskripsi = self.deskripsi.toPlainText()
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"UPDATE {self.table} SET add_on = '{str(deskripsi)}' where id = {self.id_data}")
        conn.commit()
        cursor.close()
        conn.close()

        self.save_monitoring_clicked.emit()

        

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_snapshot_edit("8")
#     window.show()
#     sys.exit(app.exec_())
