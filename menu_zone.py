import sys
import os
import subprocess
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor, QPainter, QPen, QImage
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal, QTimer, QPoint
from sidebar import App_sidebar
import cv2
import numpy as np
import sqlite3
import random

class DrawingLabel(QLabel):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.points = []
        self.lines = []
        self.frame = None
        self.rois =  [] 
        self.labels =  []
        self.colors = []
        self.current_roi_index = 0

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            pos = event.pos()
            if self.rect().contains(pos):
                if self.type_draw_event == "Border":
                    pos = self.mapFromGlobal(event.globalPos())
                    self.points.append(pos)
                    self.update()
                elif self.type_draw_event == "Line":
                    pos = self.mapFromGlobal(event.globalPos())
                    self.lines.append(pos)
                    self.update()
                elif self.type_draw_event == "Area":
                    pos = self.mapFromGlobal(event.globalPos())
                    self.rois[self.current_roi_index].append(pos)
                    self.update()

    def paintEvent(self, event):
        super().paintEvent(event)
        
        label_width = self.width()
        label_height = self.height()
        if self.frame is not None:
            if self.type_draw_event == "Border" or self.type_draw_event == "Line":
                self.draw_border_line(label_width, label_height)
            elif self.type_draw_event == "Area":
                self.draw_area(label_width, label_height)

    def draw_area(self, label_width, label_height):
        image_width = self.frame.shape[1]
        image_height = self.frame.shape[0]
        self.resolution_wh = (image_width, image_height)
        width_ratio = image_width / label_width
        height_ratio = image_height / label_height

        frame_copy = self.frame.copy()  # Make a copy of the frame
        # print(self.colors)
        index = 0
        for roi, label in zip(self.rois, self.labels):
            if len(roi) > 3:
                # If more than 3 points, draw polyline
                poly_points = [(int(point.x() * width_ratio), int(point.y() * height_ratio)) for point in roi]
                poly_points = np.array(poly_points, dtype=np.int32)
                cv2.polylines(frame_copy, [poly_points], True, self.colors[index], thickness=2)
                # Determine text input box position based on ROI
                min_x = np.min(poly_points[:, 0])
                min_y = np.min(poly_points[:, 1])
                input_box_position = (min_x + 10, min_y + 10)
                input_box_size = (180, 30)
                cv2.putText(frame_copy, self.labels[index][0], (input_box_position[0] + 5, input_box_position[1] + input_box_size[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

            else:
                # If less than or equal to 3 points, draw points
                for point in roi:
                    cv2.circle(frame_copy, (int(point.x() * width_ratio), int(point.y() * height_ratio)), 5, (0, 0, 255), -1)
            index += 1


        # Convert frame to QImage and draw it on the label
        h, w, ch = frame_copy.shape
        bytes_per_line = ch * w
        qImg = QImage(frame_copy.data, w, h, bytes_per_line, QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(qImg)
        painter = QPainter(self)
        painter.drawPixmap(self.rect(), pixmap)


    def draw_border_line(self, label_width, label_height):
        image_width = self.frame.shape[1]
        image_height = self.frame.shape[0]
        width_ratio = image_width / label_width
        height_ratio = image_height / label_height

        frame_copy = self.frame.copy()  # Make a copy of the frame

        # Draw lines
        if len(self.lines) == 2:
            # If more than 1 line, draw polyline
            poly_lines = [(int(line.x() * width_ratio), int(line.y() * height_ratio)) for line in self.lines]
            poly_lines = np.array(poly_lines, dtype=np.int32)
            for i in range(len(poly_lines) - 1):
                # Draw line between two points
                cv2.line(frame_copy, tuple(poly_lines[i]), tuple(poly_lines[i + 1]), color=(255, 0, 0), thickness=2)
            # Draw the last line (between the last point and the first point)
            cv2.line(frame_copy, tuple(poly_lines[-1]), tuple(poly_lines[0]), color=(0, 0, 0), thickness=2)
        else:
            for line in self.lines:
                cv2.circle(frame_copy, (int(line.x() * width_ratio), int(line.y() * height_ratio)), 5, (0, 0, 255), -1)

        # Draw points
        if len(self.points) > 3:
            # If more than 3 points, draw polyline
            poly_points = [(int(point.x() * width_ratio), int(point.y() * height_ratio)) for point in self.points]
            poly_points = np.array(poly_points, dtype=np.int32)
            cv2.polylines(frame_copy, [poly_points], True, color=(255, 0, 0), thickness=2)
        else:
            # If less than or equal to 3 points, draw points
            for point in self.points:
                cv2.circle(frame_copy, (int(point.x() * width_ratio), int(point.y() * height_ratio)), 5, (0, 0, 255), -1)

        # Convert frame to QImage and draw it on the label
        h, w, ch = frame_copy.shape
        bytes_per_line = ch * w
        qImg = QImage(frame_copy.data, w, h, bytes_per_line, QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(qImg)
        painter = QPainter(self)
        painter.drawPixmap(self.rect(), pixmap)

    def update_frame(self, frame, table, id_data_cam, type_draw_event):
        self.frame = frame
        self.table_data = table
        self.id_data_cam = id_data_cam
        self.type_draw_event = type_draw_event
        if self.frame is not None:
            h, w, ch = self.frame.shape            
            bytes_per_line = ch * w
            qImg = QImage(self.frame.data, w, h, bytes_per_line, QImage.Format_RGB888)
            pixmap = QPixmap.fromImage(qImg)
            self.setPixmap(pixmap)
    
    def generate_random_colors(self, num_colors):
        return [(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)) for _ in range(num_colors)]
    
    def save_to_sqlite(self):
        poly_points = [point for point in self.points]
        poly_points_tes = [(int(point.x()), int(point.y())) for point in poly_points]
        poly_points_tes = np.array(poly_points_tes, dtype=np.int32)
        poly_points_tes = [(point[0], point[1]) for point in poly_points_tes.tolist()]

        poly_lines = [line for line in self.lines]
        poly_lines_tes = [(int(line.x()), int(line.y())) for line in poly_lines]
        poly_lines_tes = np.array(poly_lines_tes, dtype=np.int32)
        poly_lines_tes = [(line[0], line[1]) for line in poly_lines_tes.tolist()]

        label_width = self.width()
        label_height = self.height()
        image_width = self.frame.shape[1]
        image_height = self.frame.shape[0]
        width_ratio = image_width / label_width
        height_ratio = image_height / label_height
        # print(poly_points_tes)        
        # # Connect to SQLite database
        # print(self.table_data)
        # # print(poly_points)
        # print(self.id_data_cam)
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f'UPDATE {self.table_data} SET garis="{poly_lines_tes}" WHERE id="{self.id_data_cam}"')
        cursor.execute(f'UPDATE {self.table_data} SET points="{poly_points_tes}" WHERE id="{self.id_data_cam}"')
        cursor.execute(f'UPDATE {self.table_data} SET rasio_width="{width_ratio}" WHERE id="{self.id_data_cam}"')
        cursor.execute(f'UPDATE {self.table_data} SET rasio_height="{height_ratio}" WHERE id="{self.id_data_cam}"')
        # Commit changes and close connection
        conn.commit()
        conn.close()

    def save_area_to_sqlite(self):
        area = [[(point.x(), point.y()) for point in sublist] for sublist in self.rois]
        label_area = self.labels

        label_width = self.width()
        label_height = self.height()
        image_width = self.frame.shape[1]
        image_height = self.frame.shape[0]
        width_ratio = image_width / label_width
        height_ratio = image_height / label_height
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f'UPDATE {self.table_data} SET resolution_wh="{self.resolution_wh}" WHERE id="{self.id_data_cam}"')
        cursor.execute(f'UPDATE {self.table_data} SET area="{area}" WHERE id="{self.id_data_cam}"')
        cursor.execute(f'UPDATE {self.table_data} SET label_area="{label_area}" WHERE id="{self.id_data_cam}"')
        cursor.execute(f'UPDATE {self.table_data} SET rasio_width="{width_ratio}" WHERE id="{self.id_data_cam}"')
        cursor.execute(f'UPDATE {self.table_data} SET rasio_height="{height_ratio}" WHERE id="{self.id_data_cam}"')
        # Commit changes and close connection
        conn.commit()
        conn.close()


class App_home3(QMainWindow):
    back_zone_configuration_clicked = pyqtSignal()
    save_zone_configuration_clicked = pyqtSignal()
    def __init__(self, id_data, type_cam, type_draw):
        super().__init__()
        self.sidebar = App_sidebar()  # Create an instance of App_sidebar
        self.sidebar.update_layout()  # Call update_layout for initial setup        

    def update_content(self, id_data, type_cam, type_draw):
        self.id_data = id_data
        self.table = ''
        if type_cam == 1 or type_cam == 4: 
            self.table = "setting_camera"
        elif type_cam == 2: 
            self.table = "setting_camera_gender"
        elif type_cam == 3: 
            self.table = "setting_camera_area"
        self.type_draw = type_draw
        self.menu3()
        
    def menu3(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.sidebar)
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)

        # Main Content Area
        screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = screen_geometry.width() / 1280
        self.height_ratio = screen_geometry.height() / 832

        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * self.width_ratio),
            int(90 * self.height_ratio),
            int(973 * self.width_ratio),
            int(699 * self.height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * self.width_ratio),
            int(105 * self.height_ratio),
            int(950 * self.width_ratio),
            int(43 * self.height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )

        if self.id_data == 0:
            self.GButton_12.setText(" Select the camera first in the camera list menu ")
        else:
            data_camera_name = self.get_data_camera(self.table, "nama", self.id_data)
            self.GButton_12.setText(f" Draw {self.type_draw} {data_camera_name[0]}")
            
            self.label8 = DrawingLabel(self)
            self.label8.setGeometry(
                int(300 * self.width_ratio),
                int(170 * self.height_ratio),
                int(930 * self.width_ratio),
                int(550 * self.height_ratio)
            )
            self.label8.setScaledContents(True)
            # set border value
            if self.type_draw =="Border" or self.type_draw == "Line":
                self.tool_border_line()
            elif self.type_draw == "Area":
                self.tool_area()

    def tool_area(self):
        data_areas = self.get_data_camera(self.table, "area", self.id_data)
        data_labels = self.get_data_camera(self.table, "label_area", self.id_data)
        data_area = eval(data_areas[0]) if data_areas[0] else []
        data_label = eval(data_labels[0]) if data_labels[0] else []

        back_data_button = QPushButton("Back", self)
        back_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        back_data_button.setGeometry(
            int(1175 * self.width_ratio),
            int(740 * self.height_ratio),
            int(60 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        back_data_button.clicked.connect(self.back_button_clicked)

        save_data_button = QPushButton("Save", self)
        save_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        save_data_button.setGeometry(
            int(1100 * self.width_ratio),
            int(740 * self.height_ratio),
            int(60 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        save_data_button.clicked.connect(self.save_button_clicked)

        # button reset border and line
        delete_data_button = QPushButton(f"Delete {self.type_draw}", self)
        delete_data_button.setStyleSheet(
            """
            background-color: #d10078;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        delete_data_button.setGeometry(
            int(1005 * self.width_ratio),
            int(740 * self.height_ratio),
            int(80 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        delete_data_button.clicked.connect(self.delete_data)

        add_data_button = QPushButton(f"Add {self.type_draw}", self)
        add_data_button.setStyleSheet(
            """
            background-color: #d1c100;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(910 * self.width_ratio),
            int(740 * self.height_ratio),
            int(80 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        add_data_button.clicked.connect(self.add_data)
        
        reset_data_button = QPushButton(f"Reset {self.type_draw}", self)
        reset_data_button.setStyleSheet(
            """
            background-color: #00d159;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        reset_data_button.setGeometry(
            int(815 * self.width_ratio),
            int(740 * self.height_ratio),
            int(80 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        reset_data_button.clicked.connect(self.reset_data)

        self.label_area = QtWidgets.QLabel(self)
        self.label_area.setFont(QtGui.QFont("Inter", 15))
        self.label_area.setText("Area")
        self.label_area.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label_area.setGeometry(
            int(300 * self.width_ratio),
            int(725 * self.height_ratio),
            int(200 * self.width_ratio),
            int(20 * self.height_ratio),)
        self.input_list_area = QtWidgets.QComboBox(self)        
        for label in data_label:
            self.input_list_area.addItem(label[0])
        self.input_list_area.setFont(QtGui.QFont("Inter", 12))
        self.input_list_area.setGeometry(
            int(300 * self.width_ratio),
            int(750 * self.height_ratio),
            int(200 * self.width_ratio),
            int(30 * self.height_ratio),)
        self.input_list_area.setStyleSheet(
            "QComboBox {border: 3px solid white;border-radius: 5px;padding: 1px 18px 1px 3px;min-width: 6em;color: white;background-color: rgba(0,0,0,0);}"
            "QComboBox::drop-down:button{background-color: #5C6164;width: 40px;}"
            "QComboBox::down-arrow {image: url(img/arrrow.png);width: 30px;height: 36px ;padding: 5px;}"
            "QComboBox::item { color: white; }" # Mengatur warna teks item menjadi putih
        )
        self.input_list_area.currentTextChanged.connect(self.update_input_name_area)

        self.label_name_area = QtWidgets.QLabel(self)
        self.label_name_area.setFont(QtGui.QFont("Inter", 15))
        self.label_name_area.setText("Label")
        self.label_name_area.setStyleSheet("color: white; background-color: rgba(0,0,0,0);")
        self.label_name_area.setGeometry(
            int(525 * self.width_ratio),
            int(725 * self.height_ratio),
            int(200 * self.width_ratio),
            int(20 * self.height_ratio),)
        self.input_name_area = QtWidgets.QLineEdit(self)
        self.input_name_area.setFont(QtGui.QFont("Inter", 12))
        self.input_name_area.setGeometry(
            int(525 * self.width_ratio),
            int(750 * self.height_ratio),
            int(200 * self.width_ratio),
            int(30 * self.height_ratio),)
        self.input_name_area.setText(self.input_list_area.currentText())
        self.input_name_area.setStyleSheet("border: 3px solid white; border-radius: 5px; color: white; background-color: rgba(0,0,0,0);padding: 1px 18px 1px 3px;")
        self.input_name_area.textChanged.connect(self.update_input_list_area)

        self.label8.rois = [[QPoint(x, y) for x, y in sublist] for sublist in data_area]
        self.label8.labels = data_label
        self.label8.colors = self.label8.generate_random_colors(len(self.label8.rois))

        data_rtsp = self.get_data_camera(self.table, "path", self.id_data)
        self.cap = cv2.VideoCapture(data_rtsp[0])
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(30)
    
    def update_input_list_area(self, text):
        # Setel nilai saat ini di QComboBox ke nilai baru yang diubah di QLineEdit
        index = self.label8.current_roi_index
        self.label8.labels[index][0] = text
        self.input_list_area.setItemText(index, text)

    def update_input_name_area(self, text):
        current_index = self.input_list_area.currentIndex()
        self.label8.current_roi_index = current_index
        current_text = self.input_list_area.currentText()
        self.input_name_area.setText(text)

    def tool_border_line(self):
        back_data_button = QPushButton("Back", self)
        back_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        back_data_button.setGeometry(
            int(1175 * self.width_ratio),
            int(730 * self.height_ratio),
            int(60 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        back_data_button.clicked.connect(self.back_button_clicked)

        add_data_button = QPushButton("Save", self)
        add_data_button.setStyleSheet(
            """
            background-color: #1e9fff;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1100 * self.width_ratio),
            int(730 * self.height_ratio),
            int(60 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        add_data_button.clicked.connect(self.save_button_clicked)

        # button reset border and line
        add_data_button = QPushButton(f"Reset {self.type_draw}", self)
        add_data_button.setStyleSheet(
            """
            background-color: #00d159;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1005 * self.width_ratio),
            int(730 * self.height_ratio),
            int(80 * self.width_ratio),
            int(36 * self.height_ratio),
        )

        add_data_button.clicked.connect(self.reset_data)
        data_border = self.get_data_camera(self.table, "points", self.id_data)
        data_line = self.get_data_camera(self.table, "garis", self.id_data)
        # print(data_border)
        # print(data_line)
        data_border_tuples = eval(data_border[0]) if data_border[0] else []                
        data_border_points = [(int(x), int(y)) for x, y in data_border_tuples]
        self.label8.points = [QPoint(x, y) for x, y in data_border_points]

        data_line_tuples = eval(data_line[0]) if data_line[0] else []                
        data_line_points = [(int(x), int(y)) for x, y in data_line_tuples]
        self.label8.lines = [QPoint(x, y) for x, y in data_line_points]
            
        data_rtsp = self.get_data_camera(self.table, "path", self.id_data)
        self.cap = cv2.VideoCapture(data_rtsp[0])
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(30)

    def add_data(self):
        new_color = self.label8.generate_random_colors(1)
        self.label8.colors.append(new_color[0]) 
        self.label8.rois.append([])
        self.label8.labels.append(['New Area'])
        self.input_list_area.addItem('New Area')
        # print(self.label8.rois)
        # print(self.label8.labels)
        # print(self.label8.colors)

    def delete_data(self):
        current_index = self.input_list_area.currentIndex()
        del self.label8.colors[current_index]        
        del self.label8.rois[current_index]        
        del self.label8.labels[current_index]
        self.input_list_area.removeItem(current_index)        

    def reset_data(self):
        if self.type_draw == 'Border':
            self.label8.points = []
        elif self.type_draw == 'Line':
            self.label8.lines = []
        elif self.type_draw == 'Area':
            index = self.label8.current_roi_index
            self.label8.rois[index] = []

    def update_frame(self):
        ret, frame = self.cap.read()
        if ret:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            self.label8.update_frame(frame, self.table, self.id_data, self.type_draw)

    def closeEvent(self, event):
        self.cap.release()
        self.timer.stop()
    
    def get_data_camera(self, table, column, id_data):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        data = cursor.execute(f'SELECT {column} FROM {table} where id={id_data}').fetchone()
        return data
    
    def back_button_clicked(self):        
        self.back_zone_configuration_clicked.emit()
        self.release_rtsp()

    def save_button_clicked(self):
        if self.type_draw == "Border" or self.type_draw == "Line":
            self.label8.save_to_sqlite()
        elif self.type_draw == "Area":
            self.label8.save_area_to_sqlite()
        self.save_zone_configuration_clicked.emit()
        self.release_rtsp()

    def release_rtsp(self):
        self.cap.release()
        self.timer.stop()
# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_home()
#     window.show()
#     sys.exit(app.exec_())
