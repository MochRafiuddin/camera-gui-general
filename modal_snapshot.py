import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton, QDialog, QLabel, QTextEdit
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt
import time
import sqlite3

class PopupForm(QDialog):
    def __init__(self, filename, camera_type):
        super().__init__()
        self.filename = filename
        self.camera_type = camera_type
        self.setStyleSheet("background-color: #F2F2F2; margin: 0; padding: 0;")  # Atur warna latar belakang popup dan hilangkan padding
        self.setFixedSize(684, 325)  # Tetapkan ukuran tetap dialog

        # Nonaktifkan judul bawaan
        self.setWindowFlags(QtCore.Qt.CustomizeWindowHint)

        # Tambahkan header kustom
        self.header = QtWidgets.QWidget(self)
        self.header.setObjectName("customHeader")
        self.header_layout = QtWidgets.QHBoxLayout(self.header)
        self.header_layout.setContentsMargins(0, 0, 0, 0)
        self.header.setStyleSheet("background-color: #333333; color: #FFFFFF; padding: 5px;")  # Atur warna latar belakang dan teks header

        # Tambahkan label judul
        self.title_label = QtWidgets.QLabel('Add Snapshot', self.header)
        self.title_label.setStyleSheet("font-size: 20px; font-weight: bold;")

        # Tambahkan tombol close
        self.close_button = QtWidgets.QPushButton('✕', self.header)
        self.close_button.setStyleSheet("font-size: 18px; background-color: transparent; border: none; color: #FFFFFF;")
        self.close_button.clicked.connect(self.close)

        # Tambahkan judul dan tombol close ke dalam layout header
        self.header_layout.addWidget(self.title_label)
        self.header_layout.addStretch(1)
        self.header_layout.addWidget(self.close_button)

        # Tambahkan layout header ke dalam layout utama
        layout = QVBoxLayout()
        layout.addWidget(self.header)

        self.label3 = QtWidgets.QLabel(self)
        self.label3.setFont(QtGui.QFont("Inter", 15))
        self.label3.setText("Deskripsi")
        self.label3.setStyleSheet("color: #333333;; background-color: rgba(0,0,0,0); margin-top: 10px;")

        self.deskripsi = QtWidgets.QTextEdit(self)
        self.deskripsi.setFont(QtGui.QFont("Inter", 16))
        self.deskripsi.setStyleSheet(
            "border: 3px solid #333333;; border-radius: 5px; color: #333333;; background-color: rgba(0,0,0,0);"
            "padding: 5px;"  # Menambahkan padding agar teks terlihat lebih baik
            "margin-bottom: 15px;"  # Menambahkan padding agar teks terlihat lebih baik
        )

        layout.addWidget(self.label3)
        layout.addWidget(self.deskripsi)

        button = QPushButton(self)
        button.setStyleSheet(
            "background-color: #333333; color: white; font-family: Inter; font-size: 20px; text-align: center; border-radius: 10px; font-weight: 1000; padding: 3px"
        )
        button.setText(" Submit ")
        button.clicked.connect(self.submit_form)
        layout.addWidget(button)
        button = QPushButton(self)
        button.setStyleSheet(
            "background-color: #333333; color: white; font-family: Inter; font-size: 20px; text-align: center; border-radius: 10px; font-weight: 1000; padding: 3px"
        )
        button.setText(" back ")
        button.clicked.connect(self.close_form)
        layout.addWidget(button)

        self.setLayout(layout)

    def submit_form(self):
        text = self.deskripsi.toPlainText()

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        waktu = time.time()
        created_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(waktu))
        insert_query = "INSERT INTO t_snapshot (file, camera_type, created_at, deskripsi) VALUES (?, ?, ?, ?)"
        cursor.execute(insert_query, (self.filename, self.camera_type, created_at, text))
        conn.commit()        
        cursor.close()
        conn.close()

        self.close()

    def close_form(self):
        self.close()

# class MyApp(QWidget):
#     def __init__(self):
#         super().__init__()

#         self.initUI()

#     def initUI(self):
#         self.setGeometry(300, 300, 300, 200)
#         self.setWindowTitle('Popup Example')

#         layout = QVBoxLayout()
#         button = QPushButton('Show Popup', self)
#         button.clicked.connect(self.show_popup)
#         layout.addWidget(button)

#         self.setLayout(layout)

#     def show_popup(self):
#         popup = PopupForm()
#         popup.exec_()

# if __name__ == '__main__':
#     app = QApplication(sys.argv)
#     ex = MyApp()
#     ex.show()
#     sys.exit(app.exec_())
