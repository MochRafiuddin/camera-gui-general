# Check if the terminal application is running
$isRunning = Get-Process | Where-Object {$_.ProcessName -eq "cmd"}
$tanggal = Get-Date -Format "yyyy-MM-dd"
$pathfile = "C:\Users\USER\Documents\aplikasi\camera-gui-general\monitoring\monitoring-$tanggal.txt"
# $batchFilePath = "C:\Users\USER\Documents\aplikasi\camera-gui-general\Camfig-Pro.bat"  # Update this path to your batch file location
$batchFilePathDetection = "C:\Users\USER\Documents\aplikasi\camera-gui-general\Camfig-Pro.bat"  # Update this path to your batch file location
$batchFilePathKill = "C:\Users\USER\Documents\aplikasi\camera-gui-general\kill_detection.bat"  # Update this path to your batch file location
$currentHour = (Get-Date).Hour
# If the terminal application is running
if ($isRunning) {
    # Get list of terminal processes
    $terminalProcesses = Get-Process | Where-Object {$_.ProcessName -eq "cmd"}

    # Prepare the list of terminal processes
    $terminalList = "List of terminal processes:"
    $nocmd = 0
    $terminalProcesses | ForEach-Object {
        $nocmd++
        $terminalList += "`nProcess Name: $($_.ProcessName) | ID: $($_.Id) | Window Title: $($_.MainWindowTitle)"
    }
    Write-Host $terminalList

    if ($nocmd -lt 4) {
        # Execute if $nocmd is less than 4
        Start-Process -FilePath $batchFilePathKill
        Start-Sleep -Seconds 3
        Start-Process -FilePath $batchFilePathDetection
    }
    elseif ($nocmd -ge 6) {
        # Execute if $nocmd is 4 or more
        Start-Process -FilePath $batchFilePathDetection
        Start-Sleep -Seconds 3
        Start-Process -FilePath $batchFilePathKill
    }
}
else {
    $terminalList = "cmd: Terminal application is not running."
    if ($currentHour -ge 8 -and $currentHour -le 22) {
        Start-Process -FilePath $batchFilePathDetection
    }
}

# Memeriksa apakah ada proses Python yang berjalan
$isPythonRunning = Get-Process | Where-Object {$_.ProcessName -like "python*"}

# Jika ada proses Python yang berjalan
if ($isPythonRunning) {
    $PythonRunning = "Python process is running."
} else {
    $PythonRunning = "No Python process is running."
}

    # CPU Usage
    $cpuUsage = Get-WmiObject win32_processor | Measure-Object -Property LoadPercentage -Average | Select-Object -ExpandProperty Average

    # RAM Usage
    $ramInfo = Get-WmiObject win32_operatingsystem | Select-Object FreePhysicalMemory,TotalVisibleMemorySize
    $ramUsage = "Free Physical Memory: $($ramInfo.FreePhysicalMemory) KB`nTotal Visible Memory: $($ramInfo.TotalVisibleMemorySize) KB"

    # GPU Usage (NVIDIA only)
    $gpuUsage = & 'C:\Windows\System32\nvidia-smi.exe'

    # Get Current DateTime
    $dateTime = Get-Date -Format "yyyy-MM-dd HH:mm:ss"


    # Combine all information
    $output = "`n===================$dateTime========================`nCPU Load Percentage: $cpuUsage%`n$ramUsage`nGPU Usage:`n$gpuUsage`n$terminalList`nPython File :$PythonRunning`n===========================END==================="

# Check if the system_usage.txt file exists
if (-not (Test-Path $pathfile)) {
    # Create the system_usage.txt file if it doesn't exist
    New-Item -Path $pathfile -ItemType File -Force
}

# Append to file
Add-Content -Path $pathfile -Value $output