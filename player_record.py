import sys
import cv2
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QFrame, QVBoxLayout, QHBoxLayout, QPushButton, QSlider
from PyQt5.QtGui import QPixmap, QFont, QImage
from PyQt5.QtCore import Qt, QTimer, pyqtSignal
import sqlite3

class CameraLayout(QMainWindow):
    back_record_clicked = pyqtSignal()
    save_record_clicked = pyqtSignal()
    minimize_clicked = pyqtSignal()
    close_clicked = pyqtSignal()

    def __init__(self):
        super().__init__()
        # self.update_layout(0)
        self.updating_position = False  # Flag to control slider updates
        # self.setWindowTitle("Control Panel")
        # self.setStyleSheet("background-color: #222222;")
        # self.showFullScreen()
    def top_bar(self):
        # top bar
        top_bar = QLabel(self)
        top_bar.setStyleSheet("background-color: #000000;")
        top_bar.setGeometry(0, 0, self.screen_geometry.width(), int(63 * self.height_ratio))

        self.icon1_label = QLabel(self)
        self.icon1_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon1_label.setGeometry(
            int(1206 * self.width_ratio),
            int(17 * self.height_ratio),
            int(24 * self.width_ratio),
            int(24 * self.height_ratio),
        )
        self.icon1_label.setPixmap(
            QPixmap("img/min.png").scaled(20, 20)
        ) 
        self.icon1_label.mousePressEvent = self.minimize_window

        self.icon2_label = QLabel(self)
        self.icon2_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon2_label.setGeometry(
            int(1232 * self.width_ratio),
            int(15 * self.height_ratio),
            int(33 * self.width_ratio),
            int(33 * self.height_ratio),
        )
        self.icon2_label.setPixmap(
            QPixmap("img/kotak.png").scaled(20, 20)
        ) 

        self.icon3_label = QLabel(self)
        self.icon3_label.setStyleSheet("color: white; background-color: #000000;")
        self.icon3_label.setGeometry(
            int(1252 * self.width_ratio),
            int(9 * self.height_ratio),
            int(45 * self.width_ratio),
            int(45 * self.height_ratio),
        )
        self.icon3_label.setPixmap(
            QPixmap("img/x.png").scaled(20, 20)
        )  
        self.icon3_label.mousePressEvent = self.on_close_button_clicked

        control_panel_label = QLabel("Preview Record Camera", self)
        control_panel_label.setStyleSheet("color: white; background-color: #000000;")
        control_panel_label.setFont(
            QFont("Times", int(23 * min(self.width_ratio, self.height_ratio)))
        )
        control_panel_label.setGeometry(
            int(30 * self.width_ratio),
            int(10 * self.height_ratio),
            int(280 * self.width_ratio),
            int(45 * self.height_ratio),
        )

    def update_layout(self, id_data): 
        self.id_data = id_data
        self.record_datas = self.get_data_record_by_id()
        self.screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = self.screen_geometry.width() / 1280
        self.height_ratio = self.screen_geometry.height() / 832
        self.top_bar()

        if self.record_datas[1] == 1:
            text_in = "System Count In"
            nilai_in = self.record_datas[8]
            text_out = "System Count Out"
            nilai_out = self.record_datas[9]
        elif self.record_datas[1] == 4:
            text_in = "System Count Left"
            nilai_in = self.record_datas[10]
            text_out = "System Count Right"
            nilai_out = self.record_datas[11]
        
        # Video display
        self.display_video = QLabel(self)
        self.display_video.setStyleSheet("background-color: #3d3d3d")
        self.display_video.setGeometry(
            int(20 * self.width_ratio),
            int(90 * self.height_ratio),
            int(970 * self.width_ratio),
            int(680 * self.height_ratio),
        )

        label_in = QLabel(text_in, self)
        label_in.setStyleSheet("color: white;")
        label_in.setFont(QFont("Times", 18))
        label_in.setAlignment(Qt.AlignCenter)
        label_in.setGeometry(
            int(1080 * self.width_ratio),
            int(100 * self.height_ratio),
            int(140 * self.width_ratio),
            int(39 * self.height_ratio),
        )
        self.value_in = QLabel(str(nilai_in), self)
        self.value_in.setStyleSheet("color: white;")
        self.value_in.setFont(QFont("Times", 18))
        self.value_in.setAlignment(Qt.AlignCenter)
        self.value_in.setGeometry(
            int(1080 * self.width_ratio),
            int(135 * self.height_ratio),
            int(140 * self.width_ratio),
            int(39 * self.height_ratio),
        )
        
        label_out = QLabel(text_out, self)
        label_out.setStyleSheet("color: white;")
        label_out.setFont(QFont("Times", 18))
        label_out.setAlignment(Qt.AlignCenter)
        label_out.setGeometry(
            int(1080 * self.width_ratio),
            int(165 * self.height_ratio),
            int(140 * self.width_ratio),
            int(39 * self.height_ratio),
        )

        self.value_out = QLabel(str(nilai_out), self)
        self.value_out.setStyleSheet("color: white;")
        self.value_out.setFont(QFont("Times", 18))
        self.value_out.setAlignment(Qt.AlignCenter)
        self.value_out.setGeometry(
            int(1080 * self.width_ratio),
            int(195 * self.height_ratio),
            int(140 * self.width_ratio),
            int(39 * self.height_ratio),
        )

        prev_button = QPushButton("-10", self)
        prev_button.setStyleSheet("color: white; background-color: #3d3d3d")        
        prev_button.setFont(QFont("Times", 18))
        prev_button.setGeometry(
            int(1015 * self.width_ratio),
            int(250 * self.height_ratio),
            int(75 * self.width_ratio),
            int(30 * self.height_ratio),
        )
        prev_button.clicked.connect(self.seek_backward)

        self.play_button = QPushButton('■', self)
        self.play_button.setStyleSheet("color: white; background-color: #3d3d3d")        
        self.play_button.setFont(QFont("Times", 18))
        self.play_button.setGeometry(
            int(1100 * self.width_ratio),
            int(250 * self.height_ratio),
            int(75 * self.width_ratio),
            int(30 * self.height_ratio),
        )
        self.play_button.clicked.connect(self.toggle_play_stop)

        next_button = QPushButton("+10", self)
        next_button.setStyleSheet("color: white; background-color: #3d3d3d")        
        next_button.setFont(QFont("Times", 18))
        next_button.setGeometry(
            int(1185 * self.width_ratio),
            int(250 * self.height_ratio),
            int(75 * self.width_ratio),
            int(30 * self.height_ratio),
        )
        next_button.clicked.connect(self.seek_forward)

        add_data_button = QPushButton("Back", self)
        add_data_button.setStyleSheet(
            """
            background-color: #fb404f;
            color: white;
            font-family: arial;
            font-size: 18px;
            width: 117px;
            height: 43px;
            top: 124px;
            left: 1116px;
            margin: 0px;
            border-radius: 10px; /* Atur radius sudut */
            opacity: 0px;
        """
        )
        add_data_button.setGeometry(
            int(1200 * self.width_ratio),
            int(732 * self.height_ratio),
            int(60 * self.width_ratio),
            int(36 * self.height_ratio),
        )
        add_data_button.clicked.connect(self.back_button_clicked)

        # Bottom frame for playback bar
        bottom_frame = QFrame(self)
        bottom_frame.setGeometry(0, int(770 * self.height_ratio), self.screen_geometry.width(), int(50 * self.height_ratio))

        playback_bar_layout = QVBoxLayout(bottom_frame)
        self.playback_bar = QSlider(Qt.Horizontal)
        self.playback_bar.setStyleSheet("height: 50px;")
        self.playback_bar.sliderPressed.connect(self.slider_pressed)
        self.playback_bar.sliderReleased.connect(self.slider_released)
        playback_bar_layout.addWidget(self.playback_bar)

        # Connect events for real count labels
        # self.value_in.mousePressEvent = self.increment_real_count_in_klik
        # self.value_out.mousePressEvent = self.increment_real_count_out_klik

        self.cap = cv2.VideoCapture(self.record_datas[6])
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(30)
        
        self.playback_bar.setRange(0, int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT)))

    def update_frame(self):
        if not self.updating_position:
            ret, frame = self.cap.read()
            if ret:
                frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                frame_rgb = cv2.resize(frame_rgb, (self.display_video.width(), self.display_video.height()))
                h, w, ch = frame_rgb.shape
                bytes_per_line = ch * w
                qt_image = QImage(frame_rgb.data, w, h, bytes_per_line, QImage.Format_RGB888)
                self.display_video.setPixmap(QPixmap.fromImage(qt_image))
                self.playback_bar.setValue(int(self.cap.get(cv2.CAP_PROP_POS_FRAMES)))
            else:
                self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
                self.playback_bar.setValue(0)

    def increment_real_count_in_klik(self, event):
        if event.button() == Qt.RightButton:
            current_value = int(self.value_in.text())
            self.value_in.setText(str(current_value + 1))

    def increment_real_count_out_klik(self, event):
        if event.button() == Qt.LeftButton:
            current_value = int(self.value_out.text())
            self.value_out.setText(str(current_value + 1))

    def increment_real_count_in(self):
        current_value = int(self.value_in.text())
        self.value_in.setText(str(current_value + 1))

    def increment_real_count_out(self):
        current_value = int(self.value_out.text())
        self.value_out.setText(str(current_value + 1))

    def toggle_play_stop(self):
        if self.timer.isActive():
            self.timer.stop()
            self.play_button.setText('▶')
        else:
            self.timer.start()
            self.play_button.setText('■')

    def set_position(self, position):
        self.updating_position = True
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, position)
        self.update_frame()
        self.updating_position = False

    def slider_pressed(self):
        self.timer.stop()

    def slider_released(self):
        self.set_position(self.playback_bar.value())
        self.timer.start()

    def minimize_window(self, event):
        self.minimize_clicked.emit() 

    def on_close_button_clicked(self, event):
        self.close_clicked.emit() 

    def back_button_clicked(self):
        self.release_resources()
        self.back_record_clicked.emit() 
    
    def save_button_clicked(self):
        value_in_text = self.value_in.text()
        value_out_text = self.value_out.text()

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        if self.record_datas[1] == 1:
            cursor.execute("""
                    UPDATE t_record
                    SET total_visual_in = ?, total_visual_out = ?
                    WHERE id = ?
                """, (value_in_text, value_out_text, self.id_data))
            
        elif self.record_datas[1] == 4:
            cursor.execute("""
                    UPDATE t_record
                    SET total_visual_left = ?, total_visual_right = ?
                    WHERE id = ?
                """, (value_in_text, value_out_text, self.id_data))
            
        print(value_in_text, value_out_text, self.id_data)
        conn.commit()
        cursor.close()
        conn.close()

        self.save_record_clicked.emit()

    def seek_backward(self):
        current_frame = self.cap.get(cv2.CAP_PROP_POS_FRAMES)
        fps = self.cap.get(cv2.CAP_PROP_FPS)
        new_frame = max(current_frame - 10 * fps, 0)
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, new_frame)
        self.update_frame()

    def seek_forward(self):
        current_frame = self.cap.get(cv2.CAP_PROP_POS_FRAMES)
        fps = self.cap.get(cv2.CAP_PROP_FPS)
        frame_count = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
        new_frame = min(current_frame + 10 * fps, frame_count - 1)
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, new_frame)
        self.update_frame()
    
    def get_data_record_by_id(self):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute(f"SELECT * FROM t_record where id = {self.id_data}")
        data = cursor.fetchone()
        conn.commit()                
        conn.close()

        return data
    
    def release_resources(self):
        if self.cap.isOpened():
            self.cap.release()
        self.timer.stop()
# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = CameraLayout()
#     window.show()
#     sys.exit(app.exec_())
