import sys
import os
import subprocess
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal
from sidebar import App_sidebar

class App_home1(QMainWindow):
    def __init__(self):
        super().__init__()
        # self.sidebar = sidebar  
        self.sidebar = App_sidebar()  
        self.sidebar.update_layout()  # Panggil update_layout untuk pengaturan awal
        self.menu1()

    def menu1(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        # Main Content Area
        screen_geometry = QApplication.desktop().screenGeometry()
        self.width_ratio = screen_geometry.width() / 1280
        self.height_ratio = screen_geometry.height() / 832

        # self.loading_tes = QLabel(self)
        # self.loading_tes.setText("loading . . .")
        # self.loading_tes.setStyleSheet(
        #     "color: black; background-color: orange; font-weight: 400; border: 1px solid orange;"
        # )
        # self.loading_tes.setFont(QFont("Times", int(10 * min(self.width_ratio, self.height_ratio))))
        # self.loading_tes.setGeometry(
        #     int(35 * self.width_ratio),
        #     int(100 * self.height_ratio),
        #     int(135 * self.width_ratio),
        #     int(24 * self.height_ratio),
        # )
        # self.loading_tes.setAlignment(Qt.AlignCenter)  # Mengatur alignment grid layout ke atas
        # self.loading_tes.setVisible()  # Menampilkan teks loading

        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * self.width_ratio),
            int(90 * self.height_ratio),
            int(973 * self.width_ratio),
            int(699 * self.height_ratio),
        )

        # add_data_button = QPushButton("Add Data", self)
        # add_data_button.setStyleSheet(
        #     """
        #     background-color: #BB8493;
        #     color: white;
        #     font-family: arial;
        #     font-size: 18px;
        #     width: 117px;
        #     height: 43px;
        #     top: 124px;
        #     left: 1116px;
        #     margin: 0px;
        #     border-radius: 10px; /* Atur radius sudut */
        #     opacity: 0px;
        # """
        # )
        # add_data_button.setGeometry(
        #     int(1116 * self.width_ratio),
        #     int(124 * self.height_ratio),
        #     int(117 * self.width_ratio),
        #     int(43 * self.height_ratio),
        # )
        # add_data_button.clicked.connect(self.add_data_button_clicked)    

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * self.width_ratio),
            int(105 * self.height_ratio),
            int(250 * self.width_ratio),
            int(43 * self.height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" About ")

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_home2()
#     window.show()
#     sys.exit(app.exec_())
