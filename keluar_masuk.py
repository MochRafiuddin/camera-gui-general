import cv2
import torch
import time
import numpy as np
import app_sync
import sqlite3
import datetime
class CentroidTracker:
    def __init__(self, max_disappeared=40):
        self.next_object_id = 0
        self.objects = {}
        self.disappeared = {}
        self.max_disappeared = max_disappeared

    def register(self, centroid):
        self.objects[self.next_object_id] = centroid
        self.disappeared[self.next_object_id] = 0
        self.next_object_id += 1

    def deregister(self, object_id):
        del self.objects[object_id]
        del self.disappeared[object_id]

    def update(self, rects):
        if len(rects) == 0:
            for object_id in list(self.disappeared.keys()):
                self.disappeared[object_id] += 1

                if self.disappeared[object_id] > self.max_disappeared:
                    self.deregister(object_id)

            return self.objects, {}

        input_centroids = np.zeros((len(rects), 2), dtype="int")
        input_boxes = {}

        for (i, (startX, startY, endX, endY)) in enumerate(rects):
            cX = int((startX + endX) / 2.0)
            cY = int((startY + endY) / 2.0)
            input_centroids[i] = (cX, cY)
            input_boxes[i] = (startX, startY, endX, endY)

        if len(self.objects) == 0:
            for i in range(0, len(input_centroids)):
                self.register(input_centroids[i])
        else:
            object_centroids = list(self.objects.values())
            object_ids = list(self.objects.keys())
            object_centroids = np.array(object_centroids)
            input_centroids = np.array(input_centroids)

            D = np.linalg.norm(object_centroids[:, np.newaxis] - input_centroids, axis=2)

            rows = D.min(axis=1).argsort()
            cols = D.argmin(axis=1)[rows]

            used_rows = set()
            used_cols = set()

            for (row, col) in zip(rows, cols):
                if row in used_rows or col in used_cols:
                    continue

                object_id = object_ids[row]
                self.objects[object_id] = input_centroids[col]
                self.disappeared[object_id] = 0

                used_rows.add(row)
                used_cols.add(col)

            unused_rows = set(range(0, D.shape[0])).difference(used_rows)
            unused_cols = set(range(0, D.shape[1])).difference(used_cols)

            if D.shape[0] >= D.shape[1]:
                for row in unused_rows:
                    object_id = object_ids[row]
                    self.disappeared[object_id] += 1

                    if self.disappeared[object_id] > self.max_disappeared:
                        self.deregister(object_id)
            else:
                for col in unused_cols:
                    self.register(input_centroids[col])

        return self.objects, input_boxes

class VideoCamera:
    def __init__(self):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model = torch.hub.load('yolov5', 'custom', 'models/crowdhuman_yolov5m.pt', source='local', force_reload=True).to(device).eval()
        self.device = device
        self.model = model
        self.centroid_tracker = CentroidTracker(max_disappeared=30)
        self.fps = 0        
        self.frame_count = 0
        self.start_time = time.time()
        self.last_time_insert = time.time()
        self.time_interval = app_sync.get_cosntant_by_code('time_interval')

    def get_frame(self, frame, id_camera, data_record):
        self.id_data = id_camera
        self.data = self.get_peopel_hari_ini()
        self.people_in = self.data[11]
        self.people_out = self.data[12]
        label = self.data[17]
        data_garis =  eval(self.data[4]) if self.data[4] else []
        data_roi = eval(self.data[3]) if self.data[3] else []
        rasio_width = self.data[21]
        rasio_height = self.data[22]
        in_side = self.data[5]
        self.tracked_objects = eval(self.data[15]) if self.data[15] else {}
        
        daily_in = self.data[26]
        daily_out = self.data[27]
        traffic_in = self.data[28]
        traffic_out = self.data[29]

        height, width, _ = frame.shape
        self.frame_count += 1
        if self.frame_count >= 15:
            end_time = time.time()
            elapsed_time = end_time - self.start_time
            self.fps = self.frame_count / elapsed_time
            self.frame_count = 0
            self.start_time = time.time()

        cv2.putText(frame, f"FPS: {self.fps:.2f}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
        cv2.putText(frame, f"{label}", (10, height - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        garis = data_garis
        if len(garis) == 2:
            garis = [list(point) for point in garis]
            garis[0][0] = int(garis[0][0] * float(rasio_width))
            garis[0][1] = int(garis[0][1] * float(rasio_height))
            garis[1][0] = int(garis[1][0] * float(rasio_width))
            garis[1][1] = int(garis[1][1] * float(rasio_height))
            cv2.line(frame, (garis[0][0], garis[0][1]), (garis[1][0], garis[1][1]), (0, 0, 0), thickness=2)
        elif len(garis) < 2:
            for line in garis:
                cv2.circle(frame, (int(line[0]), int(line[1])), 4, (0, 0, 0), -1)

        roi = data_roi
        if len(roi) > 3:
            roi_coords = np.array(roi, np.float64)
            roi_coords[:, 0] *= float(rasio_width)
            roi_coords[:, 1] *= float(rasio_height)
            roi_coords = roi_coords.astype(np.int32)
            roi_coords = roi_coords.reshape((-1, 1, 2))
            cv2.polylines(frame, [roi_coords], True, (0, 0, 255), thickness=2)
            mask = np.zeros_like(frame)
            cv2.fillPoly(mask, [roi_coords], (255, 255, 255))
            roi_frame = cv2.bitwise_and(frame, mask)
            results, input_boxes = self.detect_objects(roi_frame)
            frame = self.draw_boxes(frame, input_boxes)
        else:
            results, input_boxes = self.detect_objects(frame)
            frame = self.draw_boxes(frame, input_boxes)

        frame_vidio = frame.copy()
        self.id_record = 0
        self.in_vidio = 0
        self.out_vidio = 0
        if data_record != 0:
            self.id_record = data_record
            self.data_camera_record = self.get_data_record()
            # print(f"data record {self.id_record} :", self.data_camera_record)
            if self.data_camera_record[1] == 1:
                self.in_vidio = self.data_camera_record[8]
                self.out_vidio = self.data_camera_record[9]
            else:
                self.in_vidio = self.data_camera_record[10]
                self.out_vidio = self.data_camera_record[11]

        for (object_id, centroid) in results.items():
            text = f"ID {object_id}"
            cv2.putText(frame, f"ID {object_id}", (centroid[0], centroid[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            cv2.putText(frame_vidio, f"ID {object_id}", (centroid[0], centroid[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

            if len(garis) == 2:
                if object_id not in self.tracked_objects:
                    if self.is_left(garis[0], garis[1], centroid):
                        self.tracked_objects[object_id] = ("left", False)
                    elif self.is_right(garis[0], garis[1], centroid):
                        self.tracked_objects[object_id] = ("right", False)
                else:
                    current_position = "left" if self.is_left(garis[0], garis[1], centroid) else "right"
                    last_position, has_crossed = self.tracked_objects[object_id]

                    if in_side == "left":
                        if last_position == "left" and current_position == "right" and not has_crossed:
                            self.people_in += 1
                            self.in_vidio += 1
                            daily_in += 1
                            traffic_in += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "right" and current_position == "left" and not has_crossed:
                            self.people_out += 1
                            self.out_vidio += 1
                            daily_out += 1
                            traffic_out += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "left" and current_position == "right" and has_crossed:
                            self.people_out -= 1
                            self.out_vidio -= 1
                            daily_out -= 1
                            traffic_out -= 1
                            self.tracked_objects[object_id] = (current_position, False)
                        elif last_position == "right" and current_position == "left" and has_crossed:
                            self.people_in -= 1
                            self.in_vidio -= 1
                            daily_in -= 1
                            traffic_in -= 1
                            self.tracked_objects[object_id] = (current_position, False)
                    else:
                        if last_position == "right" and current_position == "left" and not has_crossed:
                            self.people_in += 1
                            self.in_vidio += 1
                            daily_in += 1
                            traffic_in += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "left" and current_position == "right" and not has_crossed:
                            self.people_out += 1
                            self.out_vidio += 1
                            daily_out += 1
                            traffic_out += 1
                            self.tracked_objects[object_id] = (current_position, True)
                        elif last_position == "right" and current_position == "left" and has_crossed:
                            self.people_out -= 1
                            self.out_vidio -= 1
                            daily_out -= 1
                            traffic_out -= 1
                            self.tracked_objects[object_id] = (current_position, False)
                        elif last_position == "left" and current_position == "right" and has_crossed:
                            self.people_in -= 1
                            self.in_vidio -= 1
                            daily_in -= 1
                            traffic_in -= 1
                            self.tracked_objects[object_id] = (current_position, False)

                self.update_data(self.people_in, self.people_out, self.tracked_objects)
                self.add_value_constant(self.data[18], daily_in, daily_out, traffic_in, traffic_out)
                if data_record != 0:
                    self.update_data_record(self.in_vidio, self.out_vidio)


        cv2.putText(frame_vidio, f"in: {self.in_vidio}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
        cv2.putText(frame_vidio, f"out: {self.out_vidio}", (10, 80), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        # cv2.putText(frame, f"in: {self.people_in}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
        # cv2.putText(frame, f"out: {self.people_out}", (10, 80), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        current_time = time.time()
        if (current_time - self.last_time_insert) > int(self.time_interval[2]):  # Ubah sesuai interval waktu yang diinginkan
            app_sync.simpan_log_in_out(self.data[0])
            self.last_time_insert = current_time

        return frame, frame_vidio

    def detect_objects(self, frame):
        with torch.amp.autocast('cuda'):
            results = self.model(frame)
        objects = []

        for index, row in results.pandas().xyxy[0].iterrows():
            confidence = float(row['confidence'])
            class_name = row['name']
            if class_name != "head":
                continue
            if confidence > 0.375:
                x1 = int(row['xmin'])
                y1 = int(row['ymin'])
                x2 = int(row['xmax'])
                y2 = int(row['ymax'])
                class_name = str(row['name'])
                objects.append({'label': class_name, 'confidence': confidence, 'bbox': row})

        bounding_boxes = [(int(obj['bbox']['xmin']), int(obj['bbox']['ymin']), int(obj['bbox']['xmax']), int(obj['bbox']['ymax'])) for obj in objects]
        bounding_boxes_conf = [(int(obj['bbox']['xmin']), int(obj['bbox']['ymin']), int(obj['bbox']['xmax']), int(obj['bbox']['ymax']), float(obj["confidence"])) for obj in objects]
        objects, input_boxes = self.centroid_tracker.update(bounding_boxes)
        return objects, bounding_boxes_conf

    def draw_boxes(self, frame, bounding_boxes):
        for bbox in bounding_boxes:
            x1, y1, x2, y2, conf = bbox
            titik_tengah_x = int((x1 + x2) / 2)
            titik_tengah_y = int((y1 + y2) / 2)
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 2)
        return frame

    def is_left(self, p1, p2, p):
        return (p2[0] - p1[0]) * (p[1] - p1[1]) - (p[0] - p1[0]) * (p2[1] - p1[1]) > 0

    def is_right(self, p1, p2, p):
        return not self.is_left(p1, p2, p)
    
    def update_data_record(self, masuk, keluar):

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        if self.data_camera_record[1] == 1:
            update_query = "UPDATE t_record SET total_system_in = ?, total_system_out = ? WHERE id = ?"
        else:
            update_query = "UPDATE t_record SET total_system_left = ?, total_system_right = ? WHERE id = ?"
        
        # Execute the update query
        cursor.execute(update_query, (masuk, keluar, self.id_record))
        
        # Commit the changes
        conn.commit()
        
        cursor.close()
        conn.close()

    def get_peopel_hari_ini(self):
        
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        # Create the table if it doesn't exist
        data = cursor.execute(f'SELECT * FROM setting_camera where id="{self.id_data}"').fetchone()
        return data
    
    def update_data(self, people_in, people_out, tracked_objects):
        record_id = self.data[0]

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        update_query = "UPDATE setting_camera SET people_in = ?, people_out = ?, people_masuk = ? WHERE id = ?"
        
        # Execute the update query
        cursor.execute(update_query, (people_in , people_out, str(tracked_objects), record_id))
        
        # Commit the changes
        conn.commit()
        
        cursor.close()
        conn.close()
    
    def update_current_deteksi(self, value, code):
        record_id = self.data[0]

        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        # cursor.execute(f"UPDATE constant SET value = {value} WHERE code = '{code}'")
        # print(code, value, self.id_data)
        cursor.execute(f"UPDATE setting_camera SET {code} = {value} WHERE id = {record_id}")
        conn.commit()
        cursor.close()
        conn.close()
    
    def add_value_constant(self, type, daily_in, daily_out, traffic_in, traffic_out):
        if int(type) == 1 :
            self.update_current_deteksi(int(daily_in), 'visitor_daily_in')
            self.update_current_deteksi(int(daily_out), 'visitor_daily_out')
        elif int(type) == 4 :
            self.update_current_deteksi(int(traffic_in), 'outer_traffic_left')
            self.update_current_deteksi(int(traffic_out), 'outer_traffic_right')
            self.update_current_deteksi(int(traffic_out)+int(traffic_in), 'outer_traffic_total_traffic')
    
    def get_data_record(self):    
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()
        # Create the table if it doesn't exist
        data = cursor.execute(f'SELECT * FROM t_record where id="{self.id_record}"').fetchone()
        return data