import sys
import os
import subprocess
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QLabel,
    QVBoxLayout,
    QHeaderView,
    QHBoxLayout,
    QWidget,
    QMessageBox
)
from PyQt5.QtGui import QPixmap, QFont, QIcon, QColor
from PyQt5.QtCore import QSize, Qt, QProcess, pyqtSignal
from sidebar import App_sidebar
import sqlite3
from datetime import datetime

class App_home4(QMainWindow):
    edit_snapshot_clicked = pyqtSignal(int)

    def __init__(self):
        super().__init__()
        self.sidebar = App_sidebar()  # Buat instance dari kelas App_sidebar
        self.sidebar.update_layout()  # Panggil update_layout untuk pengaturan awal
        self.update_content()
        
    def update_content(self):
        self.menu4()
    
    def menu4(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)  # Set jarak antar widget menjadi nol
        layout.setContentsMargins(0, 0, 0, 0)  # Set ruang perbatasan menjadi nol
        layout.addWidget(self.sidebar)  # Add the sidebar to the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        # Main Content Area
        screen_geometry = QApplication.desktop().screenGeometry()
        width_ratio = screen_geometry.width() / 1280
        height_ratio = screen_geometry.height() / 832
        main_content = QLabel(self)
        main_content.setStyleSheet("background-color: #3d3d3d; border-radius: 10px;")
        main_content.setGeometry(
            int(280 * width_ratio),
            int(90 * height_ratio),
            int(973 * width_ratio),
            int(699 * height_ratio),
        )

        self.GButton_12 = QPushButton(self)
        self.GButton_12.setGeometry(
            int(290 * width_ratio),
            int(105 * height_ratio),
            int(250 * width_ratio),
            int(43 * height_ratio),
        )
        self.GButton_12.setStyleSheet(
            "background-color: rgba(0,0,0,0); color: white; font-family: Inter; font-size: 38px; text-align: left; border-radius: 10px; font-weight: 1000"
        )
        self.GButton_12.setText(" Data Snapshot ")

        self.table_widget = QTableWidget(self)
        self.table_widget.setGeometry(
            int(312 * width_ratio),
            int(191 * height_ratio),
            int(921 * width_ratio),
            int(510 * height_ratio),
        )
        self.table_widget.setColumnCount(6)
        self.table_widget.setStyleSheet(
            """
            QTableWidget {
                background-color: #3d3d3d;
                border: none;
                font-family: arial;
                font-size: 18px;
                
            }
            QTableWidget::item {
                padding: 10px;
                border-bottom: 1px solid white; /* Set border color to white */
                color: white;
                font-family: arial;
                font-size: 18px;
            }
            QHeaderView::section {
                background-color: black; /* Set header background to black */
                color: white; 
                border: none;
                font-weight: 400;
                
                padding: 10px;
                font-family: arial;
                font-size: 20px;
                
            }
            
            """
        )

        self.table_widget.setHorizontalHeaderLabels(
            ["No", "File", "Camera Type", "Deskripsi", "Created At", "Option"]
        )

        header = self.table_widget.horizontalHeader()
        header.setStyleSheet(
            """
            background-color: black; /* Set header background to black */
            color: white; /* Set header text color to white */

           
            border: none;
            """
        )

        self.table_widget.setShowGrid(False)
        self.table_widget.verticalHeader().setVisible(False)

        self.table_widget.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        # Set alignment for specific header sections (kolom 3 dan 4)
        self.table_widget.horizontalHeaderItem(2).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(3).setTextAlignment(Qt.AlignCenter)
        self.table_widget.horizontalHeaderItem(4).setTextAlignment(Qt.AlignCenter)
        # self.table_widget.horizontalHeaderItem(5).setTextAlignment(Qt.AlignCenter)

        self.table_widget.horizontalHeader().resizeSection(0, int(35 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(1, int(250 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(2, int(110 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(3, int(250 * width_ratio))  # Atur lebar kolom 3 secara manual
        self.table_widget.horizontalHeader().resizeSection(4, int(150 * width_ratio))
        self.table_widget.horizontalHeader().resizeSection(5, int(110 * width_ratio))

        # self.table_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.add_data_to_table()

    def add_data_to_table(self):
        conn = sqlite3.connect('setting.db')
        cursor = conn.cursor()        
        cursor.execute("SELECT * FROM t_snapshot WHERE deleted = 1 ORDER BY id desc")
        dataall = cursor.fetchall()
        conn.commit()                
        conn.close()
        books = []
        for data in dataall:
            if data[2] == "1":
                camera_type = "Visitor"
            elif data[2] == "2":
                camera_type = "Gender"
            elif data[2] == "3":
                camera_type = "Zone"
            elif data[2] == "4":
                camera_type = "Outer Traffic"
            
            tanggal_awal = data[3]            
            tanggal_objek = datetime.strptime(tanggal_awal, "%Y-%m-%d %H:%M:%S")
            tanggal_ubah = tanggal_objek.strftime("%d-%m-%Y %H:%M:%S")

            books.append((data[1], camera_type, data[5], tanggal_ubah, data[0]))

        for row, (file, cam_type, deskripsi, created_at, id_snapshot) in enumerate(books):
            self.table_widget.insertRow(row)

            no = QTableWidgetItem(f"{row+ 1}")
            self.table_widget.setItem(row, 0, no)

            file_title = QTableWidgetItem(file)
            self.table_widget.setItem(row, 1, file_title)

            cam_type_title = QTableWidgetItem(cam_type)
            self.table_widget.setItem(row, 2, cam_type_title)

            deskripsi_title = QTableWidgetItem(deskripsi)
            self.table_widget.setItem(row, 3, deskripsi_title)
            
            created_at_title = QTableWidgetItem(created_at)
            self.table_widget.setItem(row, 4, created_at_title)            

            icon_widget = QWidget()
            icon_widget.setStyleSheet("QWidget { background-color: transparent;}")

            icon_layout = QHBoxLayout()

            self.button_edit_snapshot = QLabel()
            self.button_edit_snapshot.setPixmap(QPixmap("img/edit.png").scaled(20, 20))
            self.button_edit_snapshot.setAlignment(Qt.AlignRight)
            icon_layout.addWidget(self.button_edit_snapshot)
            self.button_edit_snapshot.setStyleSheet("QLabel { background-color: #3d3d3d;}")
            self.button_edit_snapshot.mousePressEvent = lambda event, id_data=id_snapshot: self.emit_menu_edit_snapshot_clicked(id_data)

            clickable_icon_2 = QLabel()
            clickable_icon_2.setPixmap(QPixmap("img/del.png").scaled(20, 20))
            clickable_icon_2.setAlignment(Qt.AlignLeft)
            clickable_icon_2.setFixedHeight(20)
            icon_layout.addWidget(clickable_icon_2)
            clickable_icon_2.setStyleSheet("QLabel { background-color: #3d3d3d; }")
            clickable_icon_2.mousePressEvent = lambda event, id_data=id_snapshot: self.show_delete_confirmation_popup(id_data)

            icon_widget.setLayout(icon_layout)

            self.table_widget.setCellWidget(row, 5, icon_widget)

            self.table_widget.setRowHeight(row, 60)
    
    def show_delete_confirmation_popup(self, id_data):
        # Membuat instance QMessageBox
        msg = QMessageBox()
        msg.setWindowTitle("Confirm Data Deletion")
        msg.setText("Are you sure you want to delete the data?")
        msg.setIcon(QMessageBox.Warning)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setDefaultButton(QMessageBox.Cancel)

        # Menampilkan popup dan mendapatkan respons pengguna
        response = msg.exec_()

        # Jika pengguna menekan tombol Ok, maka lakukan aksi penghapusan data
        if response == QMessageBox.Ok:
            print("Data berhasil dihapus!") 
            conn = sqlite3.connect('setting.db')
            cursor = conn.cursor()
            update_query = f"UPDATE t_snapshot SET deleted = ? where id = ?"
            cursor.execute(update_query, ('0', id_data))
            conn.commit()        
            cursor.close()
            conn.close()
            self.clear_table()
            self.add_data_to_table()

    def clear_table(self):
        self.table_widget.clearContents()  # Menghapus konten tabel
        self.table_widget.setRowCount(0)  # Mengatur jumlah baris tabel menjadi 0

    def emit_menu_edit_snapshot_clicked(self, id_data):
        # print("Menu edit cam list clicked")
        self.edit_snapshot_clicked.emit(id_data)

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = App_home2()
#     window.show()
#     sys.exit(app.exec_())
