@echo off
rem Menghentikan semua proses python.exe
for /f "tokens=2 delims=," %%a in ('tasklist /FI "IMAGENAME eq python.exe" /NH /FO CSV') do (
    taskkill /PID %%a /F
)

rem Menutup semua jendela Command Prompt
taskkill /FI "IMAGENAME eq cmd.exe" /F

rem Mengakhiri script batch utama agar tidak tetap terbuka
exit
